"""Mixins for the BaseMR."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from functools import cached_property
from os import environ
from textwrap import dedent
from time import sleep

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
import prometheus_client as prometheus

from webhook.approval_rules import ApprovalRule
from webhook.common import find_config_items_kconfigs
from webhook.common import get_owners_parser
from webhook.description import Commit
from webhook.fragments import GL_USER
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineResult

LOGGER = get_logger('cki.webhook.base_mr_mixins')

METRIC_KWF_QUERY_RETRIES = prometheus.Counter(
    'kwf_query_retries',
    'Number of times query data was found on an MR only after retrying'
)


@dataclass(repr=False)
class ApprovalsMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for approval rules to base_mr.BaseMR."""

    APPROVALS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrApprovals
        }
      }
    }

    fragment MrApprovals on MergeRequest {
      approvalState {
        rules {
          ...MrApprovalRule
          approved
          approvedBy {
            nodes {
              ...GlUser
            }
          }
          sourceRule {
            ...MrApprovalRule
          }
        }
      }
    }

    fragment MrApprovalRule on ApprovalRule {
      approvalsRequired
      id
      overridden
      name
      type
      eligibleApprovers {
        ...GlUser
      }
    }
    """)

    APPROVALS_QUERY = APPROVALS_QUERY_BASE + GL_USER

    @cached_property
    def approval_rules(self):
        """Return a dict of ApprovalRule objects with the rule name as the key."""
        results = self.query(self.APPROVALS_QUERY, paged_key='project/mr/approvalState/rules')
        raw_rules = get_nested_key(results, 'project/mr/approvalState/rules', [])
        return {rule['name']: ApprovalRule(self.user_cache, rule) for rule in raw_rules}


@dataclass(repr=False)
class CommitsMixin:
    """A Mixin to provide support for commits to BaseMR."""

    COMMIT_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrCommits
        }
      }
    }

    fragment MrCommits on MergeRequest {
      commits: commitsWithoutMergeCommits(after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          author {
            ...GlUser
          }
          authorEmail
          authorName
          authoredDate
          description
          sha
          title
        }
      }
    }
    """)

    COMMIT_QUERY = COMMIT_QUERY_BASE + GL_USER

    @property
    def all_commits(self):
        """Return a list of all the Commit objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.faux_description_commit] + list(self.commits.values())

    @property
    def all_descriptions(self):
        """Return a list of all the Description objects of the MR."""
        if not self.description.text and not self.commits:
            return []
        return [self.description] + [commit.description for commit in self.commits.values()]

    @cached_property
    def commits(self):
        """Return a dict of Commit objects with sha as the key."""
        results = self.query(self.COMMIT_QUERY, paged_key='project/mr/commits')
        raw_commits = get_nested_key(results, 'project/mr/commits/nodes', [])
        commits = {}
        for raw_commit in raw_commits:
            # Pass in an author User from the cache.
            author = self.user_cache.get(raw_commit.pop('author')) if \
                raw_commit.get('author') else None
            commit = Commit(input_dict=raw_commit, author=author)
            commits[commit.sha] = commit
        return commits

    @property
    def commits_without_depends(self):
        """Return a dict of the MR commits excluding commits from depends."""
        # We use depends_mrs here because it will update self.description.depends in the case where
        # only self.description.depends_mrs is set.
        if not self.depends_mrs:
            return self.commits
        commits = {}
        for sha, commit in self.commits.items():
            if any(bug for bug in commit.description.bugzilla if bug in self.description.depends):
                break
            commits[sha] = commit
        return commits

    @cached_property
    def faux_description_commit(self):
        """Return a faux Commit derived from the MR Description."""
        # If the MR author has no public email then assume (lol) the first commit is theirs and take
        # its authorEmail?
        author_email = ''
        if self.author and self.author.emails:
            author_email = self.author.emails[0]
        elif self.commits:
            author_email = list(self.commits.values())[0].authorEmail
        author_name = self.author.name if self.author else ''
        return Commit(author=self.author, authorEmail=author_email, authorName=author_name,
                      description=self.description, sha='MR Description', title=self.title)

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose Bugzilla tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        return next((sha for sha, commit in self.commits.items() for bz_id in
                     self.description.depends if bz_id in commit.description.bugzilla), '')

    @cached_property
    def files(self):
        """Return the list of files affected by the non-dependency commits of the MR."""
        if not self.has_depends:
            return self._files
        commit_shas = list(self.commits_without_depends.keys())
        comparison = self.gl_project.repository_compare(f'{commit_shas[-1]}^', commit_shas[0])
        return [diff['new_path'] for diff in comparison['diffs']]

    @property
    def fresh_commits(self):
        """Return a fresh dict of Commits."""
        if 'commits' in self.__dict__:
            del self.commits
        return self.commits

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(commit.description.bugzilla or commit.description.marked_internal for
                       commit in self.commits.values())


@dataclass(repr=False)
class DependsMixin:
    """A Mixin to provide support for Dependencies to BaseMR."""

    is_dependency: bool = field(default=False, kw_only=True)

    @cached_property
    def depends_mrs(self):
        """Return a list of BaseMR objects representing any Depends: MRs."""
        return [self.new_depends_mr(mr_id) for mr_id in self.description.depends_mrs]

    def new_depends_mr(self, iid):
        """Return a new instance of self with dependency=True set."""
        new_url = f"{self.url.rsplit('/', 1)[0]}/{iid}"
        return replace(self, url=new_url, is_dependency=True)


@dataclass(repr=False)
class GraphMixin:
    """A mixin to provide a graphql query wrapper for MRs."""

    graphql: GitlabGraph
    query_retries: int = field(default=2, kw_only=True)
    query_retry_delay: int = field(default=5, kw_only=True)

    @staticmethod
    def __check_query_results(results):
        """Check the query results are useful."""
        if not results['project']['mr']:
            LOGGER.warning('Merge request does not exist?')
            return False
        return True

    def query(self, query_string, query_params_extra=None, paged_key=None, process_function=None):
        """Run the given query and process it with the given function."""
        query_params = {}
        if namespace := getattr(self, 'namespace'):
            query_params['namespace'] = namespace
        if iid := getattr(self, 'iid'):
            query_params['mr_id'] = str(iid)
        if query_params_extra:
            query_params.update(query_params_extra)
        for retry in range(self.query_retries):
            results = self.graphql.client.query(query_string, variable_values=query_params,
                                                paged_key=paged_key)
            if not results:
                LOGGER.warning('No results for query with params: %s\n%s', query_params,
                               query_string)
                return None
            if self.__check_query_results(results):
                if retry:
                    LOGGER.warning('API returned data on the second try 🙄.')
                    METRIC_KWF_QUERY_RETRIES.inc()
                break
            if retry:
                LOGGER.info('Still no data after retry 🤷.')
                break
            LOGGER.info('No data found, trying again in %s seconds.', self.query_retry_delay)
            sleep(self.query_retry_delay)
        return process_function(results) if process_function else results


@dataclass(repr=False, kw_only=True)
class OwnersMixin:
    """A mixin to provide support for an owners parser to base_mr.BaseMR."""

    owners_path: str = environ.get('OWNERS_YAML')
    kernel_src: str = environ.get('LINUS_SRC', '')
    merge_entries: bool = False  # whether the owners_entries method should merge results or not.

    @cached_property
    def all_files(self):
        """Return the set of all files impacted by the MR."""
        kconfigs = find_config_items_kconfigs(self.config_items, self.kernel_src) if \
            self.kernel_src else set()
        return set(self.files) | kconfigs

    @cached_property
    def config_items(self):
        """Return the list of CONFIG items touched by this MR."""
        # return common.get_configs_from_paths(self.files)
        return [path.rsplit('/', 1)[-1] for path in self.files if path.startswith('redhat/configs/')
                and '/CONFIG_' in path]

    @cached_property
    def owners(self):
        """Return an owners parser objects."""
        return get_owners_parser(self.owners_path)

    @cached_property
    def owners_entries(self):
        # pylint: disable=protected-access
        """Return the list of matching owners entries."""
        if not self.merge_entries:
            return self.owners.get_matching_entries(self.all_files)
        # It is possible for an MR to match multiple entries which have the same subsystem_label. In
        # this case we merge those entries together and just return one. Information is lost but for
        # our purposes we (currently) only care about the list of reviewers/maintainers.
        entries = {}
        for entry in self.owners.get_matching_entries(self.all_files):
            if not (existing := entries.get(entry.subsystem_label)):
                entries[entry.subsystem_label] = entry
                continue
            for user in entry.maintainers:
                if user not in existing.maintainers:
                    existing._entry['maintainers'].append(user)
            for user in entry.reviewers:
                if user not in existing.reviewers:
                    existing._entry['reviewers'].append(user)
            if entry.required_approvals:
                existing._entry['requiredApproval'] = True
        return list(entries.values())


@dataclass(repr=False)
class PipelinesMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Pipelines to base_mr.BaseMR."""

    PIPELINES_QUERY = dedent("""
    query mrData($namespace: ID!, $mr_id: String!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          headPipeline {
            jobs {
              nodes {
                allowFailure
                id
                name
                createdAt
                pipeline {
                  id
                }
                status
                downstreamPipeline {
                  id
                  project {
                    id
                    fullPath
                  }
                  status
                  stages {
                    nodes {
                      name
                      jobs {
                        nodes {
                          status
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    """)

    @property
    def fresh_pipelines(self):
        """Get a fresh dict of pipelines."""
        if 'pipelines' in self.__dict__:
            del self.pipelines
        return self.pipelines

    @cached_property
    def pipelines(self):
        """Return the list of newest PipelineResults for the MR's head pipeline."""
        results = self.query(self.PIPELINES_QUERY)
        raw_pipelines = get_nested_key(results, 'project/mr/headPipeline/jobs/nodes', [])
        # If a downstream job has been retried then filter out any old results.
        return PipelineResult.prepare_pipelines(raw_pipelines)


@dataclass(repr=False)
class ReviewersMixin:
    # pylint: disable=too-few-public-methods
    """A Mixin to provide support for Reviewers to base_mr.BaseMR."""

    REVIEWERS_QUERY_BASE = dedent("""
    query mrData($mr_id: String!, $namespace: ID!) {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...MrReviewers
        }
      }
    }

    fragment MrReviewers on MergeRequest {
      reviewers {
        nodes {
          ...GlUser
        }
      }
    }
    """)

    REVIEWERS_QUERY = REVIEWERS_QUERY_BASE + GL_USER

    @property
    def fresh_reviewers(self):
        """Get a fresh dict of reviewers."""
        if 'reviewers' in self.__dict__:
            del self.reviewers
        return self.reviewers

    @cached_property
    def reviewers(self):
        """Return a dict of Users representing the current reviewers of the MR."""
        results = self.query(self.REVIEWERS_QUERY)
        raw_reviewers = get_nested_key(results, 'project/mr/reviewers/nodes', [])
        return {raw_reviewer['username']: self.user_cache.get(raw_reviewer) for
                raw_reviewer in raw_reviewers}

    def set_reviewers(self, usernames, mode='APPEND'):
        """Call graphql.set_mr_reviewers and uncache the reviewers property."""
        LOGGER.info('%sing reviewers with: %s', mode.capitalize().removesuffix('e'), usernames)
        self.graphql.set_mr_reviewers(self.namespace, self.iid, usernames, mode)
        if 'reviewers' in self.__dict__:
            del self.reviewers
