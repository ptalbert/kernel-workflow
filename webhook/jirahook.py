# pylint: disable=invalid-name
"""Query JIRA for MRs."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
import re
import sys

from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
import gql

from webhook import common
from webhook import defs
from webhook import fragments
from webhook import libjira
from webhook import table
from webhook.description import Description
from webhook.description import MRDescription
from webhook.graphql import GitlabGraph
from webhook.jissue import JIssue
from webhook.jissue import make_jissues
from webhook.rh_metadata import Branch
from webhook.rh_metadata import Projects

LOGGER = get_logger('cki.webhook.jirahook')

COMMENT_TITLE = '**JIRA Hook Readiness Report**'
COMMENT_FOOTER = ("Guidelines for these entries can be found in CommitRules: "
                  "https://red.ht/kwf_commit_rules.  \nTo request re-evalution either remove the "
                  "JIRA label from the MR or add a comment with only the text: "
                  "request-jirahook-evaluation.")


@dataclass
class MR:
    # pylint: disable=too-many-instance-attributes
    """Store for MR data."""

    # required parameters
    graphql: GitlabGraph
    projects: Projects = field(repr=False)
    namespace: str
    mr_id: int
    is_dependency: bool = False

    # set by do_jirahook_query()
    commits: dict = field(default_factory=dict, init=False, repr=False)
    title: str = field(default='', init=False)
    description: MRDescription | None = field(default=None, init=False)
    global_id: str = field(default='', init=False)
    project_id: str = field(default='', init=False)
    branch: Branch = field(default=None, init=False, repr=False)
    state: defs.MrState = field(default=defs.MrState.UNKNOWN, init=False)
    only_internal_files: bool = False  # set if the MR only touches INTERNAL_FILES
    depends_mrs: list = field(default_factory=list, init=False, repr=False)
    pipeline_finished: datetime = field(default=None, init=False)
    is_draft: bool = field(default=False, init=False)

    def __post_init__(self):
        """Initialize the object."""
        super().__init__()
        self._get_mr_data()
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Return a string represantion of the MR."""
        repr_str = f'MR {self.namespace}!{self.mr_id}'
        repr_str += f', commits: {len(self.commits)}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', is_draft: {self.is_draft}'
        repr_str += f", is_dependency: {self.is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def _get_mr_data(self):
        """Query the MR and return an MR object."""
        query_params = {'namespace': self.namespace, 'mr_id': str(self.mr_id)}
        results = self.graphql.check_query_results(self.graphql.client.query(
            JIRAHOOK_QUERY, variable_values=query_params, paged_key='project/mr/commits'),
            check_keys={'currentUser', 'project'})
        if not results:
            LOGGER.warning('No query results??')
            return
        if not results['project']['mr']:
            LOGGER.warning('Merge request does not exist?')
            return
        self._populate_attributes(results)

    def _populate_attributes(self, results):
        """Parse JIRAHOOK_QUERY results into attributes."""
        raw_commits = results['project']['mr']['commits']['nodes']
        self.commits = {commit['sha']: Description(commit['description']) for commit in raw_commits}
        self.global_id = results['project']['mr']['id']
        self.project_id = results['project']['id']
        self.title = results['project']['mr']['title']
        self.description = MRDescription(results['project']['mr']['description'], self.namespace)
        mr_file_list = [path['path'] for path in results['project']['mr']['files']]
        self.only_internal_files = self._mr_has_only_internal_files(mr_file_list)
        self.state = defs.MrState.from_str(results['project']['mr']['state'])
        self.branch = self.projects.get_target_branch(self.project_id,
                                                      results['project']['mr']['targetBranch'])
        if results['project']['mr']['headPipeline'] and \
           results['project']['mr']['headPipeline']['finishedAt']:
            pipeline_timestamp = results['project']['mr']['headPipeline']['finishedAt']
            self.pipeline_finished = datetime.fromisoformat(pipeline_timestamp[:19])
        self.is_draft = results['project']['mr']['draft']

    @staticmethod
    def _mr_has_only_internal_files(path_list):
        """Return True if all the file paths begin with allowed paths for INTERNAL issues."""
        return all(path.startswith(defs.INTERNAL_FILES) for path in path_list)

    @property
    def all_jissue_ids(self):
        """Return the set of all the JIssue IDs referenced in the MR."""
        if not self.description:
            return set()
        return {ji_id for desc in self.all_descriptions for ji_id in desc.jissue}
        # return {ji_id for desc in self.all_descriptions for ji_id in desc.jissue | desc.depends}

    @property
    def all_descriptions(self):
        """Return  a list of all the Description objects from the MR."""
        if not self.description:
            return []
        return [self.description] + list(self.commits.values())

    @property
    def jissues(self):
        """Return the list of all JIssue objects derived from this MR."""
        # This should be any JIRA Issue called out in a JIRA: or Depends: tag in all the MRs
        # plus possibly the faux INTERNAL and/or UNTAGGED JIssue.
        all_mrs = [self] + self.depends_mrs
        jissues = make_jissues(self.all_jissue_ids, mrs=all_mrs)
        if self.has_internal:
            jissues.append(JIssue.new_internal(mrs=all_mrs))
        if self.has_untagged:
            jissues.append(JIssue.new_untagged(mrs=all_mrs))
        return jissues

    @property
    def issues_with_scopes(self):
        """Return the list of JIssue objects after doing tests."""
        jissues = self.jissues
        for jissue in jissues:
            jissue.set_scope()
        return jissues

    @property
    def cves(self):
        """Return the list of Cve objects derived from this MR's description only."""
        # In other words, CVE tags in the descriptions of dependency MRs are not considered.
        return make_jissues(self.description.cve, mrs=[self] + self.depends_mrs) if \
            self.description else []

    @property
    def cves_with_scopes(self):
        """Return the list of Cves objects after doing tests."""
        cves = self.cves
        for cve in cves:
            cve.set_scope()
        return cves

    @property
    def first_dep_sha(self):
        """Return the sha of the first commit whose JIRA tag refers to a Depends."""
        if not self.description or not self.description.depends:
            return ''
        return next((sha for sha, commit in self.commits.items() for ji_id in
                     self.description.depends if ji_id in commit.jissue), '')

    @property
    def has_internal(self):
        """Return True if the MR description or any commits are marked INTERNAL."""
        return any(description.marked_internal for description in self.all_descriptions)

    @property
    def has_untagged(self):
        """Return True if any commits are UNTAGGED."""
        return not all(description.jissue or description.bugzilla or description.marked_internal for
                       description in self.commits.values())

    def new_depends_mr(self, mr_id):
        """Return a new MR object with is_dependency set to True."""
        return MR(graphql=self.graphql, projects=self.projects, namespace=self.namespace,
                  mr_id=mr_id, is_dependency=True)

    @property
    def project(self):
        """Return the project associated with the MR, if any."""
        return self.branch.project if self.branch else None

    def get_depends_mrs(self):
        # pylint: disable=protected-access
        """Populate depends_mrs with MR objects derived from this MR's Description.depends_mrs."""
        depends_mr_ids = self.description.depends_mrs.copy()
        loop_limit = 10
        while loop_limit and depends_mr_ids:
            mr_id = depends_mr_ids.pop()
            loop_limit -= 1
            if mr_id in [mr.mr_id for mr in self.depends_mrs]:
                LOGGER.warning('Dependency MR %s already exists.', mr_id)
                continue
            new_mr = self.new_depends_mr(mr_id)
            self.depends_mrs.append(new_mr)
            depends_mr_ids.update(new_mr.description.depends_mrs)
            # Any JIRA IDs referenced in a dependency MR should be treated as if they were listed
            # in the dependant MR's description.
            self.description._depends.update(new_mr.description.jissue | new_mr.description.depends)


class TagRow(table.TableRow):
    # pylint: disable=too-few-public-methods
    """Generic functions for all Tag TableRow objects."""

    @staticmethod
    def _format_cve(cve_name):
        """Return the cve_name as a markdown link."""
        def make_cve_link(cve_match):
            """Return the CVE in the match object as a markdown link."""
            return f'[{cve_match.group()}]({defs.JIRA_SERVER}browse/{cve_match.group()})'

        return re.sub(r'CVE-\d{4}-\d{4,7}', make_cve_link, cve_name)

    @staticmethod
    def _format_JIRA_Issues(jissue):
        """Format the JIRA Issue field."""
        status_str = jissue.ji_status.name
        if jissue.ji_status is defs.JIStatus.CLOSED:
            status_str += f': {jissue.ji_resolution.name}'
        return f'{jissue.id} ({status_str})' if isinstance(jissue.id, str) else jissue.id

    def _format_CVEs(self, cves):
        """Format the CVEs field."""
        if not cves:
            cve_str = 'None'
        elif isinstance(cves, str):
            cve_str = self._format_cve(cves)
        else:
            cve_str = ''
            for cve in cves:
                cve_str += f'{self._format_cve(cve)}<br>'
        return cve_str

    @staticmethod
    def _format_Commits(commits):
        """Format the list of commits."""
        if len(commits) > defs.MAX_COMMITS_PER_COMMENT_ROW:
            commits = list(commits)[:defs.MAX_COMMITS_PER_COMMENT_ROW]
            commits.append('(...)')
        return commits if commits else 'None'

    @staticmethod
    def _format_Notes(notes):
        """Format the Notes field."""
        return '<br>'.join(f'See {note}' for note in notes) if notes else '-'

    @staticmethod
    def _format_Policy_Check(issue):
        """Create a string for the Policy Check field."""
        check_passed, check_msg = issue.policy_check_ok
        if check_passed is True:
            policy_str = 'Passed'
        elif check_passed is False:
            policy_str = 'Failed:<br>' + check_msg
        elif check_passed is None:
            policy_str = check_msg
        return policy_str


class IssueRow(TagRow):
    """Format a TableRow for JIssue objects."""

    def __init__(self):
        """Init."""
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    def populate(self, tag, footnotes):
        """Populate the row with data from a JIssue tag."""
        jtag = f'[{tag.id}]({defs.JIRA_SERVER}browse/{tag.id}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('Commits', tag.commits)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Readiness', tag.scope.name)
        self.set_value('Policy_Check', tag)
        self.set_value('Notes', footnotes)
        return self


class CveRow(TagRow):
    """Format a TableRow for Cve objects."""

    def __init__(self):
        """Init."""
        self.CVEs = ''
        self.Priority = ''
        self.Commits = ''
        self.Clones = ''
        self.Readiness = ''
        self.Notes = ''

    @staticmethod
    def _format_Clones(cve):
        """Format the Clones field."""
        # If no parent_mr we can't proceed.
        if not cve.parent_mr:
            return 'Unknown'
        # Skip over low prio CVEs and rhel-6 (it has no zstreams)
        if cve.ji_priority < defs.JIPriority.HIGH or cve.parent_mr.project.name == 'rhel-6':
            return 'N/A'
        if not (clones := cve.ji_depends_on):
            return 'None'
        parent_clone = next((issue for issue in clones if issue.ji_branch == cve.parent_mr.branch),
                            None)
        clones_str = ''
        for clone in clones:
            target = clone.ji_branch.internal_target_release if \
                clone.ji_branch.internal_target_release else \
                clone.ji_branch.zstream_target_release
            if target == clone.ji_branch.zstream_target_release and target.endswith('0'):
                target = target[:-1] + 'z'
            status = clone.ji_status.name
            if clone.ji_status is defs.JIStatus.CLOSED:
                status += f' {clone.ji_resolution.name}'
            clone_str = f'{target}: {clone.id} ({status})'
            clones_str += f'{clone_str}<br>' if parent_clone != clone else f'**{clone_str}**<br>'
        return clones_str

    def populate(self, cve, footnotes):
        """Populate the row with data from a CVE JIRA Issue."""
        self.set_value('CVEs', cve.cve_ids)
        self.set_value('Priority', cve.ji_priority.name.capitalize())
        self.set_value('Commits', cve.commits)
        self.set_value('Clones', cve)
        self.set_value('Readiness', cve.scope.name)
        self.set_value('Notes', footnotes)
        return self


class DepRow(TagRow):
    """Format a TableRow for JIssue objects that are dependencies."""

    def __init__(self):
        """Init."""
        self.MR = ''
        self.JIRA_Issue = ''
        self.CVEs = ''
        self.Commits = ''
        self.Readiness = ''
        self.Policy_Check = ''
        self.Notes = ''

    @staticmethod
    def _format_MR(jissue):
        """Set the MR field."""
        return f'!{jissue.mr.mr_id} ({jissue.mr.branch.name})' if jissue.mr else '?'

    def populate(self, tag, footnotes):
        """Populate the row with data from a JIssue tag."""
        jtag = f'[{tag.id}]({defs.JIRA_SERVER}browse/{tag.id}) ({tag.ji_status.name})'
        self.set_value('JIRA_Issue', jtag)
        self.set_value('MR', tag)
        self.set_value('CVEs', tag.ji_cves)
        self.set_value('Commits', tag.commits)
        self.set_value('Notes', footnotes)
        if tag.mr.state is not defs.MrState.MERGED:
            self.set_value('Readiness', tag.scope.name)
            self.set_value('Policy_Check', tag)
        return self


# All the (non-jira) data we need to process the MR in one shot. Maybe.
JIRAHOOK_QUERY_BASE = """
query mrData($mr_id: String!, $namespace: ID!, $first: Boolean = true, $after: String = "") {
  ...CurrentUser @include(if: $first)
  project(fullPath: $namespace) {
    id @include(if: $first)
    mr: mergeRequest(iid: $mr_id) {
      ...MrCommits
      ...MrFiles @include(if: $first)
      title @include(if: $first)
      description @include(if: $first)
      id @include(if: $first)
      state @include(if: $first)
      draft @include(if: $first)
      targetBranch @include(if: $first)
      headPipeline @include(if: $first) {
        finishedAt
      }
    }
  }
}
"""

JIRAHOOK_QUERY = (JIRAHOOK_QUERY_BASE +
                  fragments.CURRENT_USER +
                  fragments.MR_COMMITS +
                  fragments.MR_FILES +
                  fragments.GL_USER)


def find_needed_footnotes(jissue_list):
    """Return a dict of test_name: test.notes needed for the given list of Issues."""
    return {test_name: next((test.note for test in jissue.test_list if test.__name__ == test_name))
            for jissue in jissue_list for test_name in jissue.failed_tests}


def create_table(row_class, jissue_list):
    """Return a Table object populated with the given row class."""
    footnotes = find_needed_footnotes(jissue_list) if jissue_list else {}
    tag_table = table.Table(footnote_list=footnotes.values())
    for ji in jissue_list:
        note_indexes = sorted([list(footnotes.keys()).index(test) + 1 for test in ji.failed_tests])
        row = row_class().populate(ji, note_indexes)
        tag_table.add_row(row)
    return tag_table


def create_ji_table(row_class, jissue_list):
    """Return a Table object populated with the given row class."""
    footnotes = find_needed_footnotes(jissue_list) if jissue_list else {}
    tag_table = table.Table(footnote_list=footnotes.values())
    for jissue in jissue_list:
        note_idx = sorted([list(footnotes.keys()).index(test) + 1 for test in jissue.failed_tests])
        row = row_class().populate(jissue, note_idx)
        tag_table.add_row(row)
    return tag_table


def generate_comment(target_branch, mr_scope, tables):
    """Generate the comment string."""
    status = 'fails' if mr_scope < defs.MrScope.READY_FOR_QA else 'passes'
    label = mr_scope.label("JIRA")
    total_rows = len(tables[0]) + len(tables[1]) + len(tables[2])
    issue_table = str(tables[0])
    dep_table = str(tables[1])
    cve_table = str(tables[2])

    post = COMMENT_TITLE + '\n\n'

    post += f'Target Branch: {target_branch}  \n\n'
    post += (f'This merge request **{status}** jirahook validation: '
             f'~"{label}"  \n\n')

    if total_rows > 10:
        post += '<details><summary>Click to expand</summary>  \n\n'
    if issue_table:
        post += '##### JIRA Issue tags:  \n' + issue_table + '\n'
    if dep_table:
        post += '##### Depends tags:  \n' + dep_table + '\n'
    if cve_table:
        post += '##### CVE tags:  \n' + cve_table + '\n'
    if total_rows > 10:
        post += '</details>  \n'

    post += '\n' + COMMENT_FOOTER
    return post


def update_gitlab(this_mr, labels, comment, post_comment=False):
    """Wrap old API stuff to set the labels and check dependencies; Return True if MR is ready."""
    gl_instance = get_instance(defs.GITFORGE)
    gl_project = gl_instance.projects.get(this_mr.namespace)
    gl_mergerequest = gl_project.mergerequests.get(this_mr.mr_id)

    # Post the comment using REST since graphql didn't work?
    if post_comment:
        LOGGER.info('Posting comment using REST:\n%s', comment)
        common.update_webhook_comment(gl_mergerequest, this_mr.graphql.username,
                                      COMMENT_TITLE, comment)

    cur_labels = common.add_label_to_merge_request(gl_project, this_mr.mr_id, labels)
    LOGGER.debug('Current MR labels: %s', cur_labels)
    common.add_merge_request_to_milestone(this_mr.branch, gl_project, gl_mergerequest)


def generate_dependencies_label(first_dep_sha):
    """Generate the Dependencies label from the given sha, if any."""
    scope = first_dep_sha[:12] if first_dep_sha else defs.READY_SUFFIX
    return f'Dependencies::{scope}'


def get_lowest_scope(issues):
    """Return the lowest scope of all the JIssues/CVEs."""
    scope = defs.MrScope.READY_FOR_MERGE
    for issue in issues:
        scope = min(scope, issue.scope)
    return scope


def mark_issues_as_in_progress(issue_list, pipeline_timestamp):
    """Move the given JIRA issues state to Development."""
    to_move = libjira.issues_to_move_to_in_progress(issue_list, pipeline_timestamp)
    libjira.update_issue_status(to_move, defs.JIStatus.DEVELOPMENT)


def _process_link_removals(mr_id, namespace, description, action, changes):
    """Process any necessary gitlab link removals from jira issues."""
    mr_issues = set(Description(description).jissue)

    # If this is an MR being closed, unlink this MR from all referenced issues
    if action == 'close':
        LOGGER.info("Removing links from issues due to MR being closed")
        libjira.remove_gitlab_link_in_issues(mr_id, namespace, mr_issues)
        return

    # Remove MR links in issues dropped from the MR description
    if changes and 'description' in changes:
        old_issues = set(Description(changes['description']['previous']).jissue)
        removed_issues = old_issues - mr_issues
        LOGGER.info("Old issues: %s, to remove: %s", old_issues, removed_issues)
        libjira.remove_gitlab_link_in_issues(mr_id, namespace, removed_issues)


def process_mr(namespace, mr_id, graphql, projects):
    # pylint: disable=too-many-arguments,too-many-locals
    """Process the given MR."""
    projects.do_load_policies()
    libjira.fetch_issues([], clear_cache=True)  # make sure we begin with an empty issue_cache

    # Fetch and parse MR data.
    this_mr = MR(graphql, projects, namespace, mr_id)

    # Skip funny MRs.
    if this_mr.state in (defs.MrState.MERGED, defs.MrState.CLOSED):
        LOGGER.info('This MR is %s, nothing to do.', this_mr.state.name.title())
        return
    if not this_mr.description:
        LOGGER.info('This MR has no description, nothing to do.')
        return
    if not this_mr.commits:
        LOGGER.info('This MR has no commits, nothing to do.')
        return

    mr_issues = [jissue.ji for jissue in this_mr.jissues if jissue.mr is this_mr and jissue.ji]

    # Hack to not run when there are bugzilla refs but no jira issues
    bugs = this_mr.description.bugzilla
    if bugs and not mr_issues:
        LOGGER.info("No jira issues provided, but we have bugzillas: %s", bugs)
        comment = COMMENT_TITLE + '\n\n'
        comment += "Nothing to report, this MR is using bugzilla instead of jira."
        update_gitlab(this_mr, [f'JIRA::{defs.READY_SUFFIX}'], comment, True)
        return

    if not this_mr.is_draft:
        # Move mr_issues to Development.
        mark_issues_as_in_progress(mr_issues, this_mr.pipeline_finished)
        # Add cross-reference to GitLab MR in JIRA Issue
        libjira.add_gitlab_link_in_issues(mr_issues, this_mr)
    else:
        LOGGER.info('MR is in draft state, not moving JIRA Issues to Development.')

    # Get data for Depends: MRs.
    this_mr.get_depends_mrs()

    # Get the JIRA issue lists with their scopes set
    issues = this_mr.issues_with_scopes
    for issue in issues:
        LOGGER.info("Issue: %s", issue)
    cves = this_mr.cves_with_scopes

    # Generate a tuple of results tables: Issues, Deps, and CVEs.
    tables = (create_ji_table(IssueRow, [issue for issue in issues if issue.mr is this_mr]),
              create_table(DepRow, [issue for issue in issues if issue.mr is not this_mr]),
              create_table(CveRow, cves)
              )

    # Post the comment.
    # For MRs with lots of events this may cause a timeout. In that case set post_comment=True and
    # the later update_gitlab() call will post the comment using REST.
    # Sigh.
    mr_scope = get_lowest_scope(issues + cves)
    comment = generate_comment(this_mr.branch.name, mr_scope, tables)
    post_comment = False
    try:
        graphql.replace_note(namespace, mr_id, graphql.username, COMMENT_TITLE, comment)
    except gql.transport.exceptions.TransportQueryError:
        post_comment = True

    # Update the MR labels
    # dependencies_label = generate_dependencies_label(this_mr.first_dep_sha)
    update_gitlab(this_mr, [mr_scope.label('JIRA')], comment, post_comment)


def process_umb_event(body, headers, **_):
    """Process a message sent by the umb_bridge."""
    if headers['source'] != 'jira':
        LOGGER.info('Ignoring UMB event from non-jira source.')
        return
    namespace = body['mrpath'].split('!')[0]
    mr_id = int(body['mrpath'].split('!')[-1])
    graphql = GitlabGraph(get_user=True)
    projects = Projects()
    process_mr(namespace, mr_id, graphql, projects)


def process_mr_event(msg, graphql, projects, **_):
    # pylint: disable=too-many-boolean-expressions,too-many-locals
    """Process an MR event."""
    code_changed = common.mr_action_affects_commits(msg)
    changes = msg.payload.get('changes')
    description_changed = 'description' in changes
    is_draft, draft_changed = common.draft_status(msg.payload)
    now_ready = not is_draft and draft_changed
    label_changed = common.has_label_prefix_changed(msg.payload['changes'], 'JIRA::')
    has_jira_label = any(label['title'] for label in msg.payload['labels'] if
                         label['title'].startswith('JIRA::'))
    action = msg.payload['object_attributes'].get('action')
    mr_closed = action == 'close'

    if not code_changed and not description_changed and not label_changed and not now_ready \
       and has_jira_label and not mr_closed:
        LOGGER.info('MR event does not indicate any relevant changes, ignoring.')
        return

    namespace = msg.payload['project']['path_with_namespace']
    mr_id = msg.payload['object_attributes']['iid']
    description = msg.payload['object_attributes'].get('description')

    if mr_closed or description_changed:
        _process_link_removals(mr_id, namespace, description, action, changes)

    process_mr(namespace, mr_id, graphql, projects)


def process_note_event(msg, graphql, projects, **_):
    """Process a note event."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'],
                                           ['jira', 'jirahook']):
        LOGGER.info('Note event does not request evaluation, ignoring.')
        return
    process_mr(msg.payload['project']['path_with_namespace'],
               msg.payload['merge_request']['iid'],
               graphql, projects)


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
    defs.UMB_BRIDGE_MESSAGE_TYPE: process_umb_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('JIRAHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False, get_graphql_instance=True)


if __name__ == "__main__":
    main(sys.argv[1:])
