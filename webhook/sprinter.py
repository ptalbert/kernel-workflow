"""Implement the CKI GitLab workflow."""
import os
import re
import sys
import typing

from cki_lib import gitlab
from cki_lib import logger
from cki_lib import misc
import dateutil.parser
import yaml

from . import common

LOGGER = logger.get_logger('cki.webhook.sprinter')

LABELS_FORMAT = 'CWF::Sprint::{year}-week-{week}'

CONFIG = yaml.safe_load(os.environ.get('SPRINTER_CONFIG', '{}'))


def get_label_name(date):
    """Return label name for current sprint."""
    date = dateutil.parser.parse(date).isocalendar()
    return LABELS_FORMAT.format(year=date.year, week=date.week)


def run_issue_commands(gl_project, issue_iid, cmds):
    """Run slash commands on a GitLab issue."""
    LOGGER.info('Running slash commands on issue %s/%s: %s',
                gl_project.web_url, issue_iid, ' '.join(cmds))
    if misc.is_production():
        gl_project.issues.get(issue_iid, lazy=True).notes.create({'body': '\n'.join(cmds)})


def add_labels_to_issue(
    project_url: str,
    issue_iid: int,
    label_names: list[str],
    *,
    labels: list[dict[typing.Any]] | None = None,
    level: str | None = None,
) -> None:
    """Add labels to a GitLab issue."""
    LOGGER.info('Adding labels %s to %s/%s', label_names, project_url, issue_iid)
    gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
    with gl_instance:
        label_dicts = common.validate_labels(label_names)
        # pylint: disable=protected-access  # api needs to stabilize before move to common
        label_cmds = common._add_label_quick_actions(gl_project, label_dicts, level=level)
        run_issue_commands(gl_project, issue_iid, label_cmds)

    if labels is not None:
        labels.extend({'title': name}
                      for name in set(label_names) - set(label['title'] for label in labels))


def remove_labels_from_issue(
    project_url: str,
    issue_iid: int,
    label_names: list[str],
    *,
    labels: list[dict[typing.Any]] | None = None,
) -> None:
    """Remove labels from a GitLab issue."""
    LOGGER.info('Removing labels %s from %s/%s', label_names, project_url, issue_iid)
    gl_instance, gl_project = gitlab.parse_gitlab_url(project_url)
    with gl_instance:
        run_issue_commands(gl_project, issue_iid, [f"/unlabel \"{name}\"" for name in label_names])
    if labels is not None:
        labels[:] = [label for label in labels if label['title'] not in label_names]


def process_issue(_, message, **__):
    """Check issues."""
    project_url = misc.get_nested_key(message.payload, 'project/web_url')
    if (rule := enabled_for_project(CONFIG.get('sprint_labels', []), project_url)) is not None:
        check_sprint_label(project_url, message.payload, rule)
    if (rule := enabled_for_project(CONFIG.get('incident_labels', []), project_url)) is not None:
        check_incident_labels(project_url, message.payload, rule)
    if (rule := enabled_for_project(CONFIG.get('type_labels', []), project_url)) is not None:
        check_type_labels(project_url, message.payload, rule)


def check_sprint_label(project_url, payload, rule):
    """Add sprint label to closed issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    closed_at = misc.get_nested_key(payload, 'object_attributes/closed_at')
    level = rule.get('level')

    sprint_labels = [title for label in labels
                     if (title := label['title']).startswith('CWF::Sprint::')]

    if action == 'reopen':
        if sprint_labels:
            remove_labels_from_issue(project_url, issue_iid, sprint_labels,
                                     labels=labels)
    if action == 'close':
        add_labels_to_issue(project_url, issue_iid, [get_label_name(closed_at)],
                            labels=labels,
                            level=level)


def check_incident_labels(project_url, payload, rule):
    """Ensure correct incident labels on issues."""
    changes = payload.get('changes', {})
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    state = misc.get_nested_key(payload, 'object_attributes/state')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    level = rule.get('level')

    had_type_incident = any(label['title'] == 'CWF::Type::Incident'
                            for label in misc.get_nested_key(changes, 'labels/previous', []))
    has_type_incident = any(label['title'] == 'CWF::Type::Incident' for label in labels)
    incident_labels = [title for label in labels
                       if (title := label['title']).startswith('CWF::Incident::')]

    if action == 'close':
        if incident_labels:
            remove_labels_from_issue(project_url, issue_iid, incident_labels)
    if action in {'open', 'reopen'} or (action == 'update' and state == 'opened'):
        if incident_labels and not has_type_incident and had_type_incident:
            remove_labels_from_issue(project_url, issue_iid, incident_labels,
                                     labels=labels)
        elif incident_labels and not has_type_incident and not had_type_incident:
            add_labels_to_issue(project_url, issue_iid, ['CWF::Type::Incident'],
                                labels=labels,
                                level=level)
        elif has_type_incident and not incident_labels:
            add_labels_to_issue(project_url, issue_iid, ['CWF::Incident::Active'],
                                labels=labels,
                                level=level)


def check_type_labels(project_url, payload, rule):
    """Ensure correct type labels on issues."""
    issue_iid = misc.get_nested_key(payload, 'object_attributes/iid')
    state = misc.get_nested_key(payload, 'object_attributes/state')
    action = misc.get_nested_key(payload, 'object_attributes/action')
    labels = misc.get_nested_key(payload, 'object_attributes/labels')
    level = rule.get('level')
    type_label = rule.get('type')

    has_type_labels = any(label['title'].startswith('CWF::Type::') for label in labels)

    if action in {'open', 'reopen'} or (action == 'update' and state == 'opened'):
        if not has_type_labels and type_label:
            add_labels_to_issue(project_url, issue_iid, [f'CWF::Type::{type_label}'],
                                labels=labels,
                                level=level)


def enabled_for_project(
    rules: list[dict[str, str]],
    project_url: str,
) -> typing.Optional[dict[str, typing.Any]]:
    """Check whether the project is enabled for the given rules.

    Returns the rule that enables it, or None if the project is disabled.
    """
    for rule in rules:
        if rule_project := rule.get('project'):
            if project_url == rule_project:
                return rule if rule.get('enabled', True) else None
            continue
        if rule_group := rule.get('group'):
            if project_url.startswith(rule_group + '/'):
                return rule if rule.get('enabled', True) else None
            continue
        return {**rule, 'enabled': True} if rule.get('enabled', True) else None
    return {'enabled': True}


def process_mr(_, message, **__):
    """Check MRs."""
    project_url = misc.get_nested_key(message.payload, 'project/web_url')
    if (rule := enabled_for_project(CONFIG.get('issue_checks', []), project_url)) is not None:
        check_mr_issue_ref(project_url, message.payload, rule)


def check_mr_issue_ref(project_url, payload, rule):
    """Check MRs for GitLab issue links."""
    changes = payload['changes']
    action = misc.get_nested_key(payload, 'object_attributes/action')
    description = misc.get_nested_key(payload, 'object_attributes/description')
    iid = misc.get_nested_key(payload, 'object_attributes/iid')
    level = rule.get('level')

    description_changed = 'description' in changes
    label_changed = common.has_label_prefix_changed(changes, 'CWF::Issue::')
    if action != 'open' and not description_changed and not label_changed:
        LOGGER.info('MR event does not indicate any relevant changes, ignoring.')
        return

    regex = re.compile(r'https://gitlab[^ ]+/-/issues/\d+|(?:[^\s]+)?#\d+')
    labels = ['CWF::Issue::Missing' if regex.search(description) is None else 'CWF::Issue::OK']

    _, gl_project = gitlab.parse_gitlab_url(project_url)
    common.add_label_to_merge_request(gl_project, iid, labels,
                                      compute_mr_status_labels=False, level=level)


WEBHOOKS = {
    "issue": process_issue,
    'merge_request': process_mr,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SPRINTER')
    args = parser.parse_args(args)
    args.disable_inactive_branch_check = True
    args.disable_closed_status_check = True

    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
