"""Library for interacting with bugzilla."""
from datetime import datetime
from os import environ

from bugzilla import Bugzilla
from cki_lib.logger import get_logger
from cki_lib.misc import is_production
from cki_lib.retrying import retrying_on_exception
import prometheus_client

from webhook.defs import BZStatus
from webhook.defs import EXT_TYPE_URL

LOGGER = get_logger('cki.webhook.libbz')

MAX_CONNECTION_RETRIES = 2

METRIC_KWF_LIBBZ_MISSING_BUGS = prometheus_client.Counter(
    'kwf_libbz_missing_bugs', 'Number of times bugzilla api did not return any BZ data'
)

BUG_FIELDS = ['alias',
              'cf_internal_target_release',
              'cf_verified',
              'cf_zstream_target_release',
              'component',
              'depends_on',
              'external_bugs',
              'flags',
              'id',
              'priority',
              'product',
              'resolution',
              'status',
              'sub_component',
              'summary',
              'version'
              ]


def get_bzcon(api_key=environ.get('BUGZILLA_API_KEY'), host='bugzilla.redhat.com'):
    """Return a bugzilla.Bugzilla connection object."""
    return Bugzilla(host, api_key=api_key)


def _clear_bug_cache(bug_cache):
    """Clear the bug_cache list."""
    LOGGER.info('Clearing %s bugs from the cache.', len(bug_cache))
    bug_cache.clear()


def _update_bug_cache(new_bugs, bug_cache):
    """Put the new_bugs in the bug_cache."""
    for bug in new_bugs:
        if bug.id in bug_cache:
            LOGGER.warning('Bug #%s already in bug_cache.', bug.id)
        bug_cache[bug.id] = bug


@retrying_on_exception(ConnectionError, retries=MAX_CONNECTION_RETRIES, initial_delay=1)
def _getbugs(bzcon, bug_list):
    """Get bug data from bugzilla and return a list of Bug objects."""
    LOGGER.info('Fetching bugzilla data for these bugs: %s', bug_list)
    return bzcon.getbugs(bug_list, include_fields=BUG_FIELDS)


def _get_bugs_by_id(bug_ids, bug_cache):
    """Return the set of bugs from the cache which match on bz_id."""
    return {bug for bug in bug_cache.values() if bug.id in bug_ids}


def _get_bugs_by_alias(alias_list, bug_cache):
    """Return the set of bugs from the cache which have an alias which is in alias_list."""
    return {bug for bug in bug_cache.values() for alias in alias_list if alias in bug.alias}


def get_missing_bugs(bug_names, bug_list):
    """Return the set of IDs that are not in the list of Bug objects."""
    missing_ids = {bz_id for bz_id in bug_names if isinstance(bz_id, int) and
                   bz_id not in [bug.id for bug in bug_list]}
    missing_aliases = {alias for alias in bug_names if not isinstance(alias, int) and
                       alias not in [alias for bug in bug_list for alias in bug.alias]}
    return missing_ids | missing_aliases


def fetch_bugs(bug_list, clear_cache=False, bug_cache={}):
    # pylint: disable=dangerous-default-value
    """Return a list of Bug objects; taken from the bug_cache dict or queried from bugzilla."""
    LOGGER.debug('Cache is %s with %s entries.}', id(bug_cache), len(bug_cache))
    if not bug_list and clear_cache:
        _clear_bug_cache(bug_cache)
        return set()
    if not bug_list:
        LOGGER.debug('Empty input bug_list, nothing to do.')
        return set()

    # Get a bugzilla connection object.
    bzcon = next(iter(bug_cache.values())).bugzilla if bug_cache else get_bzcon()
    if clear_cache:
        _clear_bug_cache(bug_cache)

    # Filter out bugs which already exist in the bug_cache
    to_do = get_missing_bugs(bug_list, bug_cache.values())
    if to_do != set(bug_list):
        LOGGER.debug('Using bug data from cache: %s', list(set(bug_list) - set(to_do)))
    # Fetch bug data and update the bug_cache with it
    if to_do:
        new_bugs = _getbugs(bzcon, to_do)
        _update_bug_cache(new_bugs, bug_cache)
    if missing_bzs := get_missing_bugs(bug_list, bug_cache.values()):
        LOGGER.warning('Bugzilla did not return data for these bugs: %s', missing_bzs)
        METRIC_KWF_LIBBZ_MISSING_BUGS.inc()

    bug_ids = [bz_id for bz_id in bug_list if isinstance(bz_id, int)]
    aliases = [bz_id for bz_id in bug_list if not isinstance(bz_id, int)]
    return list(_get_bugs_by_id(bug_ids, bug_cache) | _get_bugs_by_alias(aliases, bug_cache))


def bugs_with_lower_status(bug_list, status, min_status=BZStatus.NEW):
    """Return the list of bugs that have a status lower than the input status."""
    return [bug for bug in bug_list if min_status <= BZStatus.from_str(bug.status) < status]


def latest_failedqa_timestamp(bug_history):
    """Return the most recent timestamp in the history when FailedQA was added to cf_verified."""
    # Surely you would only call this if the bug currently has 'FailedQA' in cf_verified.
    timestamp = None
    for update in reversed(bug_history):
        if next((change for change in update['changes'] if
                 change['field_name'] == 'cf_verified' and
                 change['added'] == 'FailedQA'), None):
            # convert the goofy xmlrpc.client.DateTime to datatime
            timestamp = datetime.strptime(update['when'].value, '%Y%m%dT%H:%M:%S')
            LOGGER.debug('FailedQA last added %s', timestamp)
            return timestamp
    LOGGER.warning('Did not find FailedQA being added to cf_verified in history.')
    return None


def bugs_to_move_to_post(bug_list, mr_pipeline_timestamp):
    """Return the bugs from the input list that need to be moved to POST."""
    # For any input bug with FailedQA we check the timestamp when FailedQA was last added to the bug
    # against the mr_pipeline_timestamp and if the former is newer we pop it from the bugs_to_update
    # list.
    # mr_pipeline_timestamp is the datetime when the MR's head pipeline finished, if any.
    bugs_to_update = bugs_with_lower_status(bug_list, BZStatus.POST)
    if not (bz_ids_to_check := [bug.id for bug in bugs_to_update if 'FailedQA' in bug.cf_verified]):
        return bugs_to_update
    if not mr_pipeline_timestamp:
        raise ValueError("The MR doesn't have a completed pipeline but a Bug has already FailedQA?")
    histories = bug_list[0].bugzilla.bugs_history_raw(bz_ids_to_check)
    bz_ids_to_remove = []
    for bz_id in bz_ids_to_check:
        if not (bug_history := next((hist for hist in histories['bugs'] if hist['id'] == bz_id),
                                    None)):
            raise ValueError(f'Failed to fetch BZ history for {bz_id}')
        if failed_qa_timestamp := latest_failedqa_timestamp(bug_history['history']):
            if failed_qa_timestamp > mr_pipeline_timestamp:
                LOGGER.debug('Bug %s got FailedQA more recently than the latest pipeline.', bz_id)
                bz_ids_to_remove.append(bz_id)
    bugs_to_update = [bug for bug in bugs_to_update if bug.id not in bz_ids_to_remove]
    return bugs_to_update


def update_bug_status(bug_list, new_status, min_status=BZStatus.NEW):
    """Change the bug status to new_status if it is currently lower, otherwise do nothing."""
    # Do nothing if the current status is lower than min_status.
    # Returns the list of bug objects which have had their status changed.
    if not bug_list:
        LOGGER.info('No bugs to update status for.')
        return []
    if not (bugs_to_update := bugs_with_lower_status(bug_list, new_status, min_status)):
        LOGGER.info('All of these bugs have status of %s or higher: %s', new_status.name,
                    [bug.id for bug in bug_list])
        return []
    bug_ids = [bug.id for bug in bugs_to_update]
    LOGGER.info('Updating status for these bugs to %s: %s', new_status.name, bug_ids)
    if not is_production():
        for bug in bugs_to_update:
            bug.status = new_status.name
        return bugs_to_update
    bzcon = bug_list[0].bugzilla
    status_update = bzcon.build_update(status=new_status.name)
    update_result = bzcon.update_bugs(bug_ids, status_update)
    for bug in bugs_to_update:
        bz_result = next((bz for bz in update_result['bugs'] if bz['id'] == bug.id), None)
        if bz_result and bz_result['changes']['status']['added'] == new_status.name:
            bug.status = new_status.name
        else:
            raise ValueError(f'Failed to update bug status for {bug.id}: {bz_result}')
    return bugs_to_update


def _get_bz_mr_id(external_bugs, namespace):
    """Return the list of MR IDs in the BZs external trackers that match the namespace."""
    return {int(eb['ext_bz_bug_id'].split('/')[-1]) for eb in external_bugs if
            eb['type']['url'] == EXT_TYPE_URL and
            eb['ext_bz_bug_id'].startswith(f'{namespace}/-/merge_requests/')}


def resolve_bzs_to_mrs(bz_ids, namespace):
    """Return the set of MR IDs linked to the given bz_ids."""
    if not bz_ids or not namespace:
        return set()
    dep_mrs = set()
    for bz_bug in fetch_bugs(bz_ids):
        if not (mr_ids := _get_bz_mr_id(bz_bug.external_bugs, namespace)):
            LOGGER.info('Bug %s not linked to any MR in the %s namespace.', bz_bug.id, namespace)
            continue
        LOGGER.info('Bug %s resolved to MR(s) %s', bz_bug.id, mr_ids)
        dep_mrs.update(mr_ids)
    return dep_mrs


def get_depends_on_bzs(cve_ids, branch):
    """Return the list of depends_on bug objects from the CVEs which match the given branch."""
    depends_bz_ids = [bz_id for cve in fetch_bugs(cve_ids) for bz_id in cve.depends_on]
    attributes = {'component': branch.component,
                  'product': branch.project.product,
                  'version': branch.version}
    bug_list = filter_bug_list(fetch_bugs(depends_bz_ids), attributes)
    LOGGER.info('For CVEs %s found these bugs matching branch %s: %s', cve_ids, branch.name,
                [bug.id for bug in bug_list])
    return bug_list


def filter_bug_list(bug_list, attributes):
    """Return the bug objects from the input list that match on all the given attributes."""
    return [bug for bug in bug_list if
            all(getattr(bug, key) == value for key, value in attributes.items())]
