#!/usr/bin/env python
"""Weed out MRs that have no blocking comments but an Acks::Blocked label."""

import argparse
from importlib import import_module
import os

from cki_lib import logger
from cki_lib import misc

from webhook.graphql import GitlabGraph

LOGGER = logger.get_logger('utils.acks_blocked_check')

# Get the resolve details for every discussion of the given MR.
MR_QUERY = """
query mrData($namespace: ID!, $mr_id: String!, $after: String = "") {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      iid
      discussions(after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          resolvable
          resolved
        }
      }
    }
  }
}
"""

# Get all open MRs with an Acks::Blocked label and their discussion resolve details.
BLOCKED_QUERY = """
query mrData($namespace: ID!, $after: String = "") {
  %s(fullPath: $namespace) {
    mergeRequests(state: opened, labels: "Acks::Blocked", after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        webUrl
        project {
          fullPath
        }
        discussions {
          pageInfo {
            hasNextPage
            endCursor
          }
          nodes {
            resolvable
            resolved
          }
        }
      }
    }
  }
}
"""


def mr_has_blocked_discussions(graphql, mr) -> bool:
    """Return True if the MR has resolveable discussions which are not resolved."""
    if not misc.get_nested_key(mr, 'discussions/nodes'):
        return False

    if misc.get_nested_key(mr, 'discussions/pageInfo/hasNextPage'):
        result = graphql.check_query_results(
            graphql.client.query(MR_QUERY,
                                 variable_values={
                                     'namespace': misc.get_nested_key(mr, 'project/fullPath'),
                                     'mr_id': mr['iid'],
                                 },
                                 paged_key='project/mergeRequest/discussions'),
            check_keys={'project'})
        mr = misc.get_nested_key(result, 'project/mergeRequest')

    mr_is_blocked = any(
        d['resolvable'] and not d['resolved']
        for d in misc.get_nested_key(mr, 'discussions/nodes')
    )

    LOGGER.debug('MR %s: %s discussions, unresolved: %s',
                 mr['iid'],
                 len(misc.get_nested_key(mr, 'discussions/nodes')),
                 mr_is_blocked)
    return mr_is_blocked


def get_blocked_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR IDs with the Acks::Blocked label & no blocked discussions."""
    result = graphql.check_query_results(
        graphql.client.query(BLOCKED_QUERY % namespace_type,
                             variable_values={'namespace': namespace},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    mr_list = [mr['webUrl']
               for mr in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes')
               if not mr_has_blocked_discussions(graphql, mr)]

    LOGGER.info('%s %s MRs: found %s (%s not blocked)',
                namespace_type.title(),
                namespace,
                len(misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes')),
                len(mr_list))
    return mr_list


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Confirm Acks::Blocked label')

    # Global options
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    return parser.parse_args()


def main():
    """Find Acks::Blocked MRs and check for blocking comments.."""
    args = _get_parser_args()
    LOGGER.info('Finding blocked MRs...')

    graphql = GitlabGraph()
    mrs_with_resolved_discussions = []
    for group in args.groups:
        mrs_with_resolved_discussions.extend(get_blocked_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_with_resolved_discussions.extend(get_blocked_mrs(graphql, project, 'project'))

    if not mrs_with_resolved_discussions:
        LOGGER.info('All MRs with Acks::Blocked label have a blocked discussion, nothing to do.')
        return

    ack_nack = import_module('webhook.ack_nack')
    for mr in mrs_with_resolved_discussions:
        LOGGER.info('Triggering ack_nack for %s', mr)
        args = ['--merge-request', mr]
        ack_nack.main(args)


if __name__ == '__main__':
    main()
