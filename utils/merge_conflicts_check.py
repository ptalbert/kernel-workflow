#!/usr/bin/env python
"""Look for MRs that cannot be merged and have stale Merge::OK labels."""

import argparse
import os
import subprocess

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.misc import sentry_init
import sentry_sdk

from webhook import defs
from webhook import fragments
from webhook.common import update_webhook_comment
from webhook.graphql import GitlabGraph
from webhook.mergehook import _git
from webhook.mergehook import _git_branch_copy
from webhook.mergehook import _git_branch_delete
from webhook.mergehook import _git_merge
from webhook.mergehook import _git_reset
from webhook.mergehook import build_mr_conflict_string
from webhook.mergehook import check_for_merge_conflicts
from webhook.mergehook import clean_up_temp_merge_branch
from webhook.mergehook import format_conflict_info
from webhook.mergehook import mr_not_mergeable

LOGGER = logger.get_logger('utils.merge_conflicts_check')

# Get all open MRs.
MR_QUERY_BASE = """
query mrData($namespace: ID!, $first: Boolean = true, $after: String = "") {
  %s(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after) {
      pageInfo {hasNextPage endCursor}
      nodes {iid author{username} title targetBranch project{fullPath} ...MrLabels}
    }
  }
}
"""

MR_QUERY = MR_QUERY_BASE + fragments.MR_LABELS


def get_open_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR IDs with the Acks::OK label."""
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY % namespace_type, variable_values={'namespace': namespace},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    if result and result[namespace_type] is None:
        raise RuntimeError(f"Namespace '{namespace}' is not visible!")
    mrs = {}
    # Sort MRs into namespace/target_branch buckets
    for mr in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes'):
        namespace = mr['project']['fullPath']
        target_branch = mr['targetBranch']
        ns_tb = f'{namespace}_{target_branch}'
        if ns_tb in mrs:
            mrs[ns_tb].append(mr)
            continue
        mrs[ns_tb] = [mr]

    LOGGER.debug('%s MRs: found %s', namespace_type.title(), mrs)
    return mrs


def prep_branch_for_merges(rhkernel_src, gl_project, target_branch):
    """Prepare a git branch/worktree for doing bulk merges."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    proj_target_branch = f"{gl_project.name}/{target_branch}"
    merge_branch = f"{gl_project.name}-{target_branch}-megamerge"
    worktree_dir = f"/{'/'.join(rhkernel_src.strip('/').split('/')[:-1])}/{merge_branch}/"
    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    _git(rhkernel_src, ['worktree', 'add', '-B', merge_branch, worktree_dir, proj_target_branch])
    return worktree_dir


def try_merging_all(mrs, pname, target_branch, reset_branch, merge_dir):
    """Try merging all MRs together both directions to identify MRs with conflicts."""
    conflict_mrs = []
    interim_branch = f"{reset_branch}-interim"

    for mr in list(reversed(mrs)):
        _git_branch_copy(merge_dir, interim_branch)
        mr_id = int(mr['iid'])
        try:
            _git_merge(merge_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError:
            _git_reset(merge_dir, interim_branch)
            if mr not in conflict_mrs:
                conflict_mrs.append(mr)
        _git_branch_delete(merge_dir, interim_branch)

    _git_reset(merge_dir, reset_branch)
    for mr in mrs:
        _git_branch_copy(merge_dir, interim_branch)
        mr_id = int(mr['iid'])
        try:
            _git_merge(merge_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError:
            _git_reset(merge_dir, interim_branch)
            if mr not in conflict_mrs:
                conflict_mrs.append(mr)
        _git_branch_delete(merge_dir, interim_branch)

    _git_reset(merge_dir, reset_branch)

    return conflict_mrs


def check_pending_conflicts(this_mr, pname, conflict_mrs, merge_dir, reset_branch):
    """Check for direct conflicts with other MRs with merge issues."""
    LOGGER.debug("Checking other pending %s MRs for conflicts with MR %s",
                 this_mr['targetBranch'], this_mr['iid'])

    save_branch = f"{reset_branch}-{this_mr['iid']}"
    _git_branch_copy(merge_dir, save_branch)

    conflicts = []
    target_branch = f"{pname}/{this_mr['targetBranch']}"
    for conflict_mr in conflict_mrs:
        mr_id = int(conflict_mr['iid'])
        if mr_id == this_mr['iid']:
            continue

        # First, make sure this MR can actually be merged by itself
        if mr_not_mergeable(merge_dir, target_branch, f'{pname}/merge-requests/{mr_id}'):
            LOGGER.debug("MR %d can't be merged by itself, skipping conflict check", mr_id)
            continue

        # Now, try to merge it on top of this MR, so we can see if it has any conflicts
        _git_reset(merge_dir, save_branch)
        try:
            _git_merge(merge_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError as err:
            conflicts.append(build_mr_conflict_string(conflict_mr))
            conflicts.append(err.output)

    _git_branch_delete(merge_dir, save_branch)
    return conflicts


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check open MRs for merge conflicts')

    # Global options
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--since', default=24,
                        help='check if target branch changed since X hours ago.',
                        type=int, required=False)
    parser.add_argument('-r', '--rhkernel-src', default=os.environ.get('RHKERNEL_SRC', ''),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('-T', '--testing', action='store_true', default=False,
                        help="Run in testing mode: do not update gitlab labels")
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(args):
    """Find open MRs and check for merge conflicts."""
    if not args.rhkernel_src:
        LOGGER.warning("No path to RH Kernel source git found, aborting!")
        return

    LOGGER.info('Fetching from git remotes to ensure we have the latest data needed')
    _git(args.rhkernel_src, ['fetch', '--all'])

    LOGGER.info('Finding open MRs for groups %s and projects %s', args.groups, args.projects)
    graphql = GitlabGraph(get_user=True)
    gl_instance = get_instance(defs.GITFORGE)

    mrs_to_conflict_check = {}
    for group in args.groups:
        mrs_to_conflict_check.update(get_open_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_to_conflict_check.update(get_open_mrs(graphql, project, 'project'))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    gl_projects = {}
    merge_dirs = {}
    # Build per-project/target_branch lists of MRs
    for mr_ns_tb, mrs in mrs_to_conflict_check.items():
        namespace = mr_ns_tb.split('_')[0]
        if not (gl_project := gl_projects.get(namespace)):
            gl_project = gl_instance.projects.get(namespace)
            gl_projects[namespace] = gl_project
        target_branch = mr_ns_tb.split('_')[1]
        pname = gl_project.name
        proj_tb = f'{gl_project.name}_{target_branch}'
        if not (merge_dir := merge_dirs.get(proj_tb)):
            merge_dir = prep_branch_for_merges(args.rhkernel_src, gl_project, target_branch)
            merge_dirs[proj_tb] = merge_dir

        merge_base = merge_dir.strip('/').split('/')[-1]
        reset_branch = f"{merge_base}-reset"
        _git_branch_copy(merge_dir, reset_branch)

        conflict_mrs = try_merging_all(mrs, pname, target_branch, reset_branch, merge_dir)

        for mr in mrs:
            if mr in conflict_mrs:
                continue
            LOGGER.debug('Merge OK for %s/%s MR %s from %s',
                         pname, target_branch, mr['iid'], mr['author']['username'])
            mr_labels = mr['labels']['nodes']
            gl_mergerequest = gl_project.mergerequests.get(mr['iid'])
            if not args.testing and {'title': f'Merge::{defs.READY_SUFFIX}'} not in mr_labels:
                gl_mergerequest.notes.create({'body': f'/label Merge::{defs.READY_SUFFIX}'})

        for mr in conflict_mrs:
            mr_id = mr['iid']
            gl_mergerequest = gl_project.mergerequests.get(mr_id)
            mr_labels = mr['labels']['nodes']
            LOGGER.debug('Checking for merge conflicts on %s MR %s', proj_tb, mr_id)

            _git_reset(merge_dir, reset_branch)
            conflict_info = check_for_merge_conflicts(gl_project, gl_mergerequest, merge_dir)
            if conflict_info:
                merge_label = defs.MERGE_CONFLICT_LABEL
                note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
                note += format_conflict_info(conflict_info, merge_label)
                LOGGER.debug('Target branch merge conflicts in %s/%s MR %s:\n%s',
                             pname, target_branch, mr_id, note)
                if not args.testing and {'title': merge_label} not in mr_labels:
                    gl_mergerequest.notes.create({'body': f'/label {merge_label}'})
                    update_webhook_comment(gl_mergerequest, graphql.username,
                                           '**Mergeability Summary:', note)
                continue

            conflict_info = check_pending_conflicts(mr, pname, conflict_mrs,
                                                    merge_dir, reset_branch)
            if conflict_info:
                merge_label = defs.MERGE_WARNING_LABEL
                note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
                note += format_conflict_info(conflict_info, merge_label)
                LOGGER.debug('Other pending MR merge conflicts found on %s/%s MR %s:\n%s',
                             pname, target_branch, mr_id, note)
                if not args.testing and {'title': merge_label} not in mr_labels:
                    gl_mergerequest.notes.create({'body': f'/label {merge_label}'})
                    update_webhook_comment(gl_mergerequest, graphql.username,
                                           '**Mergeability Summary:', note)
                continue

            LOGGER.warning('%s/%s MR %s had errors earlier, and now does not?!?',
                           pname, target_branch, mr['iid'])

        _git_branch_delete(merge_dir, reset_branch)

    for proj_tb, merge_dir in merge_dirs.items():
        merge_base = merge_dir.strip('/').split('/')[-1]
        clean_up_temp_merge_branch(args.rhkernel_src, merge_base, merge_dir)


if __name__ == '__main__':
    args = _get_parser_args()
    sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)
    main(args)
