#!/usr/bin/env python
"""Look for MRs that cannot be merged and/or are stale."""

import argparse
from datetime import datetime
from datetime import timedelta
from email.message import EmailMessage
import os
import re
import smtplib

from cki_lib import logger
from cki_lib import misc
import ldap
import sentry_sdk

from webhook import defs
from webhook.common import get_owners_parser
from webhook.graphql import GitlabGraph

LOGGER = logger.get_logger('utils.report_generator')

KWF_METRICS_URL = ('https://docs.engineering.redhat.com/pages/viewpage.action?'
                   'spaceKey=RHELPLAN&title=KWF+Metrics')
null_sst = 'NO-RHEL-SST-FOUND'

# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  %s(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after, targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {
        iid
        author{username, name, email: publicEmail}
        title
        targetBranch
        webUrl
        project{name fullPath}
        updatedAt
        draft
        labels{nodes{title description}}
      }
    }
  }
}
"""


def get_open_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR objects we're interested in."""
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY % namespace_type,
                             variable_values={'namespace': namespace, 'branches': 'main'},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    mrs = {}
    for mreq in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes'):
        namespace = mreq['project']['fullPath']
        if namespace in mrs:
            mrs[namespace].append(mreq)
            continue
        mrs[namespace] = [mreq]

    LOGGER.debug('%s MRs: found %s', namespace_type.title(), mrs)
    return mrs


def get_author_sst_from_ldap(args, author):
    """Fetch author's SST from ldap."""
    sst = ''
    email = author['email']
    name = author['name']

    dn = f"ldap://{args.ldap_server}"
    base_dn = args.base_dn
    attrs = [arg for arg in args.ldap_attrs.split()]

    ldc = ldap.initialize(dn, trace_level=0)
    ldc.simple_bind_s()

    if email and ('@redhat.com' in email or '@fedoraproject.org' in email):
        look_for = f"(|(rhatPreferredAlias={email})(rhatPrimaryMail={email}))"
    else:
        look_for = f"(CommonName={name})"

    results = ldc.search_s(base_dn, ldap.SCOPE_SUBTREE, look_for, attrs)

    if email and not results:
        LOGGER.warning("No ldap entry found for email %s. Misconfigured alias?", email)
        look_for = f"(CommonName={name})"
        results = ldc.search_s(base_dn, ldap.SCOPE_SUBTREE, look_for, attrs)
        if not results:
            LOGGER.warning("Still unable to find an SST for %s <%s>", name, email)
            sst = 'EMAIL-NOT-FOUND'
    elif not results:
        look_for = f"(&(givenName={name.split()[1]})(sn=*{name.split()[-1]}*))"
        results = ldc.search_s(base_dn, ldap.SCOPE_SUBTREE, look_for, attrs)
        if not results:
            LOGGER.warning("Cannot find entry for %s (@%s).", name, author['username'])
            sst = 'UNKNOWN-USER'

    for _, a in results:
        members_list = a['memberOf']
        for member in members_list:
            cn_byte = str(member).split(',')[0]
            cn_name = str(cn_byte).split('=')[1]
            if re.match('rhel-sst-', cn_name):
                sst = cn_name
                break

    if email and not sst:
        LOGGER.warning("No SST found in ldap for email %s.", email)
        sst = null_sst

    LOGGER.debug("Got ldap results of %s", results)
    return sst


def get_author_info(args, mreq, authors):
    """Extract necessary MR author info."""
    gl_author = mreq['author']
    username = gl_author['username']
    if username is None:
        LOGGER.info("No username for mreq %s", mreq['iid'])
        return {}
    if username in authors:
        LOGGER.debug("User %s already in authors", username)
        return {}
    new_author = {}
    new_author[username] = gl_author
    new_author[username]['sst'] = get_author_sst_from_ldap(args, new_author[username])
    LOGGER.debug("Got user info of: %s", new_author[username])
    return new_author


def format_mr_entry(mr_data):
    """Format output from mr_data."""
    mr_id = mr_data['iid']
    title = mr_data['title']
    author = mr_data['author']['username']
    mr_url = mr_data['webUrl']
    proj = mr_data['project']['name']
    target_branch = mr_data['targetBranch']

    mr_entry = f"  MR {mr_id}: {title} (@{author})\n"
    mr_entry += f"    {mr_url}\n"
    mr_entry += f"    Project: {proj}, Target Branch: {target_branch}\n"

    return mr_entry


def parse_labels(mreq_labels):
    """Get the description text for the labels of interest."""
    label_info = ''
    categories = [defs.READY_FOR_MERGE_LABEL, defs.READY_FOR_QA_LABEL,
                  defs.MERGE_CONFLICT_LABEL, defs.MERGE_WARNING_LABEL]
    matched_categories = []
    interesting_prefixes = ('Acks', 'Bugzilla', 'CKI', 'CKI_RT', 'CommitRefs',
                            'Dependencies', 'JIRA', 'Merge', 'Signoff')

    for label in mreq_labels:
        if label['title'] in categories:
            matched_categories.append(label['title'])
        if label['title'].startswith(interesting_prefixes) and not label['title'].endswith('::OK'):
            label_info += f"    - {label['description']}\n"

    label_info += "\n"

    return label_info, matched_categories


def get_subsystems(mreq_labels):
    """Get list of subsystems affecting an MR from its label list."""
    subsystems = []
    for label in mreq_labels:
        if label['title'].startswith('Subsystem:'):
            subsystems.append(f"{label['title'].split(':')[1]}")

    return subsystems


def format_subsystem_entries(reports, section):
    """Format individual report entries."""
    body = ''
    for subsystem, entries in reports[section].items():
        if subsystem.startswith('rhel-sst-'):
            subsystem = '-'.join(subsystem.split('-')[2:])
        if entries:
            body += f"*** {subsystem} {section} ***\n{entries}\n"

    return body


def section_info_builder(args, report_list):
    """Build TOC and section info headers."""
    s_info = dict.fromkeys(report_list, '')
    s_info['conflicts'] = '1. Cannot be merged to target branch (conflicts)\n'
    s_info['warnings'] = '2. Merge requests conflicting with other MRs (warnings)\n'
    s_info['stale_dev'] = f'3. MRs stalled in development for {args.stale}+ days (stale_dev)\n'
    s_info['stale_qa'] = f'4. MRs stalled in QA for {args.stale}+ days (stale_qa)\n'
    s_info['stale_km'] = f'5. MRs stalled for merge for {args.stale}+ days (stale_km)\n'
    s_info['stale_draft'] = f'6. MR drafts stalled for {args.draft_stale}+ days (stale_draft)\n'

    return s_info


def format_section_header(text):
    """Print a header wrapped with dashes."""
    dashes = "-" * len(text) + "\n"
    return dashes + text + dashes


def format_report_body(args, reports, section_info):
    """Format the body of the report from the individual section reports."""
    report_body = '================= Table of Contents =================\n'
    report_body += 'Format is "Long Description (short_tag)"\n'
    report_body += '(NOTE: Empty sections will NOT be shown in the report body)\n'
    report_body += section_info['conflicts']
    report_body += section_info['warnings']
    report_body += section_info['stale_dev']
    report_body += section_info['stale_qa']
    report_body += section_info['stale_km']
    report_body += section_info['stale_draft']
    report_body += '\n'

    if args.conflicts:
        entries = format_subsystem_entries(reports, 'conflicts')
        if entries:
            report_body += format_section_header(section_info['conflicts']) + entries

        entries = format_subsystem_entries(reports, 'warnings')
        if entries:
            report_body += format_section_header(section_info['warnings']) + entries

    if args.stale:
        entries = format_subsystem_entries(reports, 'stale_dev')
        if entries:
            report_body += format_section_header(section_info['stale_dev']) + entries

        entries = format_subsystem_entries(reports, 'stale_qa')
        if entries:
            report_body += format_section_header(section_info['stale_qa']) + entries

        entries = format_subsystem_entries(reports, 'stale_km')
        if entries:
            report_body += format_section_header(section_info['stale_km']) + entries

    if args.draft_stale:
        entries = format_subsystem_entries(reports, 'stale_draft')
        if entries:
            report_body += format_section_header(section_info['stale_draft']) + entries

    return report_body


def send_emailed_report(args, body, cc_list):
    """Create and send an email containing the report output."""
    if not (args.email and args.from_address and args.smtp_url):
        return

    msg = EmailMessage()
    msg['Subject'] = f'Weekly KWF Problematic MR summary ({datetime.utcnow().strftime("%Y.%m.%d")})'
    msg['From'] = args.from_address
    msg['To'] = ', '.join(args.email)
    if args.cc_sst_lists:
        msg['Cc'] = ', '.join(cc_list)
    msg.set_content(body)
    with smtplib.SMTP(args.smtp_url) as smtp:
        smtp.send_message(msg)


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check open MRs for merge conflicts')

    # Global options
    parser.add_argument('-c', '--conflicts', action='store_true', default=False,
                        help='include MRs with conflicts in the report.')
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--stale', default=15, type=int, required=False,
                        help='include MRs that have not been updated in X days.')
    parser.add_argument('-D', '--draft-stale', default=90, type=int, required=False,
                        help='include draft MRs that have not been updated in X days.')
    parser.add_argument('-e', '--email', default=os.environ.get('EMAIL_TO', '').split(),
                        help='email report to address(es)', nargs='+', required=False)
    parser.add_argument('-f', '--from-address', default=os.environ.get('REPORTER_EMAIL_FROM', ''),
                        help='email report from address', required=False)
    parser.add_argument('-S', '--smtp-url', default=os.environ.get('SMTP_URL', 'localhost'),
                        help='smtp server to use to send report', required=False)
    parser.add_argument('-C', '--cc-sst-lists', action='store_true', default=False,
                        help='Add subsystem team mailing lists to email cc list', required=False)
    parser.add_argument('-o', '--owners-yaml', default=os.environ.get('OWNERS_YAML', ''),
                        help='Path to the owners.yaml file')
    parser.add_argument('-l', '--ldap-server', default=os.environ.get('LDAP_SERVER', ''),
                        help='Hostname of LDAP server to query for SSTs')
    parser.add_argument('-b', '--base-dn', default=os.environ.get('LDAP_BASE_DN', ''),
                        help='Base DN on LDAP server to query for SSTs')
    parser.add_argument('-a', '--ldap-attrs', default=os.environ.get('LDAP_ATTRS', ''),
                        help='Attributes to fetch from LDAP server to query for SSTs')
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(args):
    """Find open MRs, check for merge conflicts and/or stalled MRs."""
    LOGGER.info('Finding open MRs...')
    graphql = GitlabGraph()

    mrs_to_conflict_check = {}
    for group in args.groups:
        mrs_to_conflict_check.update(get_open_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_to_conflict_check.update(get_open_mrs(graphql, project, 'project'))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    owners = get_owners_parser(args.owners_yaml)
    stale_ts = datetime.utcnow() - timedelta(days=args.stale)
    stale_draft_ts = datetime.utcnow() - timedelta(days=args.draft_stale)

    report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings']
    reports = dict.fromkeys(report_list, '')
    section_info = section_info_builder(args, report_list)
    cc_list = []
    authors = {}
    author_ssts = []
    for mr_ns, mrs in mrs_to_conflict_check.items():
        for mreq in mrs:
            authors.update(get_author_info(args, mreq, authors))
    for author, entry in authors.items():
        if entry['sst'] not in author_ssts:
            author_ssts.append(entry['sst'])
    LOGGER.debug("Authors SSTs: %s", author_ssts)
    for category in report_list:
        reports[category] = dict.fromkeys(author_ssts, '')
    for mr_ns, mrs in mrs_to_conflict_check.items():
        for mreq in mrs:
            mr_labels = mreq['labels']['nodes']
            mr_info = format_mr_entry(mreq)
            subsystems = get_subsystems(mr_labels)
            subsystem_teams = []
            author_sst = authors[mreq['author']['username']]['sst']
            for subsystem in subsystems:
                sst_lists = owners.get_matching_entries_by_label(subsystem)
                for sst_list in sst_lists:
                    if sst_list.devel_sst:
                        for sst in sst_list.devel_sst:
                            short_sst = sst
                            if sst.startswith('rhel-sst-'):
                                short_sst = '-'.join(sst.split('-')[2:])
                            if short_sst not in subsystem_teams:
                                subsystem_teams.append(short_sst)
                    if sst_list.mailing_list and sst_list.mailing_list not in cc_list:
                        cc_list.append(sst_list.mailing_list)
            if author_sst == null_sst and len(subsystem_teams) == 1:
                author_sst = f'rhel-sst-{subsystem_teams[0]}'
            if subsystem_teams:
                mr_info += f"    Affected Subsystems: {' '.join(subsystem_teams)}\n"
            extra_info = f"    Last Updated: {mreq['updatedAt']}\n"
            (label_info, categories) = parse_labels(mr_labels)
            extra_info += label_info

            if mreq['updatedAt'] < str(stale_ts):
                if defs.READY_FOR_MERGE_LABEL in categories:
                    reports['stale_km'][author_sst] += mr_info + extra_info
                elif defs.READY_FOR_QA_LABEL in categories:
                    reports['stale_qa'][author_sst] += mr_info + extra_info
                else:
                    if not mreq['draft']:
                        reports['stale_dev'][author_sst] += mr_info + extra_info
                    elif mreq['updatedAt'] < str(stale_draft_ts):
                        reports['stale_draft'][author_sst] += mr_info + extra_info

            if defs.MERGE_CONFLICT_LABEL in categories and not mreq['draft']:
                reports['conflicts'][author_sst] += mr_info + "\n"
            elif defs.MERGE_WARNING_LABEL in categories and not mreq['draft']:
                reports['warnings'][author_sst] += mr_info + "\n"

    report_body = format_report_body(args, reports, section_info)

    if report_body:
        report_body += f"Additional info: {KWF_METRICS_URL}"
        send_emailed_report(args, report_body, cc_list)
    else:
        LOGGER.info("Nothing to report!")

    if not misc.is_production() and report_body:
        LOGGER.info("Found SST Mailing lists to cc: %s", cc_list)
        LOGGER.info("Report:\n%s\n", report_body)
        authors_info = 'Got authors info of:\n"'
        for user, fields in authors.items():
            authors_info += f"{user} | e:{fields['email']} | {fields['sst']}\n"
        LOGGER.debug(authors_info)


if __name__ == '__main__':
    args = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)
    main(args)
