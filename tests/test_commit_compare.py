"""Webhook interaction tests."""
import copy
import unittest
from unittest import mock

import webhook.commit_compare
import webhook.common
import webhook.defs

# a hunk of upstream linux kernel commit ID 1fc70edb7d7b5ce1ae32b0cf90183f4879ad421a
PATCH_A = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_A += "index blahblah..blahblah 100644\n"
PATCH_A += "--- a/include/linux/netdevice.h\n"
PATCH_A += "+++ b/include/linux/netdevice.h\n"
PATCH_A += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_A += "        unsigned short          type;\n"
PATCH_A += "        unsigned short          hard_header_len;\n"
PATCH_A += "        unsigned char           min_header_len;\n"
PATCH_A += "+       unsigned char           name_assign_type;\n"
PATCH_A += "\n"
PATCH_A += "        unsigned short          needed_headroom;\n"
PATCH_A += "        unsigned short          needed_tailroom;\n"

PATCH_B = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_B += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_B += "--- a/include/linux/netdevice.h\n"
PATCH_B += "+++ b/include/linux/netdevice.h\n"
PATCH_B += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_B += "        unsigned short          type;\n"
PATCH_B += "        unsigned short          hard_header_len;\n"
PATCH_B += "        unsigned char           min_header_len;\n"
PATCH_B += "+       unsigned char           name_assign_type;\n"
PATCH_B += "\n"
PATCH_B += "        unsigned short          needed_max_headroom;\n"
PATCH_B += "        unsigned short          needed_tailroom;\n"

PATCH_C = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_C += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_C += "--- a/include/linux/netdevice.h\n"
PATCH_C += "+++ b/include/linux/netdevice.h\n"
PATCH_C += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_C += "        unsigned short          type;\n"
PATCH_C += "        unsigned short          hard_header_len;\n"
PATCH_C += "        unsigned char           min_header_len;\n"
PATCH_C += "+       RH_KABI_EXTEND(unsigned char name_assignee_type)\n"
PATCH_C += "\n"
PATCH_C += "        unsigned short          needed_headroom;\n"
PATCH_C += "        unsigned short          needed_tailroom;\n"

PATCH_D = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netdevice.h\n"
PATCH_D += "+++ b/include/linux/netdevice.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"
PATCH_D += "diff --git a/include/linux/netlink.h b/include/linux/netlink.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netlink.h\n"
PATCH_D += "+++ b/include/linux/netlink.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"

PATCH_E = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_E += "new file mode 100644\n"
PATCH_E += "index blahblah..blahblah 100644\n"
PATCH_E += "--- /dev/null\n"
PATCH_E += "+++ b/include/linux/netdevice.h\n"
PATCH_E += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_E += "        unsigned short          type;\n"
PATCH_E += "        unsigned short          hard_header_len;\n"
PATCH_E += "        unsigned char           min_header_len;\n"
PATCH_E += "+       unsigned char           name_assign_type;\n"
PATCH_E += "\n"
PATCH_E += "        unsigned short          needed_headroom;\n"
PATCH_E += "        unsigned short          needed_tailroom;\n"

PATCH_F = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_F += "deleted file mode 100644\n"
PATCH_F += "index blahblah..blahblah 100644\n"
PATCH_F += "--- a/include/linux/netdevice.h\n"
PATCH_F += "+++ /dev/null\n"
PATCH_F += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_F += "        unsigned short          type;\n"
PATCH_F += "        unsigned short          hard_header_len;\n"
PATCH_F += "        unsigned char           min_header_len;\n"
PATCH_F += "+       unsigned char           name_assign_type;\n"
PATCH_F += "\n"
PATCH_F += "        unsigned short          needed_headroom;\n"
PATCH_F += "        unsigned short          needed_tailroom;\n"

PATCH_G = "diff --git a/include/linux/netdevice.h b/include/linux/foobar.h\n"
PATCH_G += "similarity index xx%\n"
PATCH_G += "rename from include/linux/netdevice.h\n"
PATCH_G += "rename to include/linux/foobar.h\n"
PATCH_G += "index blahblah..blahblah 100644\n"
PATCH_G += "--- a/include/linux/netdevice.h\n"
PATCH_G += "+++ b/include/linux/foobar.h\n"
PATCH_G += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_G += "        unsigned short          type;\n"
PATCH_G += "        unsigned short          hard_header_len;\n"
PATCH_G += "        unsigned char           min_header_len;\n"
PATCH_G += "+       unsigned char           name_assign_type;\n"
PATCH_G += "\n"
PATCH_G += "        unsigned short          needed_headroom;\n"
PATCH_G += "        unsigned short          needed_tailroom;\n"

EXPECTED_REPORT = ("\n\nUpstream Commit ID Readiness Report\n"
                   "\n"
                   "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
                   "|:-------|:-------|:-------|:---------|:-------|\n"
                   "|1|348742bd|RHELonly|n/a       |3|\n"
                   "|2|1cd738b1|0c55f51a|Diffs     |2|\n"
                   "|4|1508d8fb|6cb6a4a2|Merge commit||\n"
                   "|5|6cb6a4a3|Posted|n/a       |1|\n"
                   "1. This commit has Upstream Status as Posted, but we're not able to "
                   "auto-compare it.  Reviewers should take additional care when reviewing "
                   "these commits.\n"
                   "2. This commit differs from the referenced upstream commit and should be "
                   "evaluated accordingly.\n"
                   "3. This commit has Upstream Status as RHEL-only and has no corresponding "
                   "upstream commit.  The author of this MR should verify if this commit has "
                   "to be applied to "
                   "[future versions of RHEL](https://gitlab.com/cki-project/kernel-ark). "
                   "Reviewers should take additional care when reviewing these commits.\n"
                   "\n"
                   "Total number of commits analyzed: **5**\n"
                   "Please verify differences from upstream.\n"
                   "* Patches that match upstream 100% not shown in table\n"
                   "\n"
                   "Merge Request passes upstream commit ID validation.\n")


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestCommitCompare(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'g/p'
                                 },
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'archived': False,
                                'web_url': 'https://web.url/g/p',
                                'path_with_namespace': 'g/p'
                                },
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'description': 'dummy description',
                    'state': 'opened',
                    'user': {'username': 'test_user'}
                    }

    MOCK_PROJS = {'projects': [{'name': 'project1',
                                'id': 12345,
                                'group_id': 9876,
                                'inactive': False,
                                'product': 'Project 1',
                                'webhooks': {'bughook': {'name': 'bughook'}},
                                'branches': [{'name': 'main',
                                              'inactive': False,
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0'
                                              },
                                             {'name': '10.1',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0'
                                              },
                                             {'name': '10.0',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0'
                                              }]
                                }]
                  }

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab, sdiff, udiff):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        self._test_payload(mocked_gitlab, False, payload=payload)

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.commit_compare.find_potential_missing_fixes')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab, sdiff, udiff, dep_data, ext_deps, fpmf, gcc):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        gcc.return_value = 100, 50
        self._test_payload(mocked_gitlab, True, payload=payload)

    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab, udiff, sdiff):
        """Check handling of a note."""
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE)

    @mock.patch('webhook.commit_compare.find_potential_missing_fixes')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('gitlab.Gitlab')
    def test_ucid_re_evaluation(self, mocked_gitlab, sdiff, udiff, dep_data, ext_deps, fpmf):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(mocked_gitlab, True, payload)
        payload["object_attributes"]["state"] = "closed"
        self._test_payload(mocked_gitlab, False, payload)
        payload["project"]["archived"] = True
        self._test_payload(mocked_gitlab, False, payload)

    def test_get_match_info(self):
        """Check that we get sane strings back for match types."""
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.FULL)
        self.assertEqual(output, "100% match")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.PARTIAL)
        self.assertEqual(output, "Partial   ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.DIFFS)
        self.assertEqual(output, "Diffs     ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.KABI)
        self.assertEqual(output, "kABI Diffs")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.RHELONLY)
        self.assertEqual(output, "n/a       ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.POSTED)
        self.assertEqual(output, "n/a       ")
        output = webhook.commit_compare.get_match_info(webhook.commit_compare.Match.BADMAIL)
        self.assertEqual(output, "Bad email ")

    def test_find_kabi_hints(self):
        """Check that we can find a kabi hint in a patch hunk."""
        output = webhook.commit_compare.find_kabi_hints('desc', PATCH_A)
        self.assertFalse(output)
        output = webhook.commit_compare.find_kabi_hints('desc', PATCH_C)
        self.assertTrue(output)
        description = "This patch is pretty basic, nothing to see"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertFalse(output)
        description = "This patch contains kABI work-arounds"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        description = "This patch references genksyms"
        output = webhook.commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        patch = "we're using an RH_RESERVED field here"
        output = webhook.commit_compare.find_kabi_hints('nada', patch)
        self.assertTrue(output)

    def test_ucid_compare(self):
        """Check that diff engine actually compares things properly."""
        # compare two identical patches (should be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_A)
        self.assertEqual(output, [])
        # compare two patches with only context differences (should be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_B)
        self.assertEqual(output, [])
        # compare two patches with actual differences (should NOT be equal)
        output = webhook.cdlib.compare_commits(PATCH_A, PATCH_C)
        self.assertNotEqual(output, [])

    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('webhook.cdlib.get_dependencies_data', return_value=(False, "abcd"))
    def test_run_zstream_comparison_checks(self, gdd, rhmeta):
        """Make sure z-stream comparisons look sane."""
        zcommit = mock.Mock(id="1234567890ab",
                            message="Y-Commit: abc012345678",
                            parent_ids=['abc'])
        zdifflist = []
        path = {'old_path': 'include/linux/netdevice.h',
                'new_path': 'include/linux/netdevice.h',
                'new_file': False,
                'renamed_file': False,
                'deleted_file': False,
                'a_mode': '100644',
                'b_mode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        zdifflist.append(path)
        zcommit.diff.return_value = zdifflist

        ycommit = mock.Mock(id="abc012345678",
                            message="commit a1b2c3d4e5f6",
                            parent_ids=['xyz'])
        ydifflist = []
        ypath = {'old_path': 'include/linux/netdevice.h',
                 'new_path': 'include/linux/netdevice.h',
                 'new_file': False,
                 'renamed_file': False,
                 'deleted_file': False,
                 'a_mode': '100644',
                 'b_mode': '100644',
                 'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                         "        unsigned short          type;\n"
                         "        unsigned short          hard_header_len;\n"
                         "        unsigned char           min_header_len;\n"
                         "+       unsigned char           name_assign_type2;\n"
                         "\n"
                         "        unsigned short          needed_headspace;\n"
                         "        unsigned short          needed_tailroom;\n"}
        ydifflist.append(ypath)
        ycommit.diff.return_value = ydifflist

        mri = mock.Mock()
        merge_request = mock.Mock()
        project = mock.Mock()
        merge_request.commits.return_value = [zcommit]
        mri.merge_request = merge_request
        mri.authlevel = 50
        project.commits.get.return_value = ycommit
        project.id = 12345
        mri.project = project
        rhmeta.return_value = self.MOCK_PROJS

        # Don't run on main
        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        merge_request.target_branch = "main"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

        # Don't run on branch without zstream_target_release
        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        merge_request.target_branch = "10.1"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        merge_request.target_branch = "10.0"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab do not match", output)

        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        ydifflist = []
        ydifflist.append(path)
        ycommit.diff.return_value = ydifflist
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab match 100%", output)

        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        zcommit.message = "There is no spoon"
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertIn("Z-Commit 1234567890ab has no Y-Commit reference", output)

        # skip merge commit
        zcommit.parent_ids = ['abcd', 'wxyz']
        ycommit.parent_ids = ['abcd', 'wxyz']
        rhmeta.return_value = copy.deepcopy(self.MOCK_PROJS)
        output = webhook.commit_compare.run_zstream_comparison_checks(mri)
        self.assertEqual("", output)

    def test_print_text_report(self):
        """See that we get a sane-looking report output table."""
        mri = mock.Mock()
        mri.notes = []
        mri.fixes = {}
        mri.rhcommits = [1, 2, 3, 4, 5]
        self.maxDiff = None
        table = [['6cb6a4a3ede59f047febaeaa801f164954541ff0', ['Posted'],
                  webhook.commit_compare.Match.POSTED, ['1'], ''],
                 ['1508d8fb007a71c7342f1ccbfa15edd5c3d47ccf',
                  ['6cb6a4a2ede59f047febaeaa801f164954541ff0'],
                  webhook.commit_compare.Match.MERGECOMMIT, [], ''],
                 ['12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                  ['abcdef01aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
                  webhook.commit_compare.Match.FULL, [], ''],
                 ['1cd738b13ae9b29e03d6149f0246c61f76e81fcf',
                  ['0c55f51a8c4bade7b2525047130f3925f1dd42bb'],
                  webhook.commit_compare.Match.DIFFS, ['2'], ''],
                 ['348742bd816fbbcda2f8243bf234bf1c91788082', ['RHELonly'],
                  webhook.commit_compare.Match.RHELONLY, ['3'], '']]
        mri.notes.append(webhook.commit_compare.posted_msg())
        mri.notes.append(webhook.commit_compare.diffs_msg())
        mri.notes.append(webhook.commit_compare.rhelonly_msg())
        mri.approved = True
        mri.commit_count = len(table)
        mri.status = webhook.defs.READY_SUFFIX
        report = webhook.commit_compare.print_text_report(mri, table)
        self.assertEqual(report, EXPECTED_REPORT)

    @mock.patch('webhook.commit_compare.print_gitlab_report')
    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_report_results(self, add_comment, gl_report):
        """Check on reporting functions."""
        mri = mock.Mock()
        mri.merge_request = mock.Mock()
        mri.rhcommits = []
        mri.commit_count = 5
        mri.fixes = False
        mri.lab.user.username = "shadowman"
        gl_report.return_value = "Here's your report\n"
        table = [['6cb6a4a3ede59f047febaeaa801f164954541ff0', ['Posted'],
                  webhook.commit_compare.Match.POSTED, ['1'], ''],
                 ['1508d8fb007a71c7342f1ccbfa15edd5c3d47ccf',
                  ['6cb6a4a2ede59f047febaeaa801f164954541ff0'],
                  webhook.commit_compare.Match.MERGECOMMIT, [], ''],
                 ['12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                  ['abcdef01aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
                  webhook.commit_compare.Match.FULL, [], ''],
                 ['1cd738b13ae9b29e03d6149f0246c61f76e81fcf',
                  ['0c55f51a8c4bade7b2525047130f3925f1dd42bb'],
                  webhook.commit_compare.Match.DIFFS, ['2'], ''],
                 ['348742bd816fbbcda2f8243bf234bf1c91788082', ['RHELonly'],
                  webhook.commit_compare.Match.RHELONLY, ['3'], '']]
        errors = "You screwed up SO bad here...\n"
        zcompare_notes = ""
        webhook.commit_compare.MRUCIDInstance.report_results(mri, table, errors, zcompare_notes)
        add_comment.assert_called_once()
        add_comment.assert_called_with(mri.merge_request, "shadowman",
                                       "Upstream Commit ID Readiness",
                                       "Here's your report\n" + errors)

    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.commit_compare.process_unknown_commit')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.commit_compare.no_upstream_commit_data')
    def test_validate_commit_ids(self, no_ucid, get_udiff, ucom, get_sdiff):
        """Check that we properly flesh out RHCommit dataclass elements."""
        mri = mock.Mock()
        mri.notes = []
        project = mock.Mock()
        mri.project = project
        mri.linux_src = "/src/linux"
        mpcget = mock.MagicMock(id="1234", author_email="johndoe@redhat.com")
        mpcget.diff.return_value = PATCH_A
        mri.project.commits.get.return_value = mpcget
        mri.nids = {'kabi': 0, 'merge': 0, 'unknown': 0}
        c1 = mock.Mock(id="abcdef012345", author_email="jdoe@redhat.com", message="")
        c2 = mock.Mock(id="fedcba012345", author_email="jdoe@redhat.com", message="")
        c3 = mock.Mock(id="fedcba012345", author_email="jdoe@redhat.com", message="")
        c4 = mock.Mock(id="fedcba012346", author_email="jdoe@redhat.com", message="")
        c5 = mock.Mock(id="aaacba012346", author_email="jdoe@redhat.com", message="")
        c6 = mock.Mock(id="aaacba012346", author_email="jdoe@redhat.com", message="Touches kABI")
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c1))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c2))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c3))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c4))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c5))
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c6))
        mri.rhcommits[0].ucids = ['RHELonly']
        mri.rhcommits[1].ucids = ['deadbeefdead']
        mri.rhcommits[2].ucids = ['Posted']
        mri.rhcommits[3].ucids = ['Merge']
        mri.rhcommits[4].ucids = ['-']
        mri.rhcommits[5].ucids = ['RHELonly']
        get_udiff.return_value = (None, None)
        get_sdiff.return_value = ("", [])
        webhook.commit_compare.validate_commit_ids(mri)
        self.assertEqual(no_ucid.call_count, 4)
        ucom.assert_called_once()
        get_udiff.assert_called_once()
        # Reset counters and run again w/an upstream diff returned
        no_ucid.call_count = 0
        get_udiff.call_count = 0
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(c2))
        mri.rhcommits[0].ucids = ['deadbeefdead']
        get_udiff.return_value = (PATCH_A, 'johndoe@redhat.com')
        get_sdiff.return_value = (PATCH_A, ['include/linux/netdevice.h'])
        webhook.commit_compare.validate_commit_ids(mri)
        no_ucid.assert_not_called()
        ucom.assert_called_once()
        get_udiff.assert_called_once()

    @mock.patch('webhook.cdlib.is_rhdocs_commit')
    @mock.patch('webhook.commit_compare.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.common.match_gl_username_to_email')
    @mock.patch('webhook.commit_compare.get_report_table')
    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.common.extract_dependencies')
    def test_check_on_ucids_has_deps(self, ext_deps, f_labels, c_labels,
                                     c_table, gluser, sdiff, udiff, is_rhdocs):
        """Check that we break loop properly when we have deps."""
        self.maxDiff = None
        mri = mock.Mock(notes=[])
        mri.commit_count = 5
        merge_request = mock.MagicMock()
        merge_request.author = {'id': 1}
        merge_request.iid = 2
        project = mock.MagicMock()
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        is_rhdocs.return_value = False
        commit = mock.Mock(id="4567", author_email="shadowman@redhat.com",
                           message="2\ncommit 12345678", parent_ids=["1234"])
        merge_request.diff_refs['start_sha'] = 'abbadabba'
        merge_request.labels = ['Dependencies::4567']
        merge_request.commits.return_value = [commit]
        mri.merge_request = merge_request
        mri.project = project
        mri.project.mergerequests.get.return_value = merge_request
        mri.rhcommits = []
        mri.rhcommits.append(webhook.commit_compare.RHCommit(commit))
        mri.nids = {'diffs': 0}
        mri.ucids = []
        mri.omitted = []
        mri.fixes = {}
        gluser.return_value = True
        table = [['4321', ['8765'], webhook.commit_compare.Match.FULL, [], '']]
        c_table.return_value = (table, True)
        ext_deps.return_value = ['4567']
        f_labels.return_value = ([], [])
        t_out, _ = webhook.commit_compare.check_on_ucids(mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.FULL)
        table = [['4321', ['8765'], webhook.commit_compare.Match.DIFFS, [], '']]
        c_table.return_value = (table, True)
        t_out, _ = webhook.commit_compare.check_on_ucids(mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.DIFFS)
        table = [['4321', ['8765'], webhook.commit_compare.Match.RHELONLY, [], '']]
        c_table.return_value = (table, True)
        t_out, _ = webhook.commit_compare.check_on_ucids(mri)
        self.assertEqual(t_out[0][0][2], webhook.commit_compare.Match.RHELONLY)

    def test_check_on_ucids_excessive_commits(self):
        mri = mock.Mock()
        mri.commit_count = 2001
        mri.authlevel = 30
        output = webhook.commit_compare.check_on_ucids(mri)
        self.assertEqual("**Upstream Commit ID Readiness Error(s)**  \n"
                         "*ERROR*: This Merge Request has too many commits for the commit "
                         "reference webhook to process (2001) -- please make sure you have "
                         "targeted the correct branch.", output[1])

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_kabi_label(self, f_labels, c_labels):
        """Test the code that adds a kabi label."""
        mri = mock.Mock()
        merge_request = mock.MagicMock()
        merge_request.iid = 2
        mri.merge_request = merge_request
        mri.project = mock.Mock()
        f_labels.return_value = ([], [])
        table = [['4321', ['8765'], webhook.commit_compare.Match.KABI, [], '']]
        status = webhook.commit_compare.add_kabi_label(mri, table)
        self.assertTrue(status)
        table = [['abcd', ['dead'], webhook.commit_compare.Match.FULL, [], '']]
        status = webhook.commit_compare.add_kabi_label(mri, table)
        self.assertFalse(status)

    def test_no_upstream_commit_data(self):
        """Test function that maps to different no ucid types."""
        mri = mock.Mock(notes=[])
        mri.nids = {'noucid': 0, 'rhelonly': 0, 'posted': 0}
        rhcommit = mock.Mock(ucids=[], match=webhook.commit_compare.Match.FULL)
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.FULL)
        rhcommit.ucids = ["-"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)
        rhcommit.ucids = ["RHELonly"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.RHELONLY)
        rhcommit.ucids = ["Posted"]
        webhook.commit_compare.no_upstream_commit_data(mri, rhcommit)
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.POSTED)

    def test_no_ucid_commit_note(self):
        """Test that we get the expected data back for commits w/o an upstream commit ID."""
        mri = mock.Mock(notes=[])
        mri.nids = {'noucid': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_no_ucid_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.noucid_msg())
        self.assertEqual(mri.nids['noucid'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)

    def test_rhelonly_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'rhelonly': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_rhel_only_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.rhelonly_msg())
        self.assertEqual(mri.nids['rhelonly'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.RHELONLY)

    def test_posted_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'posted': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_posted_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.posted_msg())
        self.assertEqual(mri.nids['posted'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.POSTED)

    def test_merge_commit_note(self):
        """Test that we get the expected data back for commits that are RHEL-only."""
        mri = mock.Mock(notes=[])
        mri.nids = {'merge': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_merge_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.merge_msg())
        self.assertEqual(mri.nids['merge'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.MERGECOMMIT)

    def test_unknown_commit_note(self):
        """Test that we get the expected data back for commits that are of unknown origin."""
        mri = mock.Mock(notes=[])
        mri.nids = {'unknown': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_unknown_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.unk_cid_msg())
        self.assertEqual(mri.nids['unknown'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.NOUCID)

    def test_kabi_commit_note(self):
        """Test that we get the expected data back for commits that flagged for kabi screening."""
        mri = mock.Mock(notes=[])
        mri.nids = {'kabi': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_kabi_patch(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.kabi_msg())
        self.assertEqual(mri.nids['kabi'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.KABI)

    def test_commit_with_diffs_note(self):
        """Test that we get the expected data back for commits with diffs from upstream."""
        mri = mock.Mock(notes=[])
        mri.nids = {'diffs': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_commit_with_diffs(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.diffs_msg())
        self.assertEqual(mri.nids['diffs'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.DIFFS)

    def test_partial_commit_note(self):
        """Test that we get the expected data back for partial backport commits."""
        mri = mock.Mock(notes=[])
        mri.nids = {'part': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_partial_backport(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.partial_msg())
        self.assertEqual(mri.nids['part'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.PARTIAL)

    def test_invalid_author_note(self):
        """Test that we get the expected data back for commits with invalid emails."""
        mri = mock.Mock(notes=[])
        mri.nids = {'badmail': 0}
        rhcommit = mock.Mock()
        rhcommit.match = 0
        webhook.commit_compare.process_invalid_author_commit(mri, rhcommit)
        self.assertEqual(mri.notes[0], webhook.commit_compare.badmail_msg())
        self.assertEqual(mri.nids['badmail'], '1')
        self.assertEqual(rhcommit.match, webhook.commit_compare.Match.BADMAIL)

    def test_get_submitted_diff(self):
        """Test that we can properly extract patch content from a submitted diff."""
        commit = []
        path = {'old_path': 'include/linux/netdevice.h',
                'new_path': 'include/linux/netdevice.h',
                'new_file': False,
                'renamed_file': False,
                'deleted_file': False,
                'a_mode': '100644',
                'b_mode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_A)
        self.assertEqual(output[1], ["include/linux/netdevice.h"])
        commit = []
        path['new_file'] = True
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_E)
        commit = []
        path['new_file'] = False
        path['deleted_file'] = True
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_F)
        commit = []
        path['new_file'] = False
        path['deleted_file'] = False
        path['renamed_file'] = True
        path['new_path'] = 'include/linux/foobar.h'
        commit.append(path)
        output = webhook.cdlib.get_submitted_diff(commit)
        self.assertEqual(output[0], PATCH_G)

    def test_partial_diff(self):
        """Check that we can match partial diffs."""
        filelist = []
        filelist.append("include/linux/netdevice.h")
        pdiff = webhook.cdlib.get_partial_diff(PATCH_D, filelist)
        output = webhook.cdlib.compare_commits(pdiff, PATCH_A)
        self.assertEqual(output, [])

    @mock.patch('webhook.common.match_gl_username_to_email')
    def test_valid_rhcommit_author(self, matcher):
        """Check that we're properly validating commit authors."""
        # Format: "rh commit author email", "upstream commit author email", "mr creator email"
        mri = mock.Mock()
        matcher.return_value = True
        c1 = mock.Mock(author_email='jdoe@redhat.com')
        c2 = mock.Mock(author_email='contrib@example.com')
        c3 = mock.Mock(author_email='noatsymbol.com')
        c4 = mock.Mock(author_email='someone@redhat.com@example.com')
        rhcommit1 = webhook.commit_compare.RHCommit(c1)
        rhcommit2 = webhook.commit_compare.RHCommit(c2)
        rhcommit3 = webhook.commit_compare.RHCommit(c3)
        rhcommit4 = webhook.commit_compare.RHCommit(c4)
        self.assertTrue(webhook.commit_compare.valid_rhcommit_author(mri, rhcommit1))
        self.assertTrue(webhook.commit_compare.valid_rhcommit_author(mri, rhcommit2))
        matcher.return_value = False
        self.assertFalse(webhook.commit_compare.valid_rhcommit_author(mri, rhcommit2))
        self.assertFalse(webhook.commit_compare.valid_rhcommit_author(mri, rhcommit3))
        self.assertFalse(webhook.commit_compare.valid_rhcommit_author(mri, rhcommit4))

    def test_has_upstream_commit_hash(self):
        """Check that we get expected return values from different match types."""
        m1 = webhook.commit_compare.Match.FULL
        m2 = webhook.commit_compare.Match.PARTIAL
        m3 = webhook.commit_compare.Match.DIFFS
        m4 = webhook.commit_compare.Match.NOUCID
        m5 = webhook.commit_compare.Match.RHELONLY
        m6 = webhook.commit_compare.Match.POSTED
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m1))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m2))
        self.assertTrue(webhook.commit_compare.has_upstream_commit_hash(m3))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m4))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m5))
        self.assertFalse(webhook.commit_compare.has_upstream_commit_hash(m6))

    def test_find_intentionally_omitted_fixes(self):
        """Check the results we get from the tested function."""
        mri = mock.Mock()
        mri.omitted = set()
        description = "Omitted-fix: abcdef012345"
        webhook.commit_compare.find_intentionally_omitted_fixes(mri, description)
        self.assertEqual({'abcdef012345'}, mri.omitted)

    @mock.patch('git.Repo')
    def test_map_fixes_to_commits(self, mocked_repo):
        """Make sure we're mapping RH-Fixes entries correctly."""
        mocked_repo.git.log.return_value = "commit aaaabbbbccccdddd\nAuthor: someone\n\n    title\n"
        mri = mock.Mock()
        mri.fixes = {'1234abcd1234': 'aaaabbbbccccdddd'}
        rhcommit = mock.Mock()
        rhcommit.fixes = ""
        commit = mock.Mock(id='deadbeef1234', title="this is a fix")
        rhcommit.commit = commit
        rhcommit.ucids = ['1234abcd1234']
        mri.rhcommits = [rhcommit]
        webhook.commit_compare.map_fixes_to_commits(mri, mocked_repo)
        self.assertIn("commit aaaabbbbccccdddd", rhcommit.fixes)
        self.assertIn("RH-Fixes: deadbeef1234 (\"this is a fix\")", rhcommit.fixes)

    @mock.patch('git.Repo')
    def test_find_potential_missing_fixes(self, mocked_repo):
        """Check the results we get from the tested function."""
        mocked_repo().git.log.return_value = '112233445566 fix one\n8675309abcde fix two\n'
        mri = mock.Mock()
        mri.linux_src = "/usr/src/linux"
        mri.ucids = {'abcdef012345', '112233445566'}
        mri.fixes = {}
        mri.rhcommits = []
        mri.omitted = {'8675309abcde'}
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            webhook.commit_compare.find_potential_missing_fixes(mri)
            self.assertEqual({}, mri.fixes)
            self.assertIn("Found 112233445566 in ucids", ' '.join(logs.output))
            self.assertIn("Found 8675309abcde in omitted", ' '.join(logs.output))

        mocked_repo().git.log.return_value = 'deadbeef1234 fix the thing\n'
        mri.ucids = {'112233445566'}
        mri.omitted = set()
        mri.fixes = {}
        webhook.commit_compare.find_potential_missing_fixes(mri)
        self.assertEqual({'112233445566': ['deadbeef1234']}, mri.fixes)

    @mock.patch('webhook.commit_compare.get_mri')
    @mock.patch('webhook.commit_compare.perform_mri_tasks')
    def test_process_mr_no_label_changes(self, mock_get_mri, mock_perform_mri_tasks):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'CommitRefs::OK'}],
                                              'current': [{'title': 'CommitRefs::OK'}]}}
                       }
        webhook.commit_compare.process_mr(None, msg, 'linux-src')
        mock_get_mri.assert_not_called()
        mock_perform_mri_tasks.assert_not_called()

    def test_extract_ucid(self):
        mri = mock.Mock()
        mri.ucids = set()
        mri.omitted = set()
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2",
                       message="2\ncommit a012345",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="abcd", author_email="shadowman2021@redhat.com",
                       title="This is commit 3",
                       message="2\nUpstream Status: git://linux-nfs.org/~bfields/linux.git",
                       parent_ids=["5678"])
        c4 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 4",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                               "\ncommit a234567890abcdef1234567890abcdef12345678"
                               "\ncommit b234567890abcdef1234567890abcdef12345678"
                               "\ncommit c234567890abcdef1234567890abcdef12345678"
                               "\ncommit d234567890abcdef1234567890abcdef12345678"
                               "\ncommit e234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c5 = mock.Mock(id="8765", author_email="developer@redhat.com",
                       title="This is commit 5",
                       message="5\ncommit  1234567890",
                       short_id="abcdefg1", parent_ids=["1234"])
        c6 = mock.Mock(id="9745", author_email="developer@redhat.com",
                       title="This is commit 6",
                       message="6\n (cherry picked from commit 1234567890abcdef)",
                       short_id="abcdefg2", parent_ids=["1235"])
        c7 = mock.Mock(id="9845", author_email="developer@redhat.com",
                       title="Merge: This is commit 7",
                       message="6\n (cherry picked from commit 1234567890abcdef)",
                       short_id="abc3efg2", parent_ids=["1357", "2468"])
        c8 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       message="1\n(cherry picked from commit "
                               "1234567890abcdef1234567890abcdef12345678)"
                               "\n (cherry picked from commit "
                               "2234567890abcdef1234567890abcdef12345678)"
                               "\n(cherry picked from commit   "
                               "3234567890abcdef1234567890abcdef12345678)",
                       parent_ids=["abcd"])
        c9 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678"
                               "\ncommit 2234567890abcdef1234567890abcdef12345678"
                               "\n commit 3234567890abcdef1234567890abcdef12345678"
                               "\ncommit 4234567890abcdef1234567890abcdef12345678"
                               "\ncommit  5234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c10 = mock.Mock(id="1234567890", author_email="jdoe@redhat.com",
                        title="This is a commit",
                        message="XYZ\nUpstream Status: https://github.com/torvalds/linux.git",
                        parent_ids=["abcdef0123"])
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c1),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c2), ([], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c3), (['Posted'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c4), (['RHELonly'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c5),
                         (['1234567890'],
                          '\n* Incorrect format git commit line in abcdefg1.  \n'
                          '`Expected:` `commit 1234567890`  \n'
                          '`Found   :` `commit  1234567890`  \n'
                          'Git hash only has 10 characters, expected 40.  \n'))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c6),
                         (['1234567890abcdef'],
                          '\n* Incorrect format git cherry-pick line in abcdefg2.  \n'
                          '`Expected:` `(cherry picked from commit 1234567890abcdef)`  \n'
                          '`Found   :` ` (cherry picked from commit 1234567890abcdef)`  \n'
                          'Git hash only has 16 characters, expected 40.  \n'))
        mri.project.commits.get.return_value = c7
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c7), (['Merge'], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c8),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c9),
                         (["1234567890abcdef1234567890abcdef12345678",
                           "2234567890abcdef1234567890abcdef12345678",
                           "4234567890abcdef1234567890abcdef12345678"], ''))
        # This commit should NOT be considered Posted, as Linus' tree always needs a hash
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c10), ([], ''))

    def _test_payload(self, mocked_gitlab, result, payload):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, [])
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, [])

    # pylint: disable=too-many-arguments
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    @mock.patch('webhook.common.add_label_to_merge_request', mock.Mock())
    def _test(self, mocked_gitlab, result, payload, labels, assert_labels=None):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        merge_request.author = {'id': 1, 'username': 'jdoe'}
        merge_request.iid = 2
        merge_request.state = payload["state"]
        merge_request.description = payload["description"]
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1", parent_ids=['abcd'],
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678")
        c1.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2", parent_ids=['abce'],
                       message="2\ncommit a01234567890abcdef1234567890abcdef123456")
        c2.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       title="This is commit 3", parent_ids=['abcf'],
                       message="3\nUpstream Status: RHEL-only")
        c3.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c4 = mock.Mock(id="deadbeef", author_email="zzzzzz@redhat.com", message="4\n"
                       "(cherry picked from commit abcdef0123456789abcdef0123456789abcdef01)",
                       title="This is commit 4", parent_ids=['abc0'])
        c4.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c5 = mock.Mock(id="0ff0ff00", author_email="spam@redhat.com",
                       title="This is commit 5", parent_ids=['abc1'],
                       message="5\nUpstream-status: Posted")
        c5.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        c6 = mock.Mock(id="f00df00d", author_email="spam@redhat.com",
                       title="This is commit 6", parent_ids=['abc2'],
                       message="6\nUpstream-status: Embargoed")
        c6.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        project.namespace = mock.MagicMock(id=1, full_path=webhook.defs.RHEL_KERNEL_NAMESPACE)
        project.archived = payload["project"]["archived"]
        merge_request.labels = labels
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6]
        merge_request.pipeline = {'status': 'success'}
        gl_instance = mock.Mock()
        gl_instance.users.get.return_value = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "deadbeef": c4, "00f00f00": c5,
                           "f00df00d": c6}
        mri = mock.Mock()
        mri.ucids = set()
        mri.omitted = set()
        mri.authlevel = 50
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c1),
                         (["1234567890abcdef1234567890abcdef12345678"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c2),
                         (["a01234567890abcdef1234567890abcdef123456"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c3), (["RHELonly"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c4),
                         (["abcdef0123456789abcdef0123456789abcdef01"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c5), (["Posted"], ''))
        self.assertEqual(webhook.commit_compare.extract_ucid(mri, c6), (["Posted"], ''))

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No upstream commit ID found in submitted patch!\n***",
            "result": "fail",
            "logs": "*** Comparing submitted patch with upstream commit ID\n***"
        }

        headers = {'message-type': 'gitlab'}
        args = webhook.common.get_arg_parser('COMMIT_COMPARE').parse_args('')
        args.disable_user_check = True
        with mock.patch('webhook.commit_compare.SESSION.post', return_value=presult):
            return_value = \
                webhook.common.process_message(args, webhook.commit_compare.WEBHOOKS, 'dummy',
                                               payload, headers, linux_src='/src/linux')

        if result:
            mocked_gitlab.assert_called()
            mocked_gitlab().__enter__().projects.get.assert_called_with('g/p')
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels), sorted(assert_labels))
        else:
            self.assertFalse(return_value)
