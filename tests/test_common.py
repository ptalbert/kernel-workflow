"""Webhook interaction tests."""
from copy import deepcopy
from datetime import datetime
from datetime import timedelta
import json
import os
import pathlib
from random import uniform
import re
import tempfile
from unittest import TestCase
from unittest import mock

from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError

from tests import fake_payloads
from tests import fakes
from webhook import common
from webhook import defs
from webhook.rh_metadata import Projects

NEEDS_REVIEW_LABEL_COLOR = '#FF0000'
NEEDS_TESTING_LABEL_COLOR = '#CAC542'
READY_LABEL_COLOR = '#428BCA'
MERGE_LABEL_COLOR = '#8BCA42'


class MyLister:
    def __init__(self, labels):
        self.labels = labels
        self.call_count = 0
        self.called_with_all = None

    def list(self, search=None, iterator=False):
        self.call_count += 1
        if iterator:
            self.called_with_all = True
            return self.labels
        self.called_with_all = False
        if not search:
            raise AttributeError
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(TestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': MERGE_LABEL_COLOR,
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'readyForQA',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NACKed',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 4,
                       'name': 'Acks::NeedsReview',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 5,
                       'name': 'Acks::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 6,
                       'name': 'Bugzilla::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request's bugzillas are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 7,
                       'name': 'Bugzilla::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's bugzillas are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 8,
                       'name': 'ExternalCI::lnst::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('The subsystem-required lnst testing has not yet'
                                       ' been completed.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 9,
                       'name': 'ExternalCI::lnst::OK',
                       'color': READY_LABEL_COLOR,
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 10,
                       'name': 'Dependencies::abcdef012345',
                       'color': '#123456',
                       'description': 'This MR has dependencies.',
                       'text_color': '#FFFFFF',
                       'priority': 15
                       },
                      {'id': 11,
                       'name': 'Acks::net::NeedsReview',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'descroption': 'Subsystem needs Acks'
                       },
                      {'id': 12,
                       'name': 'Merge::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request merges cleanly into it's "
                                       "target branch."),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 13,
                       'name': 'Merge::Conflicts',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ("This Merge Request can not be merged cleanly into "
                                       "it's target branch, due to conflicts with other "
                                       "recently merged code."),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 14,
                       'name': 'JIRA::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request's jira issues are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 15,
                       'name': 'JIRA::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's jira issues are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 16,
                       'name': 'CKI_RHEL::Failed::test',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': 'Failed CKI testing'
                       }]

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
    )

    YAML_LABELS = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                   {'name': 'Bugzilla::OnQA', 'color': '#CAC542', 'description': 'maybe'},
                   {'name': '^Subsystem:(\\w+)$',
                    'color': '#778899',
                    'description': 'hi %s',
                    'regex': True}
                   ]

    NATTRS = {'body': 'This should be unique per webhook\n\nEverything is fine, I swear...',
              'author': {'username': 'shadowman'},
              'id': '1234'}

    MOCK_PROJS = {'projects': [{'name': 'project1',
                                'id': 12345,
                                'group_id': 9876,
                                'inactive': False,
                                'pipelines': ['kernel'],
                                'product': 'Project 1',
                                'webhooks': {'bughook': {'name': 'bughook'}},
                                'branches': [{'name': 'main',
                                              'inactive': False,
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0',
                                              'milestone': 'RHEL-10.1.0'
                                              },
                                             {'name': '10.0',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0',
                                              'milestone': 'RHEL-10.0.0'
                                              },
                                             {'name': 'main-automotive',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0'
                                              }]
                                }]
                  }

    def test_mr_is_closed(self):
        mrequest = mock.Mock()
        mrequest.state = "closed"
        status = common.mr_is_closed(mrequest)
        self.assertTrue(status)
        mrequest.state = "opened"
        status = common.mr_is_closed(mrequest)
        self.assertFalse(status)

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = ["abcdef012345"]
        table = [["012345abcdef", commits, 1, "", ""]]
        for row in table:
            commits = common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        message = mock.Mock()
        # Missing 'action'
        payload = {'object_attributes': {}}
        message.payload = payload
        self.assertEqual(common.mr_action_affects_commits(message), None)

        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])

        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

        # Target branch changed should return True
        payload = {'object_attributes': {'action': 'update'},
                   'changes': {'merge_status': {'previous': 'can_be_merged',
                                                'current': 'unchecked'}}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

    def test_required_label_removed(self):
        payload = {'object_attributes': {'action': 'update'}}
        testing = defs.NEEDS_TESTING_SUFFIX
        ready = defs.READY_SUFFIX
        failed = defs.TESTING_FAILED_SUFFIX
        waived = defs.TESTING_WAIVED_SUFFIX

        changed_labels = [f'ExternalCI::lnst::{testing}', f'whatever::{ready}']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Drivers:bonding']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::rdmalab::{ready}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, True)

        changed_labels = [f'Acks::{ready}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::lnst::{failed}']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = [f'ExternalCI::lnst::{failed}']
        result = common.required_label_removed(payload, failed, changed_labels)
        self.assertEqual(result, True)

        changed_labels = [f'ExternalCI::lnst::{waived}']
        result = common.required_label_removed(payload, waived, changed_labels)
        self.assertEqual(result, True)

    def test_has_label_changed(self):
        """Returns True if the label name has changed."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has not changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'my_cool_label'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'a different label'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_changed(changes, 'my_cool_label'))

    def test_has_label_prefix_changed(self):
        """Returns True if label matched on prefix and changed, or False."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has not changed.
        changes = {}
        labels = {'previous': [{'title': 'Acks::OK'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has changed.
        labels = {'previous': [{'title': 'Acks::NeedsReview'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_prefix_changed(changes, 'Acks::'))

    def _process_message(self, mock_gl, msg, auth_user):
        webhooks = {'note': mock.Mock()}

        fake_gitlab = mock.Mock(spec=[])

        def mock_auth():
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="mock_auth")

        fake_gitlab.auth = mock_auth
        if auth_user:
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="init")

        mock_gl.return_value.__enter__.return_value = fake_gitlab

        with tempfile.NamedTemporaryFile() as tmp:
            pathlib.Path(tmp.name).write_text(json.dumps(msg, indent=None))
            parser = common.get_arg_parser('test')
            args = parser.parse_args(['--json-message-file', tmp.name])
            common.consume_queue_messages(args, webhooks)

        if auth_user:
            self.assertEqual(fake_gitlab.user._setup_from, "init")
        else:
            self.assertEqual(fake_gitlab.user._setup_from, "mock_auth")

        return webhooks['note']

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_json_processing(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_no_auth(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, None)
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_bot_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'cki-bot'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_not_called()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_regular_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    def test_get_payload_username(self):
        """Returns the username from the payload."""
        payload = {'object_kind': 'merge_request',
                   'user': {'username': 'steve'}}
        self.assertEqual(common.get_payload_username(payload), 'steve')
        payload = {'object_kind': 'push',
                   'user_username': 'bill'}
        self.assertEqual(common.get_payload_username(payload), 'bill')

    def test_process_mr_url(self):
        mr_url = 'https://web.url/g/p/-/merge_requests/123'
        project_id = 456
        target_branch = '10.0-automotive'
        note_text = 'request-evaluation'
        # Note payload
        args = mock.Mock(merge_request=mr_url, payload_project_id=project_id,
                         payload_target_branch=target_branch, note=note_text)
        payload = common.process_mr_url(args, 'note')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], project_id)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload['object_attributes']['noteable_type'], 'MergeRequest')
        self.assertEqual(payload["object_attributes"]["note"], note_text)
        self.assertEqual(payload['merge_request']['iid'], 123)
        self.assertEqual(payload['merge_request']['target_branch'], target_branch)

        # MR payload
        args = mock.Mock(merge_request=mr_url, payload_project_id=project_id,
                         payload_target_branch=target_branch)
        payload = common.process_mr_url(args, 'merge_request')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], project_id)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'merge_request')
        self.assertEqual(payload['object_attributes']['iid'], 123)
        self.assertFalse(payload['object_attributes']['work_in_progress'])
        self.assertEqual(payload['object_attributes']['target_branch'], target_branch)
        self.assertTrue('merge_request' not in payload)

        # Pipeline payload
        payload = common.process_mr_url(args, 'pipeline')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], project_id)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'pipeline')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # Push payload
        payload = common.process_mr_url(args, 'push')
        self.assertEqual(payload['ref'], f'refs/head/{target_branch}')
        self.assertTrue('merge_request' not in payload)
        self.assertTrue('object_attributes' not in payload)

        # Some other payload?
        payload = common.process_mr_url(args, 'lion_attack')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], project_id)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'lion_attack')

    @mock.patch('webhook.common.consume_queue_messages')
    @mock.patch('webhook.common.process_mr_url')
    @mock.patch('webhook.common.process_message')
    def test_generic_loop(self, mock_process_message, mock_mr_url, mock_consume):
        WEBHOOKS = {}
        mr_url = 'https://web.url/g/p/-/merge_requests/123'
        headers = {'message-type': 'gitlab'}

        # --merge-request, no --note, no --action
        args = mock.Mock(merge_request=mr_url, action=None, note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(args, 'merge_request')
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request with --note, no --action
        note_text = 'request-evaluation'
        args = mock.Mock(merge_request=mr_url, action=None, note=note_text)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(args, 'note')
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request and --action set, no --note.
        args = mock.Mock(merge_request=mr_url, action='pipeline', note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(args, 'pipeline')
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # no --merge-request
        mock_mr_url.reset_mock()
        mock_process_message.reset_mock()
        args = mock.Mock(merge_request=None)
        common.generic_loop(args, WEBHOOKS)
        mock_mr_url.assert_not_called()
        mock_process_message.assert_not_called()
        mock_consume.assert_called_with(args, WEBHOOKS)

    def test_process_jira_webhook_message(self):
        """Test processing of jira webhook receiver messages."""
        # No handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_jira_webhook_message('headers', webhooks)
            self.assertFalse(result)
            self.assertIn(f'No {defs.JIRA_WEBHOOK_MESSAGE_TYPE} handler, ignoring',
                          logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {defs.JIRA_WEBHOOK_MESSAGE_TYPE: mock_handler}
        result = common._process_jira_webhook_message('headers', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('headers')

    def test_process_umb_bridge_message(self):
        """Test processing of umb_bridge messages."""
        # No handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_umb_bridge_message('body', 'headers', webhooks)
            self.assertFalse(result)
            self.assertIn(f'No {defs.UMB_BRIDGE_MESSAGE_TYPE} handler, ignoring',
                          logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {defs.UMB_BRIDGE_MESSAGE_TYPE: mock_handler}
        result = common._process_umb_bridge_message('body', 'headers', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('body', 'headers')

    def test_process_amqp_bridge_message(self):
        """Test processing of amqp-bridge messages."""
        # No amqp-bridge handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_amqp_bridge_message('body', webhooks)
            self.assertFalse(result)
            self.assertIn('No amqp-bridge handler, ignoring', logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {'amqp-bridge': mock_handler}
        result = common._process_amqp_bridge_message('body', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('body')

    @mock.patch('webhook.common._process_umb_bridge_message')
    @mock.patch('webhook.common._process_amqp_bridge_message')
    @mock.patch('webhook.common._process_gitlab_message')
    def test_process_message(self, mock_gitlab, mock_amqp, mock_umb):
        """Test message types."""
        args = common.get_arg_parser('TEST').parse_args('')
        # Handle a gitlab message.
        headers = {'message-type': 'gitlab'}
        body = {}
        common.process_message(args, 'webhooks', 'routing.key', body, headers)
        mock_gitlab.assert_called_with(body, 'webhooks', args)

        # Handle an amqp message.
        headers = {'message-type': 'amqp-bridge'}
        common.process_message(args, 'webhooks', 'routing.key', body, headers)
        mock_amqp.assert_called_with(body, 'webhooks')

        # Handle a umb_bridge message.
        headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE}
        common.process_message(args, 'webhooks', 'routing.key', body, headers)
        mock_umb.assert_called_with(body, headers, 'webhooks')

        # Complain about unknown message type.
        headers = {'message-type': 'pigeon'}
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            result = common.process_message(args, {}, 'routing.key', '', headers)
            self.assertTrue(result)
            self.assertIn('Ignoring unknown message type pigeon', logs.output[-1])

    @mock.patch('json.dumps')
    @mock.patch('webhook.common.is_event_target_branch_active')
    def test_process_gitlab_message(self, mock_branch_active, dumps):
        """Check for expected return value."""
        webhooks = {'merge_request': mock.Mock(), 'note': mock.Mock()}
        payload = {}
        args = common.get_arg_parser('TEST').parse_args('')

        # Wrong object kind should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'delete_project'
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn('Ignoring message with missing or unhandled object_kind.',
                          logs.output[-1])

        # Inactive branches should be ignored.
        mock_branch_active.return_value = False
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'opened'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn("Ignoring message with target branch that is not active", logs.output[-1])

        # Closed state should be ignored, return False.
        mock_branch_active.return_value = True
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'closed'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn("Ignoring event with 'closed' state.", logs.output[-1])

        # If username matches return False (event was generated by our own activity).
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bot')
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'opened'}
            payload['user'] = {'username': 'bot'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            payload['user'] = {'username': 'notbot'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks, args))

        # Ignore notes on issues.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'Issue'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn('Ignoring note event for non-MR.', logs.output[-1])

        # Notes on merge requests should be processed.
        with self.assertLogs('cki.webhook.common', level='INFO'), \
                mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'MergeRequest'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks, args))
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 2)

        # Recognize get_gl_instance=False.
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'

            res = common._process_gitlab_message(payload, webhooks, args, get_gl_instance=False)
            self.assertTrue(res)
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 1)

        # Recognize get_graphql_instance=True.
        with mock.patch('webhook.common.GitlabGraph') as mock_graph:
            mock_graph.return_value = mock.Mock(username='mock_user')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'

            res = common._process_gitlab_message(payload, webhooks, args, get_gl_instance=False,
                                                 get_graphql_instance=True)
            self.assertTrue(res)
            self.assertIs(webhooks['merge_request'].call_args.kwargs['graphql'],
                          mock_graph.return_value)
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 1)

        # Recognize get_graphql_instance=True and bail for matching user.
        with mock.patch('webhook.common.GitlabGraph') as mock_graph:
            mock_graph.return_value = mock.Mock(username='mock_user')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'
            payload['user'] = {'username': 'mock_user'}

            res = common._process_gitlab_message(payload, webhooks, args, get_gl_instance=False,
                                                 get_graphql_instance=True)
            self.assertFalse(res)

    @mock.patch('webhook.common.validate_labels')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_plabel_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_validate,
                              branch='main'):
        label_names = [label['name'] for label in new_labels]
        mock_validate.return_value = new_labels
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock(target_branch=branch)
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        mock_bridge = mock.Mock(allow_failure=False)
        mock_bridge.name = 'rhel9_merge_request'
        if branch == 'main-rt':
            mock_bridge.name = 'rhel9_realtime_check_merge_request'
        elif branch == 'main-automotive':
            mock_bridge.name = 'c9s_automotive_merge_request'
        mock_bridges = mock.Mock()
        mock_bridges.list.return_value = [mock_bridge]
        gl_project.pipelines.get.return_value = mock.Mock(bridges=mock_bridges)

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(
                gl_project, 2, label_names, level='project')
            self.assertTrue(isinstance(label_ret, list))

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and expected_cmd:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and expected_cmd:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertRegex(' '.join(logs.output),
                                     f"Editing label {re.escape(label['name'])}.*mock.path")
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("{\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'}") \
                        % (label['name'], label['color'], label['description'])
                    self.assertRegex(' '.join(logs.output),
                                     f"Creating label {re.escape(label_string)}.*mock.path")
                    call_list.append(mock.call(label))
            if call_list:
                gl_project.labels.create.assert_has_calls(call_list)
            else:
                gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._update_jira_state')
    @mock.patch('webhook.common._update_bugzilla_state')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_label_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_update_bzs,
                             mock_update_jiras, remove_scoped=False, draft=False, project_id=123):
        label_names = [label['name'] for label in new_labels]
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_group = mock.Mock()
        gl_instance.groups.get.return_value = gl_group

        gl_mergerequest = mock.Mock(iid=2, draft=draft)
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.id = project_id
        gl_project.manager.gitlab = gl_instance
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(
                gl_project, 2, label_names, remove_scoped=remove_scoped)
            self.assertTrue(isinstance(label_ret, list))

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and expected_cmd:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and expected_cmd:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertRegex(' '.join(logs.output),
                                     f"Editing label {re.escape(label['name'])}.*groups.get")
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("{\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'}") \
                        % (label['name'], label['color'], label['description'])
                    self.assertRegex(' '.join(logs.output),
                                     f"Creating label {re.escape(label_string)}.*groups.get")
                    call_list.append(mock.call(label))
            if call_list:
                gl_group.labels.create.assert_has_calls(call_list)
            else:
                gl_group.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
            mock_update_bzs.assert_called_once()
        else:
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._add_label_quick_actions', return_value=['/label "hello"'])
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_label_exception(self, mock_filter, mock_quickaction, mock_compute):
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        mock_filter.return_value = ([], [])
        label_list = ['Acks::OK']

        # Catch "can't be blank" and move on.
        err_text = "400 Bad request - Note {:note=>[\"can't be blank\"]}"
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message=err_text,
                                                                  response_code=400,
                                                                  response_body='')
        exception_raised = False
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                try:
                    result = common.add_label_to_merge_request(project, mergerequest.iid,
                                                               label_list)
                except GitlabCreateError:
                    exception_raised = True
                self.assertTrue(result)
                self.assertFalse(exception_raised)
                self.assertIn("can't be blank", logs.output[-1])

        # For any other error let it bubble up.
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message='oops',
                                                                  response_code=403,
                                                                  response_body='')
        exception_raised = False
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            try:
                common.add_label_to_merge_request(project, mergerequest.iid, label_list)
            except GitlabCreateError:
                exception_raised = True
        self.assertTrue(exception_raised)

    def test_add_plabel_to_merge_request(self):
        # Add a project label which does not exist on the project.
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::abcdef012345"')

        # Add scoped Deps::OK::sha label
        new_labels = [{'name': 'Dependencies::OK::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::OK::abcdef012345"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                      'description': 'This MR has dependencies.'}]
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands(mr_labels, new_labels, None)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_create_group_milestone(self, prod):
        gl_group = mock.Mock()
        product = "Red Hat Enterprise Linux 10"
        rhmeta = mock.Mock()
        rhmeta.internal_target_release = ""
        rhmeta.zstream_target_release = ""
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Unable to find release for milestone", " ".join(logs.output))

        rhmeta.internal_target_release = "10.1.0"
        rhmeta.milestone = "RHEL-10.1.0"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common._create_group_milestone(gl_group, product, rhmeta)
            self.assertIn("Creating new milestone for", " ".join(logs.output))
            gl_group.milestones.create.assert_called_once()

        prod.return_value = False
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Would have created milestone for", " ".join(logs.output))

    @mock.patch('webhook.common._get_gitlab_group')
    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_add_merge_request_to_milestone(self, is_prod, mock_loader, gglg):
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(12345, 'mock_project')
        mock_mr = mock_project.add_mr(123, attributes={'target_branch': 'main'})
        mock_project.manager.gitlab = mock_instance
        mock_instance.add_group('mock_group')
        mock_group = mock_instance.groups.get('mock_group')
        gglg.return_value = mock_group
        target_milestone = mock.Mock()
        target_milestone.title = 'RHEL-10.1.0'
        target_milestone.id = 6060606
        mock_group.milestones.list.return_value = [target_milestone]
        mock_mr.milestone_id = 6060606
        mock_mr.milestone = {'title': 'RHEL-10.1.0', 'id': 6060606}
        mock_mr.references = {'full': 'path/to/project!666'}
        mock_loader.return_value = self.MOCK_PROJS
        projects = Projects()

        # Branch has no milestone, nothing to do.
        branch = projects.projects[12345].branches[2]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_group.milestones.list.assert_not_called()

        # MR is already assigned to the correct Milestone
        branch = projects.projects[12345].branches[0]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_mr.save.assert_not_called()

        # MR will be assigned to an existing Milestone
        branch = projects.projects[12345].branches[0]
        mock_mr.milestone_id = 0
        mock_mr.milestone = None
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        self.assertEqual(mock_mr.milestone_id, target_milestone.id)
        mock_mr.save.assert_called_once()

        # MR will be assigned to a newly created Milestone
        mock_mr.milestone_id = 0
        target_milestone.title = 'RHEL-10.1.0'
        mock_mr.save.call_count = 0
        branch = projects.projects[12345].branches[1]
        common.add_merge_request_to_milestone(branch, mock_project, mock_mr)
        mock_group.milestones.create.assert_called_once()
        mock_mr.save.assert_called_once()

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'Subsystem:net', 'color': '#778899',
                       'description': 'An MR that affects code related to net.'}]
        self._test_label_commands(mr_labels, new_labels, '/label "Subsystem:net"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being added to the MR for kernel-ark.
        mr_labels = [{'name': 'CKI::OK'}]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'), project_id=defs.ARK_PROJECT_ID)

        # Add a label which triggers readyForMerge being added to the MR for RT.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForQA being added to the MR for automotive.
        mr_labels = [{'name': 'Bugzilla::NeedsTesting'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::Warning'},
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForQA"\n'
                                   '/unlabel "readyForMerge"'))

        # Add a label which triggers readyForMerge being added to the MR - JIRA variant,
        # with merged dependencies
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK::12345678'},
                     {'name': 'ExternalCI::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being removed from the MR.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add labels which trigger readyForQA, bugzilla variant.
        mr_labels = [{'name': 'Acks::NeedsReview'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'JIRA::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForMerge'}
                     ]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'Bugzilla::NeedsTesting'}]
        expected_note = ['/label "Acks::OK"',
                         '/label "Bugzilla::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/label "readyForQA"',
                         '/unlabel "readyForMerge"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Would trigger readyForQA but the MR is in Draft state.
        expected_note = ['/label "Acks::OK"',
                         '/label "Bugzilla::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note), draft=True)

        # Add labels which trigger readyForQA, jira variant.
        mr_labels = [{'name': 'Acks::NeedsReview'},
                     {'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'Merge::OK'},
                     {'name': 'readyForMerge'}
                     ]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'JIRA::NeedsTesting'}]
        expected_note = ['/label "Acks::OK"',
                         '/label "JIRA::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/label "readyForQA"',
                         '/unlabel "readyForMerge"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label.
        new_labels = [{'name': 'CKI_RHEL::Failed::test', 'color': '#FF0000',
                       'description': "This MR\'s latest CKI pipeline failed."}]
        expected_note = ['/label "CKI_RHEL::Failed::test"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"']
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a single scoped label and ensure related double-scoped are not removed too.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'},
                     {'name': 'ExternalCI::lnst::NeedsTesting'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a double scoped label and ensure only related double scoped are removed.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::net::NeedsReview'}]
        expected_note = ['/label "Acks::net::NeedsReview"',
                         '/unlabel "Acks::net::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label with remove_scoped set.
        mr_labels = [{'name': 'CKI_RHEL::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'CKI_RHEL::Failed::test'}]
        expected_note = ['/label "CKI_RHEL::Failed::test"',
                         '/unlabel "CKI_RHEL::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note),
                                  remove_scoped=True)

        # Add a lot of labels to trigger labels.list(iterator=True).
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Acks::ss{count}::OK"
            desc = f"This MR has been approved by reviewers in the ss{count} group."
            label = {'name': name,
                     'color': '#428BCA',
                     'description': desc}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::ss1::OK"\n/label "Acks::ss2::OK"'
                                   '\n/label "Acks::ss3::OK"\n/label "Acks::ss4::OK"'
                                   '\n/label "Acks::ss5::OK"\n/label "Acks::ss6::OK"'
                                   '\n/label "Acks::ss7::OK"\n/label "Acks::ss8::OK"'))

    def test_find_dep_mr_in_line(self):
        namespace = 'dummy/test'
        line = f'Depends: {defs.GITFORGE}/dummy/test/-/merge_requests/135'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 135)
        namespace = 'foo/bar'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: https://foo.bar/nope/-/merge_requests/1'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: !8675309'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 8675309)

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        dep_mr = mock.Mock()
        dep_mr.iid = 110
        dep_mr.description = "Bugzilla: https://bugzilla.redhat.com/24681357"
        project = mock.Mock()
        project.mergerequests.get.return_value = dep_mr
        project.path_with_namespace = 'fake/stuff'
        bzs = common.extract_dependencies(project, None)
        self.assertEqual(bzs, [])

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n'
                   'Depends: https://issues.redhat.com/browse/RHEL-3\n'
                   f'Depends: {defs.GITFORGE}/fake/stuff/-/merge_requests/110\n')
        bzs = common.extract_dependencies(project, message)
        self.assertEqual(bzs, ['22334455', '33445566', 'RHEL-3', '24681357'])

    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.common.connect_bugzilla', return_value=True):
            self.assertTrue(common.try_bugzilla_conn())

    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(common.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem connecting to bugzilla server.', logs.output[-1])
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem with file permissions', logs.output[-1])

    @mock.patch('webhook.common._match_label', mock.Mock(return_value=''))
    @mock.patch('webhook.common._get_gitlab_group')
    def test_add_label_quick_actions(self, get_group):
        """Check for the right label type (project or group) being evaluated."""
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        project = mock.Mock()
        project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::mm::OK'}]
        with mock.patch('webhook.common._edit_label') as label:
            output = common._add_label_quick_actions(project, label_list)
        label.assert_called_with(get_group.return_value, '', {'name': 'Acks::mm::OK'})
        self.assertEqual(['/label "Acks::mm::OK"'], output)
        project.id = defs.ARK_PROJECT_ID
        with mock.patch('webhook.common._edit_label') as label:
            common._add_label_quick_actions(project, label_list)
        label.assert_called_with(project, '', {'name': 'Acks::mm::OK'})
        self.assertEqual(['/label "Acks::mm::OK"'], output)

    def test_extract_all_from_message(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            None, [], [], []
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, [])
        self.assertEqual(non_mr_cvez, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456\n' \
                   'CVE: CVE-2021-00001'
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            message1, [], [], []
        )
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, ['CVE-2021-00001'])
        self.assertEqual(non_mr_cvez, [])
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            message1, [], [], ['18123456']
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = \
                common.extract_all_from_message(message1, ['12345678'], ['CVE-2021-00002'], [])
            self.assertIn('Bugzilla: 18123456 not listed in MR description.', logs.output[-2])
            self.assertIn('CVE: CVE-2021-00001 not listed in MR description.', logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])
            self.assertEqual(cves, [])
            self.assertEqual(non_mr_cvez, ['CVE-2021-00001'])

        jirastuff = 'Here is an MR using jira.\nJIRA: https://issues.redhat.com/browse/RHEL-2'
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            jirastuff, [], [], []
        )
        self.assertEqual(bzs, ['RHEL-2'])
        self.assertEqual(dep_bzs, [])
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez = common.extract_all_from_message(
            jirastuff, [], [], ['RHEL-2']
        )
        self.assertEqual(bzs, [])
        self.assertEqual(dep_bzs, ['RHEL-2'])

    def test_parse_mr_url(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/merge_requests/35435747'

        self.assertEqual(('mycoolproject', 12345), common.parse_mr_url(url1))
        self.assertEqual(('group/subgroup/project', 35435747), common.parse_mr_url(url2))

    def test_bugs_to_process(self):
        old_description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                           'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                           'Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/8765432')
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                       'Bugzilla: https://bugzilla.redhat.com/3456789')
        changes = {'description': {'previous': old_description,
                                   'current': description}
                   }

        # Link all in description, unlink 8765432.
        result = common.bugs_to_process(description, 'open', changes)
        self.assertEqual(set(result['link']), {1234567, 2345678, 3456789})
        self.assertEqual(set(result['unlink']), {8765432})

        # If the action is close then just unlink everything in the current description.
        result = common.bugs_to_process(description, 'close', {})
        self.assertEqual(result['unlink'], {1234567, 2345678, 3456789})
        self.assertEqual(result['link'], set())

    def test_get_mr(self):
        gl_project = mock.Mock(id=234567)
        gl_mr = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mr

        # No error, gl_mr object is returned.
        exception_raised = False
        try:
            result = common.get_mr(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.mergerequests.get.assert_called_with(123)
        self.assertEqual(result, gl_mr)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock(return_value=True)
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=404,
                                                                  error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_mr(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.mergerequests.get.assert_called_with(456)
            self.assertIn('MR 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock()
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=502,
                                                                  error_message='oh no!')
        try:
            common.get_mr(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.mergerequests.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_get_pipeline(self):
        gl_project = mock.Mock(id=234567)
        gl_pipeline = mock.Mock()
        gl_project.pipelines.get.return_value = gl_pipeline

        # No error, gl_pipeline object is returned.
        exception_raised = False
        try:
            result = common.get_pipeline(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.pipelines.get.assert_called_with(123)
        self.assertEqual(result, gl_pipeline)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.pipelines.get.reset_mock(return_value=True)
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=404,
                                                              error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_pipeline(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.pipelines.get.assert_called_with(456)
            self.assertIn('Pipeline 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.pipelines.get.reset_mock()
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=502,
                                                              error_message='oh no!')
        try:
            common.get_pipeline(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.pipelines.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_draft_status(self):
        merge_dict = {'object_kind': 'merge_request',
                      'project': {'id': 1,
                                  'web_url': 'https://web.url/g/p'},
                      'object_attributes': {'target_branch': 'main',
                                            'iid': 2,
                                            'work_in_progress': False},
                      'state': 'opened',
                      'action': 'open',
                      'labels': []
                      }

        # Not a Draft, MR message changes do not include 'title'.
        merge_dict['object_attributes']['work_in_progress'] = False
        merge_dict['changes'] = {}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # Not a draft and Titles don't mention Draft:.
        merge_dict['changes'] = {'title': {'previous': 'My awesome MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # No 'previous' title member.
        merge_dict['changes'] = {'title': {'current': 'My cool MR'}}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # It's not a Draft any more!
        merge_dict['changes'] = {'title': {'previous': '[Draft] My cool MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertTrue(changed)

        # I turned into a Draft.
        merge_dict['object_attributes']['work_in_progress'] = True
        merge_dict['changes'] = {'title': {'previous': 'My cool MR',
                                           'current': 'Draft: My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertTrue(is_draft)
        self.assertTrue(changed)

    @mock.patch('webhook.common.run')
    def test__grep_for_config(self, mock_run):
        # No results
        mock_run.return_value = mock.Mock(returncode=1)
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, [])
        mock_run.return_value.check_returncode.assert_not_called()

        # Found something
        mock_run.return_value.returncode = 0
        mock_run.return_value.stdout = '\n/linus/arch/Kconfig\n/linus/arch/x86_64/Kconfig'
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, ['arch/Kconfig', 'arch/x86_64/Kconfig'])
        mock_run.return_value.check_returncode.assert_not_called()

        # Called check_returncode()
        mock_run.return_value.returncode = 2
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        mock_run.return_value.check_returncode.assert_called_once()

    @mock.patch('webhook.common._grep_for_config')
    def test_process_config_items(self, mock_grep):
        # Input nothing, return nothing
        path_list = []
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, (set(), []))

        # A hit
        path_list = ['redhat/configs/common/generic/x86/x86_64/CONFIG_TURBO', 'README']
        mock_grep.return_value = ['arch/x86_64/Kconfig']
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, (set(['arch/x86_64/Kconfig']), [defs.CONFIG_LABEL]))

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            result = common.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = common.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_match_single_label(self):
        # No match
        results = common.match_single_label('readyForMerge', self.YAML_LABELS)
        self.assertEqual(results, None)

        # Basic match
        results = common.match_single_label('Acks::OK', self.YAML_LABELS)
        self.assertEqual(results, self.YAML_LABELS[0])

        # Regex match
        results = common.match_single_label('Subsystem:Cookies', self.YAML_LABELS)
        expected = {'name': 'Subsystem:Cookies', 'color': '#778899', 'description': 'hi Cookies'}
        self.assertEqual(results, expected)

    @mock.patch('webhook.common.load_yaml_data')
    def test_validate_labels(self, mock_load_yaml):
        # No data generates a runtime error
        mock_load_yaml.return_value = None
        raised = False
        try:
            results = common.validate_labels(['hi', 'there'], 'utils/labels.yaml')
        except RuntimeError:
            raised = True
        mock_load_yaml.assert_called_with('utils/labels.yaml')
        self.assertTrue(raised)

        # Find some labels
        mock_load_yaml.return_value = {'labels': self.YAML_LABELS}
        results = common.validate_labels(['Acks::OK', 'Subsystem:net'], 'yaml_path')
        expected = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                    {'name': 'Subsystem:net', 'color': '#778899', 'description': 'hi net'}
                    ]
        mock_load_yaml.assert_called_with('yaml_path')
        self.assertEqual(results, expected)

        # Can't find it!
        raised = False
        try:
            results = common.validate_labels(['Acks::OK', 'Frogs::OK', 'Subsystem:net'], 'yaml')
        except RuntimeError as err:
            raised = True
            self.assertIn("unknown: ['Frogs::OK']", err.args[0])
        mock_load_yaml.assert_called_with('yaml')
        self.assertTrue(raised)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_do_not_run_hook(self, is_prod):
        proj = mock.Mock()
        proj.archived = True
        proj.path_with_namespace = "test/path"
        mreq = mock.Mock()
        mreq.iid = 666
        mreq.state = "closed"
        hook_name = "foobar"
        run_on_drafts = False
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, MR is in draft state",
                          logs.output[-1])
        run_on_drafts = True
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, MR is close",
                          logs.output[-1])
        mreq.state = "opened"
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, project is archived",
                          logs.output[-1])

    @mock.patch('webhook.common.get_authlevel')
    def test_get_commits_count(self, authlevel):
        mreq = mock.Mock()
        proj = mock.Mock()
        mreq.iid = 19
        commits = [1, 2, 3, 4]
        mreq.commits.return_value = commits
        mreq.author = mock.MagicMock(id=1234)
        authlevel.return_value = 30  # Developer access
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 4)
        i = 5
        while i < 2002:
            commits.append(i)
            i += 1
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            count, _ = common.get_commits_count(proj, mreq)
            self.assertEqual(count, 2001)
            self.assertIn("MR 19 has 2001 commits, too many to process -- wrong target branch?",
                          logs.output[-1])
        mreq.commits.return_value = []
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 0)

    def test_match_gl_username_to_email(self):
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search, iterator: [mock.Mock(username=search.split('@')[0])]
        email = "joedev@redhat.com"
        username = "joedev"
        self.assertTrue(common.match_gl_username_to_email(gl_instance, email, username))
        username = "biff"
        self.assertFalse(common.match_gl_username_to_email(gl_instance, email, username))
        username = None
        self.assertFalse(common.match_gl_username_to_email(gl_instance, email, username))
        username = 'what'
        email = ''
        self.assertFalse(common.match_gl_username_to_email(gl_instance, email, username))

    @mock.patch('cki_lib.misc.is_production', mock.Mock())
    def test_update_webhook_comment(self):
        gl_mergerequest = mock.Mock()
        discussion = mock.Mock(attributes={'notes': [self.NATTRS]})
        # discussion.notes.get.return_value = '1234'
        gl_mergerequest.discussions.list.return_value = [discussion]
        bot_name = "shadowman"
        identifier = "This should be unique per webhook"
        new_comment = "This is my new comment"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common.update_webhook_comment(gl_mergerequest, bot_name, identifier, new_comment)
            self.assertIn("Overwriting existing webhook comment", ' '.join(logs.output))
            gl_mergerequest.notes.create.assert_not_called()
        bot_name = "a_different_user"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common.update_webhook_comment(gl_mergerequest, bot_name, identifier, new_comment)
            self.assertIn("Creating new webhook comment", ' '.join(logs.output))
            gl_mergerequest.notes.create.assert_called()

    def test_get_mr_state_from_event(self):
        """Returns the state string value, if any, or None."""
        payload = {'object_attributes': {'state': 'opened'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'opened')
        payload = {'object_attributes': {}}
        self.assertEqual(common.get_mr_state_from_event(payload), None)
        payload = {'object_attributes': {}, 'merge_request': {'state': 'closed'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'closed')
        payload = {'object_attributes': None, 'merge_request': None}
        self.assertEqual(common.get_mr_state_from_event(payload), None)

    def test_make_logging_extras(self):
        """Returns a dict of some extra logging values."""
        # ampq-bridge event happy with nothing
        mock_body = {}
        mock_headers = {}
        expected = {'bugzilla_id': None,
                    'bugzilla_user': None
                    }
        results = common._make_ampq_logging_extras(mock_headers, mock_body)
        self.assertEqual(results, expected)

        # ampq-bridge event with something to say
        mock_body = {'event': {'bug_id': 123456, 'user': {'login': 'user@example.com'}}}
        expected = {'bugzilla_id': mock_body['event']['bug_id'],
                    'bugzilla_user': mock_body['event']['user']['login']
                    }
        results = common._make_ampq_logging_extras(mock_headers, mock_body)
        self.assertEqual(results, expected)

        # umb-bridge event happy with nothing
        mock_body = {}
        mock_headers = {}
        results = common._make_umb_bridge_logging_extras(mock_headers, mock_body)
        self.assertEqual(results, {})

        # umb-bridge event with something to say
        mock_headers = {'mrpath': 'group/project!3456'}
        expected = {'mr_id': 3456,
                    'path_with_namespace': 'group/project'
                    }
        results = common._make_umb_bridge_logging_extras(mock_headers, mock_body)
        self.assertEqual(results, expected)

    @mock.patch('cki_lib.misc.is_production')
    def test_create_note_empty(self, mock_is_production):
        """Raises a ValueError exception when the note string is empty."""
        object_iid = 5432
        new_note_id = 1356
        mock_object = mock.Mock(name='MergeRequest', iid=object_iid)
        mock_object.notes.create.return_value = mock.Mock(id=new_note_id)

        # No text, raise an error.
        with self.assertRaises(ValueError):
            common.create_note(mock_object, '')
        mock_object.notes.create.assert_not_called()

    @mock.patch('cki_lib.misc.is_production')
    def test_create_note(self, mock_is_production):
        """Creates a note on the given object and returns its ID."""
        note_text = 'my great idea'
        object_iid = 5432
        new_note_id = 1356
        mock_object = mock.Mock(spec=['iid', 'name', 'notes'], name='MergeRequest', iid=object_iid)
        mock_object.notes.create.return_value = mock.Mock(spec=['id'], id=new_note_id)

        # Not production, returns object IID.
        mock_is_production.return_value = False
        self.assertEqual(common.create_note(mock_object, note_text), object_iid)
        mock_object.notes.create.assert_not_called()

        # Production, returns new note ID.
        mock_is_production.return_value = True
        self.assertEqual(common.create_note(mock_object, note_text), new_note_id)
        mock_object.notes.create.assert_called_once_with({'body': note_text})

        # Production but create didn't return a new note?
        mock_object.notes.create.reset_mock()
        mock_object.notes.create.return_value = False
        self.assertIs(common.create_note(mock_object, note_text), None)
        mock_object.notes.create.assert_called_once_with({'body': note_text})

        # Create a note that is a quickaction (no 'id' returned by the API).
        note_text = '/label "readyForMerge"'
        mock_object.notes.create.reset_mock()
        mock_object.notes.create.return_value = mock.Mock(spec_set=[])
        self.assertIs(common.create_note(mock_object, note_text), 0)
        mock_object.notes.create.assert_called_once_with({'body': note_text})

    @mock.patch('cki_lib.misc.is_production')
    def test_create_mr_pipeline(self, mock_is_production):
        """Creates a new pipeline and returns its ID."""
        mock_mr = mock.Mock(iid=2345)
        mock_mr.pipelines.create.return_value = mock.Mock(id=536737)

        # Not production, returns MR IID.
        mock_is_production.return_value = False
        self.assertEqual(common.create_mr_pipeline(mock_mr), 2345)
        mock_mr.pipelines.create.assert_not_called()

        # Production, calls create and returns pipeline ID.
        mock_is_production.return_value = True
        self.assertEqual(common.create_mr_pipeline(mock_mr), 536737)
        mock_mr.pipelines.create.assert_called_once()

    @mock.patch('cki_lib.misc.is_production')
    def test_cancel_pipeline(self, mock_is_production):
        """Cancels an existing pipeline."""
        mock_project = mock.Mock()
        mock_project.pipelines.get.return_value.cancel.return_value = {'status': 'canceled'}

        # Not production, doesn't call cancel().
        mock_is_production.return_value = False
        self.assertIs(common.cancel_pipeline(mock_project, 12345), True)
        mock_project.pipelines.get.return_value.cancel.assert_not_called()

        # Production, success.
        mock_is_production.return_value = True
        self.assertIs(common.cancel_pipeline(mock_project, 12345), True)
        mock_project.pipelines.get.assert_called_once_with(12345)
        mock_project.pipelines.get.return_value.cancel.assert_called_once()

        # Production, failure.
        mock_project.pipelines.get.reset_mock()
        mock_project.pipelines.get.return_value.cancel.reset_mock()
        mock_project.pipelines.get.return_value.cancel.return_value = {'status': 'running'}
        self.assertIs(common.cancel_pipeline(mock_project, 12345), False)
        mock_project.pipelines.get.assert_called_once_with(12345)
        mock_project.pipelines.get.return_value.cancel.assert_called_once()

    @mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
    @mock.patch('webhook.libbz.update_bug_status')
    @mock.patch('webhook.libbz.bugs_to_move_to_post')
    @mock.patch('webhook.libbz.fetch_bugs')
    def test_set_mr_bugs_status(self, mock_fetch_bugs, mock_bugs_to_post, mock_update_status):
        """Calls update_bug_status for an MR's bugs."""
        bz_id = 1234567
        dep_id = 3456789
        cve_id = 'CVE-2022-12345'
        description = f'Bugzilla: https://bugzilla.redhat.com/{bz_id}\nCVE: {cve_id}'
        mr_attributes = {'description': description,
                         'draft': True,
                         'project_id': 56789,
                         'target_branch': 'main',
                         'head_pipeline': {'finished_at': '10pm'}
                         }

        # MR is draft, nothing to do.
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        common.set_mr_bugs_status(gl_mr, defs.BZStatus.POST)
        mock_fetch_bugs.assert_not_called()

        # No CVE so just the Bugzilla: items to update.
        mr_attributes['draft'] = False
        mr_attributes['description'] = f'Bugzilla: https://bugzilla.redhat.com/{bz_id}'
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_bz = mock.Mock()
        mock_bz.id = bz_id
        mock_fetch_bugs.side_effect = [[mock_bz]]
        mock_bugs_to_post.return_value = [mock_bz]

        common.set_mr_bugs_status(gl_mr, defs.BZStatus.POST)
        mock_fetch_bugs.assert_called_once_with({bz_id}, clear_cache=True)
        mock_update_status.assert_called_with([mock_bz], defs.BZStatus.POST)

        # MR with a CVE. Move the bz and corresponding rt bz to MODIFIED..
        mock_fetch_bugs.reset_mock()
        mock_bugs_to_post.reset_mock()
        mock_update_status.reset_mock()
        mr_attributes['description'] = description
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_bz = mock.Mock()
        mock_bz.id = bz_id
        mock_cve = mock.Mock(depends_on=[dep_id])
        mock_cve.id = cve_id
        mock_dep = mock.Mock(product='Red Hat Enterprise Linux 8', component='kernel-rt',
                             cf_internal_target_release='8.7.0', version='8.7',
                             spec=['product', 'component', 'cf_internal_target_release', 'version'])
        mock_dep.id = dep_id
        mock_fetch_bugs.side_effect = [[mock_bz], [mock_cve], [mock_dep]]

        common.set_mr_bugs_status(gl_mr, defs.BZStatus.MODIFIED)
        mock_update_status.assert_called_with([mock_bz, mock_dep], defs.BZStatus.MODIFIED,
                                              defs.BZStatus.POST)

    @mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
    @mock.patch('webhook.libjira.update_issue_status')
    @mock.patch('webhook.libjira.issues_to_move_to_in_progress')
    @mock.patch('webhook.libjira.fetch_issues')
    def test_set_mr_issues_status(self, mock_fetch_issues, mock_issues_to_in_prog,
                                  mock_update_status):
        """Calls update_issue_status for an MR's issues."""
        ji_id = f'{defs.JPFX}1234567'
        cve_id = 'CVE-2022-12345'
        description = f'JIRA: {defs.JIRA_SERVER}browse/{ji_id}\nCVE: {cve_id}'
        mr_attributes = {'description': description,
                         'draft': True,
                         'project_id': 56789,
                         'target_branch': 'main',
                         'head_pipeline': {'finished_at': '10pm'}
                         }

        # MR is draft, nothing to do.
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        common.set_mr_issues_status(gl_mr, defs.JIStatus.DEVELOPMENT)
        mock_fetch_issues.assert_not_called()

        # No CVE so just the JIRA: items to update.
        mr_attributes['draft'] = False
        mr_attributes['description'] = f'JIRA: {defs.JIRA_SERVER}browse/{ji_id}'
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_ji = mock.Mock()
        mock_ji.id = ji_id
        mock_fetch_issues.side_effect = [[mock_ji]]
        mock_issues_to_in_prog.return_value = [mock_ji]

        common.set_mr_issues_status(gl_mr, defs.JIStatus.DEVELOPMENT)
        mock_fetch_issues.assert_called_once_with({ji_id}, clear_cache=True)
        mock_update_status.assert_called_with([mock_ji], defs.JIStatus.DEVELOPMENT)

        # MR with a CVE. Move the issue and corresponding rt issue to Testable.
        mock_fetch_issues.reset_mock()
        mock_issues_to_in_prog.reset_mock()
        mock_update_status.reset_mock()
        mr_attributes['description'] = description
        gl_mr = fakes.FakeMergeRequest(123, mr_attributes, actual_attributes=mr_attributes)
        mock_ji = mock.Mock()
        mock_ji.id = ji_id
        mock_fetch_issues.side_effect = [[mock_ji]]

        common.set_mr_issues_status(gl_mr, defs.JIStatus.READY_FOR_QA)
        mock_update_status.assert_called_with([mock_ji], defs.JIStatus.READY_FOR_QA,
                                              defs.JIStatus.DEVELOPMENT)

    def test_get_pog_member(self):
        """Returns a matching Member object if the given username is in the project or group."""
        mock_project = mock.Mock()
        user1 = mock.Mock(username='william')
        user2 = mock.Mock(username='will')
        user3 = mock.Mock(username='willy')

        # Finds the members_all user.
        mock_project.members_all.list.return_value = [user1, user2, user3]
        self.assertIs(common.get_pog_member(mock_project, 'will'), user2)
        mock_project.members_all.list.assert_called_with(iterator=True, query='will')
        self.assertIs(common.get_pog_member(mock_project, 'will123'), None)
        mock_project.members_all.list.assert_called_with(iterator=True, query='will123')
        mock_project.members.list.assert_not_called()

        # Finds only direct members
        mock_project.members_all.list.reset_mock(return_value=True)
        mock_project.members.list.return_value = [user1, user2, user3]
        self.assertIs(common.get_pog_member(mock_project, 'will', include_inherited=False), user2)
        mock_project.members_all.list.assert_not_called()

    def test_wait_for_new_mrs_no_sleep(self):
        """Does not sleep since the MR is old."""
        # Payload without MR details, no sleep.
        payload = deepcopy(fake_payloads.PUSH_PAYLOAD)
        with mock.patch('webhook.common.sleep') as mock_sleep:
            common.wait_for_new_mrs(payload)
            mock_sleep.assert_not_called()

        # Super old MR payload, no sleep.
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        self.assertEqual(payload['object_attributes']['created_at'], '2013-12-03T17:23:34Z')
        with mock.patch('webhook.common.sleep') as mock_sleep:
            common.wait_for_new_mrs(payload)
            mock_sleep.assert_not_called()

    def test_wait_for_new_mrs_sleep(self):
        """Sleeps for some time."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        with mock.patch('webhook.common.sleep') as mock_sleep:
            for _ in range(100):
                mock_sleep.reset_mock()
                # Create a payload timestamp that is up to DELAY_MAX seconds old.
                random_delay_secs = uniform(0, common.WAIT_FOR_NEW_MRS_DELAY_MAX)
                recent_time = datetime.now() - timedelta(seconds=random_delay_secs)
                payload['merge_request']['created_at'] = recent_time.isoformat()
                common.wait_for_new_mrs(payload)
                mock_sleep.assert_called_once()
                sleepy_time = mock_sleep.call_args[0][0]
                self.assertTrue(
                    0 <= sleepy_time <=
                    common.WAIT_FOR_NEW_MRS_DELAY_MAX + common.WAIT_FOR_NEW_MRS_DELAY_RANGE
                )


class TestActiveBranch(TestCase):

    PAYLOAD = {'object_kind': None,
               'project': {'id': 12345, 'name': 'cool_project'},
               'merge_request': {'target_branch': '10.0'},
               'object_attributes': {'target_branch': '11.3'}
               }

    @mock.patch('webhook.common.is_branch_active')
    def test_unknown_object_kind(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        # No object_kind, returns None.
        mock_projects = mock.Mock()
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertEqual(common.is_event_target_branch_active(mock_projects, self.PAYLOAD),
                             None)
            mock_is_branch_active.assert_not_called()
            self.assertIn('No handler', logs.output[-2])
            self.assertIn('Result is None for kind None', logs.output[-1])

    @mock.patch('webhook.common.is_branch_active')
    def test_mr_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'merge_request'
        mock_is_branch_active.return_value = True
        mock_projects = mock.Mock()
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_called_with(mock_projects, payload['project']['id'],
                                                     payload['object_attributes']['target_branch'])
            self.assertIn('Result is True for kind merge_request', logs.output[-1])

    @mock.patch('webhook.common.is_branch_active')
    def test_build_event(self, mock_is_branch_active):
        """Should return True."""
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'build'
        mock_projects = mock.Mock()
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_not_called()
            self.assertIn('Result is True for kind build', logs.output[-1])

    @mock.patch('webhook.common.is_branch_active')
    def test_note_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        # An MR-related note event.
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'note'
        payload['object_attributes']['noteable_type'] = 'MergeRequest'
        mock_is_branch_active.return_value = True
        mock_projects = mock.Mock()
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_called_with(mock_projects, payload['project']['id'],
                                                     payload['merge_request']['target_branch'])
            self.assertIn('Result is True for kind note', logs.output[-1])

        # A note event for something else.
        mock_is_branch_active.reset_mock()
        payload['object_attributes']['noteable_type'] = 'Issue'
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertEqual(common.is_event_target_branch_active(mock_projects, payload), None)
            mock_is_branch_active.assert_not_called()
            self.assertIn('Result is None for kind note', logs.output[-1])

    @mock.patch('webhook.common.is_branch_active')
    def test_pipeline_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'pipeline'
        mock_projects = mock.Mock()

        # an upstream triggered event
        mock_is_branch_active.return_value = True
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_called_with(mock_projects, payload['project']['id'],
                                                     payload['merge_request']['target_branch'])
            self.assertIn('Result is True for kind pipeline', logs.output[-1])

        # a downstream event with variables
        mock_is_branch_active.reset_mock()
        payload.pop('merge_request')
        payload['object_attributes'] = {'variables': [{'key': 'branch', 'value': '5'},
                                                      {'key': 'mr_project_id', 'value': '54321'}
                                                      ]
                                        }
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_called_with(mock_projects, '54321', '5')
            self.assertIn('Result is True for kind pipeline', logs.output[-1])

        # some unhandled event?
        mock_is_branch_active.reset_mock()
        payload['object_attributes'].pop('variables')
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertEqual(common.is_event_target_branch_active(mock_projects, payload), None)
            mock_is_branch_active.assert_not_called()
            self.assertIn('Result is None for kind pipeline', logs.output[-1])

    @mock.patch('webhook.common.is_branch_active')
    def test_push_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        payload = deepcopy(self.PAYLOAD)
        del payload['merge_request']
        del payload['object_attributes']
        payload['object_kind'] = 'push'
        payload['ref'] = 'refs/heads/main-rt'
        mock_projects = mock.Mock()

        # A push event.
        mock_is_branch_active.return_value = True
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            self.assertTrue(common.is_event_target_branch_active(mock_projects, payload))
            mock_is_branch_active.assert_called_with(mock_projects, payload['project']['id'],
                                                     'main-rt')
            self.assertIn('Result is True for kind push', logs.output[-1])
