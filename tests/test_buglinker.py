"""Webhook interaction tests."""
from copy import deepcopy
import json
import os
from unittest import TestCase
from unittest import mock
from xmlrpc.client import Fault

from bugzilla import BugzillaError
from gitlab import GitlabGetError

from tests import fakes
from webhook import buglinker
from webhook import defs
from webhook.pipelines import PipelineType


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBuglinker(TestCase):
    """ Test Webhook class."""

    BZ_RESULTS = [{'id': 1234567,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-02315 kernel: a bad one',
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': defs.EXT_TYPE_URL
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'CVE-2020-1234 kernel-rt: another one',
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': defs.EXT_TYPE_URL
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'product': 'Red Hat Enterprise Linux 8',
                   'version': '8.2',
                   'summary': 'A kernel bug?',
                   'external_bugs': []
                   }]

    def test_get_bugs(self):
        bzcon = mock.Mock()

        # No results.
        bzcon.getbugs.return_value = None
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            results = buglinker.get_bugs(bzcon, [123])
            self.assertEqual(results, None)
            self.assertIn('getbugs() returned an empty list for these bugs: [123]', logs.output[-1])

        # Results.
        bzcon.getbugs.return_value = [456]
        results = buglinker.get_bugs(bzcon, [123])
        self.assertEqual(results, bzcon.getbugs.return_value)

        # bzcon raises an exception.
        bzcon.getbugs.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_bugs(bzcon, ['burp'])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error getting bugs.', logs.output[-1])

    @mock.patch('webhook.buglinker.unlink_mr_from_bzs')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    @mock.patch('webhook.buglinker.get_bugs')
    def test_update_bugzilla(self, mock_get_bugs, mock_link, mock_unlink):
        """Check for expected calls."""
        namespace = 'redhat/rhel/8.y/kernel'
        mr_id = 10
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Call add_mr_to_bz() with the expected parameters.
        bugs = {'link': [252626, 739272, 1544362],
                'unlink': [1234567]
                }
        bz0 = mock.Mock()
        bz1 = mock.Mock()
        bz2 = mock.Mock()
        mock_get_bugs.return_value = [bz0, bz1, bz2]

        buglinker.update_bugzilla(bugs, namespace, mr_id, bzcon)
        mock_get_bugs.assert_called_with(bzcon, [252626, 739272, 1544362])
        mock_link.assert_called_once_with(mock_get_bugs.return_value, ext_bz_bug_id, bzcon)
        mock_unlink.assert_called_once_with([1234567], ext_bz_bug_id, bzcon)

    def test_bz_is_linked_to_mr(self):
        """Check for expected return values."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        self.assertTrue(buglinker.bz_is_linked_to_mr(bz0, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz1, ext_bz_bug_id))
        self.assertFalse(buglinker.bz_is_linked_to_mr(bz2, ext_bz_bug_id))

    def test_parse_cve_from_summary(self):
        """Check for expected return values."""
        summary = 'CVE-2020-02345 a bad problem.'
        self.assertTrue(buglinker.parse_cve_from_summary(summary))
        summary = 'kernel bug (again)'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)
        summary = 'CVE-123-abcd kernel: the worst problem.'
        self.assertEqual(buglinker.parse_cve_from_summary(summary), None)

    @mock.patch('webhook.buglinker.get_bugs')
    def test_get_rt_cve_bugs(self, mock_get_bugs):
        bz0 = mock.Mock(id=11223344, summary='CVE-2020-12345 kernel: a problem to fix.')
        bz1 = mock.Mock(id=22334455, summary='kernel: another problem to fix.')
        bz2 = mock.Mock(id=33445566, summary='CVE-2021-4535 kernel: again a problem to fix.')
        bz3 = mock.Mock(id=44556677, summary='CVE-1996-01163 kernel-rt: how did we miss this?')
        bz4 = mock.Mock(id=55667788, summary='kernel stuff')
        bzcon = mock.Mock()

        # No CVE bugs.
        mock_get_bugs.return_value = [bz1, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz1.id, bz4.id])
        self.assertEqual(result, [])
        bzcon.query.assert_not_called()

        # Three CVE bugs.
        mock_get_bugs.return_value = [bz0, bz1, bz2, bz3, bz4]
        result = buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
        self.assertEqual(result, bzcon.query.return_value)
        self.assertEqual(sorted(bzcon.query.call_args.args[0]['v1']),
                         ['CVE-1996-01163', 'CVE-2020-12345', 'CVE-2021-4535'])

        # bzcon raises an exception.
        bzcon.query.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.get_rt_cve_bugs(bzcon, [bz0.id, bz1.id, bz2.id, bz3.id, bz4.id])
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error querying bugzilla.', logs.output[-1])

    @mock.patch('cki_lib.misc.is_production')
    @mock.patch('webhook.buglinker.get_rt_cve_bugs', mock.Mock(return_value=[]))
    def test_unlink_mr_from_bzs(self, mock_production):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon = mock.Mock()

        # Not production. Don't do anything.
        mock_production.return_value = False
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.unlink_mr_from_bzs([1234567], ext_bz_bug_id, bzcon)
            self.assertIn(f'Unlinking {ext_bz_bug_id} from BZ1234567.', logs.output[-1])
            bzcon.remove_external_tracker.assert_not_called()

        # Bugzilla Exception.
        mock_production.return_value = True
        bzcon.remove_external_tracker.side_effect = BugzillaError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except BugzillaError:
                exception_hit = True
            self.assertFalse(exception_hit)

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=1006,
                                                          faultString='no link to remove.')
        with self.assertLogs('cki.webhook.buglinker', level='WARNING') as logs:
            exception_hit = False
            try:
                buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
            except Fault:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('xmlrpc fault 1006:', logs.output[-1])

        # Known xmlrpc Fault.
        bzcon.remove_external_tracker.side_effect = Fault(faultCode=123, faultString='oh no!')
        exception_hit = False
        try:
            buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        except Fault:
            exception_hit = True
        self.assertTrue(exception_hit)

        # Go Go Go.
        bzcon.remove_external_tracker.reset_mock(side_effect=True)

        buglinker.unlink_mr_from_bzs([1234567, 2345678], ext_bz_bug_id, bzcon)
        self.assertEqual(bzcon.remove_external_tracker.call_count, 2)

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_link_mr_to_bzs(self):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        ext_type_url = defs.EXT_TYPE_URL
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel-rt',
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        product='Red Hat Enterprise Linux 8',
                        component='kernel',
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        bzcon = mock.Mock()

        # Early return due to no untracked bugs
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            buglinker.link_mr_to_bzs([bz0], ext_bz_bug_id, bzcon)
            self.assertIn(f'All bugs have an existing link to {ext_bz_bug_id}.', logs.output[-1])

        # bzcon raises an exception.
        bzcon.add_external_tracker.side_effect = BugzillaError('oh no!')
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
                exception_hit = False
                try:
                    buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
                except BugzillaError:
                    exception_hit = True
                self.assertFalse(exception_hit)
                self.assertIn(f'Problem adding tracker {ext_bz_bug_id} to BZs.', logs.output[-1])
                bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                              ext_type_url=ext_type_url,
                                                              ext_bz_bug_id=ext_bz_bug_id)

        # Successful call to add_external_tracker()
        bzcon.add_external_tracker.side_effect = None
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            buglinker.link_mr_to_bzs([bz0, bz1, bz2], ext_bz_bug_id, bzcon)
        bzcon.add_external_tracker.assert_called_with([bz1.id, bz2.id],
                                                      ext_type_url=ext_type_url,
                                                      ext_bz_bug_id=ext_bz_bug_id)

    @mock.patch('webhook.buglinker.comment_already_posted')
    @mock.patch('webhook.buglinker.get_rt_cve_bugs')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_to_bugs(self, mock_rt_cve_bugs, mock_comment_check):
        res = mock.Mock()
        res.json.return_value = {'result': {'id': 123}, 'version': '1.1'}
        sess = mock.Mock()
        sess.post.return_value = res
        bzcon = mock.Mock()
        bzcon.get_requests_session.return_value = sess

        bz1 = mock.Mock(id=11223344)
        bz2 = mock.Mock(id=22334455)
        bz3 = mock.Mock(id=33445566)
        bz4 = mock.Mock(id=44556677)

        # No bug objects, nothing to return.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = None

            result = buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', PipelineType.RHEL,
                                            bzcon)
            self.assertEqual(result, None)
            bzcon.update_bugs.assert_not_called()

        # Comment already posted to all bugs so don't return
        # anything (bugs should already be linked).
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = [bz1, bz2]
            mock_comment_check.return_value = True

            result = buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', PipelineType.CENTOS,
                                            bzcon)
            self.assertEqual(result, None)
            self.assertIn('This pipeline has already been posted to all relevant bugs.',
                          logs.output[-1])
            self.assertEqual(mock_comment_check.call_count, 2)
            bzcon.update_bugs.assert_not_called()

        # Succesful comment, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.reset_mock(return_value=True)
            mock_comment_check.side_effect = [False, True]

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', PipelineType.CENTOS, bzcon)
            self.assertIn('Updating these bugs [11223344] with comment', logs.output[-1])

        # Succesful comment on RT pipeline, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            pipe_type = PipelineType.get('distro_realtime_check_merge_request')
            mock_comment_check.reset_mock(return_value=True, side_effect=True)
            mock_comment_check.side_effect = [True, True, False, True]
            mock_rt_cve_bugs.return_value = [bz3, bz4]

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', pipe_type, bzcon)
            self.assertIn('Updating these bugs [33445566] with comment', logs.output[-1])

        # Updates all the bugs but only once.
        mock_comment_check.reset_mock(side_effect=True, return_value=True)
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.reset_mock(return_value=True, side_effect=True)
            pipe_type = PipelineType.get('distro_realtime_check_merge_request')
            mock_comment_check.return_value = False
            mock_rt_cve_bugs.return_value = [bz3, bz4]

            buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', pipe_type, bzcon)
            self.assertIn('Updating these bugs [11223344, 22334455, 33445566, 44556677]',
                          logs.output[-1])

    @mock.patch.dict(os.environ, {'BUGZILLA_EMAIL': 'kwf@example.com'})
    def test_comment_already_posted(self):
        pipeline_url = f'{defs.GITFORGE}/project/-/pipelines/12345'
        pipeline_text = f'Downstream Pipeline: {pipeline_url}'
        comment1 = {'creator': 'user@redhat.com', 'text': 'cool comment', 'count': 1}
        comment2 = {'creator': os.environ['BUGZILLA_EMAIL'],
                    'text': f'hey there\n{pipeline_text}\nthanks', 'count': 2}
        bug = mock.Mock()
        bug.id = 678910

        # comment2 should return True.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug.getcomments = mock.Mock(return_value=[comment1, comment2])
            self.assertTrue(buglinker.comment_already_posted(bug, pipeline_url))
            self.assertIn('Excluding bug 678910 as pipeline was already posted in comment 2.',
                          logs.output[-1])

        # comment1 is False.
        bug.getcomments = mock.Mock(return_value=[comment1])
        self.assertFalse(buglinker.comment_already_posted(bug, pipeline_url))

    @mock.patch('webhook.buglinker.make_jissues')
    @mock.patch('webhook.buglinker.update_testable_builds')
    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.buglinker.process_bridge_jobs')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.create_artifacts_text')
    @mock.patch('webhook.buglinker.post_to_bugs')
    @mock.patch('webhook.buglinker.link_mr_to_bzs')
    def test__process_pipeline(self, mock_link, mock_post, mock_create, mock_bz_conn,
                               mock_process_bridge_jobs, mock_add_label, mock_remove_label,
                               mock_update_builds, mock_make_jissues):
        """Processes the pipeline and posts to bugzilla/jira."""
        mock_create.return_value = 'artifact text'
        bz_id = 7654321
        jissue_id = 9999
        mock_branch = mock.Mock(pipelines=[PipelineType.REALTIME, PipelineType.AUTOMOTIVE])
        mock_branch.name = 'main-rt'
        mr_id = 123
        pipeline_id = 234
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(5347, 'test project')
        mr_attributes = {'description': '', 'title': 'a fake MR', 'web_url': 'https://etc/etc',
                         'head_pipeline': {'id': pipeline_id}}
        mock_mr = mock_project.add_mr(mr_id, actual_attributes=mr_attributes)
        mock_pipeline = mock_project.add_pipeline(pipeline_id)
        mock_pipeline.web_url = 'https://pipeline_web_url'

        # No Bugzilla tags in the description, nothing to do.
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_process_bridge_jobs.assert_not_called()

        # No Bugzilla connection, can't do anything.
        mock_mr.description = f'Bugzilla: https://bugzilla.redhat.com/{bz_id}'
        mock_process_bridge_jobs.side_effect = [{PipelineType.REALTIME: {'checkout': 'data'}}]
        mock_bz_conn.return_value = False
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_process_bridge_jobs.assert_called_once()
        mock_post.assert_not_called()
        mock_make_jissues.assert_not_called()

        # Posts to bugs and because this is REALTIME, links BZs.
        mock_process_bridge_jobs.reset_mock(side_effect=True)
        checkout_data = {'pipeline_url': 'https://gitlab/pipeline/url'}
        mock_process_bridge_jobs.side_effect = [{PipelineType.REALTIME: checkout_data}]
        mock_bz_conn.return_value = True
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_create.assert_called_once()
        mock_link.assert_called_once()
        mock_add_label.assert_not_called()
        mock_remove_label.assert_not_called()
        mock_make_jissues.assert_not_called()

        # Posts to bugs and adds a needs testing label because all_sources_targeted is False.
        mock_process_bridge_jobs.reset_mock(side_effect=True)
        mock_process_bridge_jobs.side_effect = [{PipelineType.RHEL:
                                                 {'all_sources_targeted': False,
                                                  'pipeline_url': 'https://bla/bla/bla'}}]
        mock_branch.pipelines = [PipelineType.RHEL]
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_add_label.assert_called_once()
        mock_remove_label.assert_not_called()
        mock_make_jissues.assert_not_called()

        # Posts to bugs and removes a needs testing label because all_sources_targeted is True.
        mock_add_label.reset_mock()
        mock_remove_label.reset_mock()
        mock_process_bridge_jobs.reset_mock(side_effect=True)
        mock_process_bridge_jobs.side_effect = [{PipelineType.CENTOS:
                                                 {'all_sources_targeted': True,
                                                  'pipeline_url': 'https://bla/bla/bla'}}]
        mock_branch.pipelines = [PipelineType.CENTOS]
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_remove_label.assert_called_once()
        mock_add_label.assert_not_called()
        mock_make_jissues.assert_not_called()

        # A wild jira appears.
        mock_mr.description = f'JIRA: https://issues.redhat.com/browse/RHEL-{jissue_id}'
        mock_add_label.reset_mock()
        mock_remove_label.reset_mock()
        mock_process_bridge_jobs.reset_mock(side_effect=True)
        mock_process_bridge_jobs.side_effect = [{PipelineType.CENTOS:
                                                 {'all_sources_targeted': True,
                                                  'pipeline_url': 'https://bla/bla/bla'}}]
        buglinker._process_pipeline(mock_project, mock_mr, mock_branch)
        mock_make_jissues.assert_called_once()
        mock_update_builds.assert_called_once()
        mock_remove_label.assert_called_once()
        mock_add_label.assert_not_called()


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestEvenHandlers(TestCase):
    """ Test event handlers."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'group/project'
                                 },
                     'object_attributes': {'target_branch': 'main',
                                           'iid': 2,
                                           'state': 'opened',
                                           'action': 'update'
                                           },
                     'labels': [],
                     'changes': {}
                     }

    PAYLOAD_PIPELINE = {'object_kind': 'pipeline',
                        'project': {'id': 1,
                                    'path_with_namespace': 'group/project'},
                        'object_attributes': {'id': 22334455,
                                              'status': 'success'},
                        'merge_request': {'iid': 2,
                                          'title': 'a test merge request'}
                        }

    PAYLOAD_BUILD = {'object_kind': 'build',
                     'build_status': 'success',
                     'build_stage': 'setup',
                     'project_id': 1,
                     'pipeline_id': 22334455}

    @mock.patch('webhook.buglinker.parse_gitlab_url')
    @mock.patch('webhook.buglinker.check_associated_mr')
    @mock.patch('webhook.buglinker.get_variables')
    @mock.patch('webhook.buglinker.get_instance')
    @mock.patch('webhook.buglinker._process_pipeline')
    def test_process_job_event(self, mock_process, mock_getinstance, mock_vars,
                               mock_check, mock_parse):
        """Verify process_job_event calls _process_pipeline when it should.

        We don't need to test internals of check_associated_mr as they are
        tested in the pipeline event processing, so let's just mock it here.
        """
        mock_getinstance.return_value = mock.Mock()
        projects = mock.Mock()

        # Not a successful job
        payload = deepcopy(self.PAYLOAD_BUILD)
        payload['build_status'] = 'canceled'
        msg = mock.Mock(payload=payload)
        buglinker.process_job_event(msg, projects)
        mock_process.assert_not_called()

        # Not a setup job
        payload = deepcopy(self.PAYLOAD_BUILD)
        payload['build_stage'] = 'something'
        msg = mock.Mock(payload=payload)
        buglinker.process_job_event(msg, projects)
        mock_process.assert_not_called()

        # Not a job associated with an MR
        payload = deepcopy(self.PAYLOAD_BUILD)
        msg = mock.Mock(payload=payload)
        mock_vars.return_value = {}
        buglinker.process_job_event(msg, projects)
        mock_process.assert_not_called()

        # All good
        payload = deepcopy(self.PAYLOAD_BUILD)
        msg = mock.Mock(payload=payload)
        mock_vars.return_value = {'mr_url': 'https://gitlab.url/project/merge-requests/10'}
        mock_check.return_value = 123
        mock_parse.return_value = ('gitlab', mock.Mock())
        buglinker.process_job_event(msg, projects)
        mock_process.assert_called()

    @mock.patch('webhook.buglinker.remove_gitlab_link_in_issues')
    @mock.patch('webhook.buglinker.update_bugzilla')
    @mock.patch('webhook.common.draft_status')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.process_pipeline_event')
    def test_process_mr(self, mock_process_pipe, mock_trybz, mock_draftcheck, mock_update_bz,
                        mock_jira_unlink):
        path_with_namespace = self.PAYLOAD_MERGE['project']['path_with_namespace']
        merge_msg = deepcopy(self.PAYLOAD_MERGE)
        merge_msg['object_attributes']['description'] = ''
        msg = mock.Mock(payload=merge_msg)
        merge_request = mock.Mock(description='')
        project = mock.Mock()
        project.mergerequests.get.return_value = merge_request
        gl_instance = mock.Mock()
        gl_instance.projects.get.return_value = project
        projects = mock.Mock()

        # MR description doesn't list any bugs.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, False
            msg.gl_instance.return_value = gl_instance

            buglinker.process_mr_event(msg, projects)
            self.assertIn('No relevant bugs or jissues found in MR description.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # MR has not changed, ignore event.
        mr_description = 'Bugzilla: https://bugzilla.redhat.com/12345678'
        merge_msg['object_attributes']['description'] = mr_description
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, False

            buglinker.process_mr_event(msg, projects)
            self.assertIn('MR has not changed, ignoring event.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # MR is a Draft, ignore it.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = True, False
            merge_msg['object_attributes']['action'] = 'open'

            buglinker.process_mr_event(msg, projects)
            self.assertIn('MR is a Draft, ignoring.', logs.output[-1])
            mock_process_pipe.assert_not_called()
            mock_update_bz.assert_not_called()

        # No bugzilla connection.
        mock_draftcheck.return_value = False, False
        mock_trybz.return_value = False

        buglinker.process_mr_event(msg, projects)
        mock_trybz.assert_called_once()
        mock_process_pipe.assert_not_called()
        mock_update_bz.assert_not_called()

        # One bug to link.
        mock_bz = mock.Mock()
        mock_trybz.return_value = mock_bz
        merge_msg['changes'] = {'description': {'previous': 'Bad description',
                                                'current': mr_description}}
        bugs = {'link': {12345678},
                'unlink': set(),
                'link_jissue': set(),
                'unlink_jissue': set()
                }

        buglinker.process_mr_event(msg, projects)
        mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)

        # Just removed Draft (send to pipeline processor) and one bug to link.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = False, True

            buglinker.process_mr_event(msg, projects)
            self.assertIn('Sending MR event to pipeline processor.', logs.output[-1])
            mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)

        # MR changed to Draft state, unlink its bug, bugzilla case
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = True, True
            bugs = {'link': set(),
                    'unlink': {12345678},
                    'link_jissue': set(),
                    'unlink_jissue': set()
                    }

            buglinker.process_mr_event(msg, projects)
            self.assertIn('MR changed to Draft, unlinking bugs.', logs.output[-2])
            mock_update_bz.assert_called_with(bugs, path_with_namespace, 2, mock_bz)

        # MR changed to Draft state, unlink its bug, jira case
        mr_description = 'JIRA: https://issues.redhat.com/browse/RHEL-101'
        merge_msg['object_attributes']['description'] = mr_description
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_draftcheck.return_value = True, True
            bugs = {'link': set(),
                    'unlink': set(),
                    'link_jissue': set(),
                    'unlink_jissue': {'RHEL-101'}
                    }
            buglinker.process_mr_event(msg, projects)
            self.assertIn('MR changed to Draft, unlinking bugs.', logs.output[-2])
            mock_jira_unlink.assert_called_with(2, path_with_namespace, bugs['unlink_jissue'])

    @mock.patch('webhook.buglinker._process_pipeline')
    @mock.patch('webhook.buglinker.get_instance')
    @mock.patch('webhook.buglinker.check_associated_mr', mock.Mock())
    def test_process_pipeline(self, mock_get_instance, mock_process_pipeline):
        """Parses payload and calls _process_pipeline."""
        # Merge Request event.
        merge_msg = deepcopy(self.PAYLOAD_MERGE)
        mock_msg = mock.Mock(payload=merge_msg)
        project_name = merge_msg['project']['path_with_namespace']
        mr_id = merge_msg['object_attributes']['iid']
        mock_projects = mock.Mock()
        mock_gl = fakes.FakeGitLab()
        mock_project = mock_gl.add_project(123, project_name)
        mock_mr = mock_project.add_mr(mr_id)
        mock_mr.project_id = 123
        mock_mr.target_branch = 'main'
        mock_get_instance.return_value = mock_gl

        buglinker.process_pipeline_event(mock_msg, mock_projects)
        mock_process_pipeline.assert_called_once()

        # Pipeline event, no MR so nothing to do.
        mock_process_pipeline.reset_mock()
        merge_msg = deepcopy(self.PAYLOAD_PIPELINE)
        merge_msg['merge_request'] = None
        mock_msg = mock.Mock(payload=merge_msg)
        buglinker.process_pipeline_event(mock_msg, mock_projects)
        mock_process_pipeline.assert_not_called()

        # Pipeline event with MR, calls _process_pipeline.
        mock_process_pipeline.reset_mock()
        merge_msg = deepcopy(self.PAYLOAD_PIPELINE)
        mock_msg = mock.Mock(payload=merge_msg)
        buglinker.process_pipeline_event(mock_msg, mock_projects)
        mock_process_pipeline.assert_called_once()

    def test_get_artifact(self):
        mock_job = mock.Mock(id=1234567)

        # GitlabGetError error.
        mock_job.artifact.side_effect = GitlabGetError('oh no!')
        with self.assertLogs('cki.webhook.buglinker', level='WARNING') as logs:
            exception_hit = False
            try:
                buglinker.get_artifact(mock_job, 'kcidb_all.json')
            except GitlabGetError:
                exception_hit = True
            self.assertFalse(exception_hit)
            self.assertIn('Error getting path for job #1234567: kcidb_all.json',
                          logs.output[-1])

        # No error.
        mock_job.artifact.side_effect = False
        mock_job.artifact = mock.Mock(return_value='A cool file')

        result = buglinker.get_artifact(mock_job, 'rc')
        self.assertEqual(result, 'A cool file')

    @mock.patch('webhook.buglinker.get_artifact')
    def test_parse_kcidb_files(self, mock_get_artifact):
        """Returns a dict populated from job object and KCIDB data."""
        mock_job1_spec = {'id': 12345678,
                          'name': 'setup ppc64le',
                          'stage': 'setup',
                          'status': 'success',
                          'pipeline': {'web_url': f'{defs.GITFORGE}/pipeline/url'},
                          'web_url': f'{defs.GITFORGE}/group/project/-/jobs/12345678'
                          }
        mock_job1 = mock.Mock(spec_set=list(mock_job1_spec.keys()), **mock_job1_spec)
        mock_job1_kcidb = {
            'checkouts': [{'misc': {'kernel_version': '4.18.0-387.el8.mr123',
                                    'all_sources_targeted': True}}],
            'builds': [
                {'architecture': 'ppc64le',
                 'misc': {'debug': False},
                 'valid': True,
                 'output_files': [
                     {'name': 'kernel_package_url',
                      'url': 'https://package_url/ppc64le/kernel-1.2.3.ppc64le'},
                     {'name': 'kernel_browse_url',
                      'url': 'https://browse_url.com'},
                 ]}
            ]
        }
        mock_get_artifact.return_value = json.dumps(mock_job1_kcidb)

        result = buglinker.parse_kcidb_files([mock_job1])
        expected = {'all_sources_targeted': True,
                    'version': '4.18.0-387.el8.mr123',
                    'builds': [{
                        'arch': 'ppc64le',
                        'debug': False,
                        'browse_url': 'https://browse_url.com',
                        'repo_url': 'https://package_url/ppc64le/kernel-1.2.3.ppc64le'
                    }],
                    'pipeline_url': mock_job1_spec['pipeline']['web_url']
                    }
        self.assertEqual(result, expected)

    @mock.patch('webhook.buglinker.get_artifact')
    def test_parse_kcidb_files_no_builds(self, mock_get_artifact):
        """Verify we get {} if we can't get data about all builds."""
        mock_job_spec = {'id': 12345678,
                         'name': 'setup x86_64 debug',
                         'stage': 'setup',
                         'status': 'success',
                         'web_url': f'{defs.GITFORGE}/group/project/-/jobs/12345678'
                         }
        mock_job1 = mock.Mock(spec_set=list(mock_job_spec.keys()), **mock_job_spec)
        mock_job2 = mock.Mock(spec_set=list(mock_job_spec.keys()), **mock_job_spec)
        mock_kcidb = {
            'checkouts': [{'misc': {'kernel_version': '4.18.0-387.el8.mr123',
                                    'all_sources_targeted': False}}],
            'builds': [
                {'id': 'missing'},
                {'architecture': 'x86_64',
                 'misc': {'debug': True},
                 'valid': True,
                 'output_files': [
                     {'name': 'kernel_package_url',
                      'url': 'https://package_url/x86_64-debug/kernel-1.2.3.x86_64'},
                     {'name': 'kernel_browse_url',
                      'url': 'https://browse_url.com'},
                 ]}
            ]
        }
        mock_get_artifact.side_effect = [json.dumps(mock_kcidb), None]

        self.assertEqual(buglinker.parse_kcidb_files([mock_job1, mock_job2]), {})

    CHECKOUT_DATA = {'all_sources_targeted': False,
                     'builds': [{'arch': 's390x',
                                 'browse_url': ('https://redhat.com/internal/browse'
                                                '/123/5.14.0-100.el9.s390x/'),
                                 'debug': False,
                                 'repo_url': ('https://redhat.com/artifacts/browse'
                                              '/123/5.14.0-100.el9.s390x'),
                                 },
                                {'arch': 'x86_64',
                                 'browse_url': ('https://redhat.com/internal/browse'
                                                '/123/5.14.0-100.el9.x86_64/'),
                                 'debug': False,
                                 'repo_url': ('https://redhat.com/artifacts/browse'
                                              '/123/5.14.0-100.el9.x86_64'),
                                 },
                                {'arch': 'x86_64',
                                 'browse_url': ('https://redhat.com/internal/browse'
                                                '/123/5.14.0-100.el9.x86_64/'),
                                 'debug': True,
                                 'repo_url': ('https://redhat.com/artifacts/browse'
                                              '/123/5.14.0-100.el9.x86_64'),
                                 }],
                     'version': '5.14.0-100.el9'}

    def test_create_artifacts_text_private(self):
        """Returns a string with one Repo URL / Debug Repo URL pair."""
        pipeline_type = PipelineType.REALTIME
        checkout_data = deepcopy(self.CHECKOUT_DATA)
        checkout_data['public_project'] = False
        checkout_data['bridge_job_name'] = 'c9s_kernel_rt'
        checkout_data['pipeline_url'] = 'https://pipeline'

        result = buglinker.create_artifacts_text(checkout_data, pipeline_type)
        self.assertEqual(result.count(f'\nDownstream Pipeline: {checkout_data["pipeline_url"]}'),
                         1)
        self.assertEqual(result.count('\nRepo URL:'), 1)
        self.assertEqual(result.count('\nDebug Repo URL:'), 1)
        self.assertEqual(result.count('$basearch\n'), 2)
        self.assertEqual(result.count('\nArtifacts (RPMs):'), 3)

    def test_create_artifacts_text_public(self):
        """Returns a string with one Repo URL per arch."""
        pipeline_type = PipelineType.REALTIME
        checkout_data = deepcopy(self.CHECKOUT_DATA)
        checkout_data['public_project'] = True
        checkout_data['bridge_job_name'] = 'c9s_kernel_rt'
        checkout_data['pipeline_url'] = 'https://pipeline'

        result = buglinker.create_artifacts_text(checkout_data, pipeline_type)
        self.assertEqual(result.count('\nRepo URL:'), 3)
        self.assertEqual(result.count('\nDebug Repo URL:'), 0)
        self.assertEqual(result.count('$basearch\n'), 0)
        self.assertEqual(result.count('\nArtifacts (RPMs):'), 3)

    def test_get_pipeline_setup_job_ids(self):
        """Returns a list of the successful setup job ids, if any."""
        # Returns an empty list if there is no project or pipeline.
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(None, 1), [])
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(1, None), [])
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(None, None), [])

        # Returns an empty list if the pipeline was canceled.
        pipeline_id = 12345
        mock_project = fakes.FakeGitLabProject(4567, 'group/project')
        mock_project.add_pipeline(pipeline_id)
        mock_pipeline = mock_project.pipelines[pipeline_id]
        mock_pipeline.status = 'canceled'
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(mock_project, mock_pipeline), [])

        mock_job1 = mock.Mock(stage='setup', status='success')
        mock_job1.id = 1
        mock_job2 = mock.Mock(stage='test', status='failed')
        mock_job2.id = 2
        mock_job3 = mock.Mock(stage='merge', status='success')
        mock_job3.id = 3
        mock_job4 = mock.Mock(stage='setup', status='failed')
        mock_job4.id = 4
        mock_job5 = mock.Mock(stage='setup', status='success')
        mock_job5.id = 5
        mock_pipeline.status = 'success'

        # Returns an empty list if there are no successful setup jobs.
        mock_pipeline.jobs.list.return_value = [mock_job2, mock_job3, mock_job4]
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(mock_project, mock_pipeline), [])

        # Returns a list of the ids of the successful setup jobs.
        mock_pipeline.jobs.list.return_value = [mock_job1, mock_job2, mock_job3, mock_job4,
                                                mock_job5]
        self.assertEqual(buglinker.get_pipeline_setup_job_ids(mock_project, mock_pipeline),
                         [mock_job1.id, mock_job5.id])

    @mock.patch('webhook.buglinker.get_pipeline_setup_job_ids',
                wraps=buglinker.get_pipeline_setup_job_ids)
    def test_process_bridge_job(self, mock_get_setup_job_ids):
        """Returns a dict with setup job info from a completed bridge job (downstream pipeline)."""
        pipeline_id = 34567
        project_id = 65432
        mock_instance = fakes.FakeGitLab()
        mock_project = mock_instance.add_project(project_id, 'group/project')
        mock_project.add_pipeline(pipeline_id)
        mock_project.add_job(1, 'setup', 'success')
        mock_project.add_job(2, 'setup', 'success')
        mock_project.add_job(3, 'setup', 'success')
        mock_project.visibility = 'public'
        mock_ds_pipeline = mock_project.pipelines[pipeline_id]
        mock_ds_pipeline.status = 'canceled'

        # No downstream pipeline job ids, nothing to do.
        mock_bridge_job = mock.Mock()
        mock_bridge_job.name = 'c9s_merge_request'
        mock_bridge_job.downstream_pipeline = {'project_id': project_id, 'id': pipeline_id}
        self.assertEqual(buglinker.process_bridge_job(mock_instance, mock_bridge_job), {})

        # No checkout data so returns an empty dict.
        mock_ds_pipeline.status = 'success'
        mock_get_setup_job_ids.return_value = [1, 2, 3]
        with mock.patch('webhook.buglinker.parse_kcidb_files', return_value={}):
            self.assertEqual(buglinker.process_bridge_job(mock_instance, mock_bridge_job), {})

        # Some checkout data so we add the public_project key.
        with mock.patch('webhook.buglinker.parse_kcidb_files', return_value={'version': 1}):
            self.assertEqual(buglinker.process_bridge_job(mock_instance, mock_bridge_job),
                             {'public_project': True,
                              'version': 1,
                              'bridge_job_name': mock_bridge_job.name})

    def test_filter_bridge_job(self):
        """Returns True if the bridge job should be processed, otherwise False."""
        branch_pipelines = [PipelineType.RHEL, PipelineType.REALTIME]
        mock_branch = mock.Mock(pipelines=branch_pipelines)
        mock_branch.name = 'main'

        # No downstream pipeline, returns False.
        mock_bridge_job = mock.Mock(downstream_pipeline={})
        mock_bridge_job.name = 'auto_pipeline'
        self.assertIs(buglinker.filter_bridge_job(mock_bridge_job,
                                                  PipelineType.AUTOMOTIVE,
                                                  mock_branch),
                      False)

        # PipelineType is not in Branch.pipelines, returns None.
        mock_bridge_job.downstream_pipeline['status'] = 'success'
        self.assertIs(buglinker.filter_bridge_job(mock_bridge_job,
                                                  PipelineType.AUTOMOTIVE,
                                                  mock_branch),
                      False)

        # downstream_pipeline exists and pipe_type exists for Branch, returns True
        self.assertIs(buglinker.filter_bridge_job(mock_bridge_job,
                                                  PipelineType.REALTIME,
                                                  mock_branch),
                      True)

    @mock.patch('webhook.buglinker.process_bridge_job')
    def test_process_bridge_jobs(self, mock_process_bridge_job):
        """Returns a dict with each valid downstream pipeline and their checkout data."""
        mock_instance = fakes.FakeGitLab()
        mock_branch = mock.Mock(pipelines=[PipelineType.REALTIME,
                                           PipelineType.CENTOS])
        # A "good" downstream job.
        mock_job1 = mock.Mock(downstream_pipeline={'status': 'success'})
        mock_job1.name = 'realtime'
        mock_job1.id = 123
        # A "bad" downstream job, won't pass the filter check.
        mock_job2 = mock.Mock(downstream_pipeline={})
        mock_job2.name = 'centos'
        mock_job2.id = 456

        # mock_job1 is processed, mock_job2 is skipped. return dict has only job1.
        result = buglinker.process_bridge_jobs(mock_instance, mock_branch, [mock_job1, mock_job2])
        self.assertEqual(result, {PipelineType.REALTIME: mock_process_bridge_job.return_value})

        # Branch has rhel compat but its result is skipped because there is no valid REGULAR result.
        mock_branch.pipelines.append(PipelineType.RHEL_COMPAT)
        mock_job3 = mock.Mock(downstream_pipeline={'status': 'success'})
        mock_job3.name = 'rhel_compat'
        mock_job3.id = 789
        result = buglinker.process_bridge_jobs(mock_instance, mock_branch, [mock_job1, mock_job2,
                                                                            mock_job3])
        self.assertEqual(result, {PipelineType.REALTIME: mock_process_bridge_job.return_value})

        # Branch has RHEL_COMPAT and complete CENTOS result. Regular is skipped, compat is kept..
        mock_job2.downstream_pipeline = {'status': 'success'}
        result = buglinker.process_bridge_jobs(mock_instance, mock_branch, [mock_job1, mock_job2,
                                                                            mock_job3])
        self.assertEqual(result, {PipelineType.REALTIME: mock_process_bridge_job.return_value,
                                  PipelineType.RHEL_COMPAT: mock_process_bridge_job.return_value})

        # No checkout data.
        mock_process_bridge_job.return_value = {}
        result = buglinker.process_bridge_jobs(mock_instance, mock_branch, [mock_job1])
        self.assertEqual(result, {})
