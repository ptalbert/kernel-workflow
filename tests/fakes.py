"""Fake classes."""
from unittest import mock

import gitlab


class FakeGitLabDict(dict):
    """Mimic GitLab access with a dictionary."""

    def __init__(self, attr_lookup=None):
        """Set the __allow_path_lookup attribute."""
        super().__init__()
        self.__attr_lookup = attr_lookup

    def get(self, key, **kwargs):
        """Override None with GitlabGetError if key doesn't exist."""
        if key in self:
            return self[key]
        if self.__attr_lookup:
            attr = self.__attr_lookup
            try:
                return next((v for v in self.values() if getattr(v, attr) == key))
            except StopIteration:
                pass
        raise gitlab.GitlabGetError


class FakeGitLabMember:
    """Class representing member of group or project."""

    def __init__(self, username):
        """Initialize."""
        self.username = username
        self.attributes = {'username': username}


class FakeGitLabMembers:
    """Manager class for group or project members."""

    def __init__(self):
        """Initialize."""
        self._member_list = []

    def list(self, **kwargs):
        """Get all members."""
        return self._member_list

    def add_member(self, username):
        """Add a member."""
        self._member_list.append(FakeGitLabMember(username))


class FakeMergeRequest:
    """Fake merge request."""

    def __init__(self, mr_id, attributes, actual_attributes=None):
        """Initialize."""
        self.iid = mr_id
        self.attributes = dict(iid=mr_id, **(attributes or {}))
        self.labels = []
        self.save = mock.Mock()
        self.commits = mock.Mock()
        self.discussions = mock.Mock()
        self.notes = mock.Mock()
        self.approvals = mock.Mock()
        self.approval_rules = mock.Mock()

        if actual_attributes is not None:
            for attribute, value in actual_attributes.items():
                setattr(self, attribute, value)


class FakeGitLabProject:
    """GitLab project."""

    def __init__(self, project_id, path_with_namespace):
        """Initialize."""
        self.attributes = {}
        self.id = int(project_id)
        self.jobs = FakeGitLabDict()
        self.manager = mock.MagicMock()
        self.members_all = FakeGitLabMembers()
        self.mergerequests = FakeGitLabDict()
        self.path_with_namespace = path_with_namespace
        self.name = self.path_with_namespace.rsplit('/', 1)[-1]
        self.pipelines = FakeGitLabDict()

    def add_job(self, job_id, stage, status):
        """Add a job."""
        self.jobs[job_id] = FakeGitLabJob(job_id, stage, status)
        return self.jobs[job_id]

    def add_mr(self, mr_id, attributes=None, actual_attributes=None):
        """Add a MR."""
        updated_attributes = {'project_id': self.id} | (actual_attributes or {})
        self.mergerequests[mr_id] = FakeMergeRequest(mr_id, attributes, updated_attributes)
        return self.mergerequests[mr_id]

    def add_pipeline(self, pipeline_id, attributes=None, variables=None):
        """Add a pipeline."""
        self.pipelines[pipeline_id] = FakeGitLabPipeline(pipeline_id, attributes, variables)
        return self.pipelines[pipeline_id]


class FakeGitLabGroup:
    """GitLab group."""

    def __init__(self):
        """Initialize."""
        self.attributes = {}
        self.manager = mock.MagicMock()
        self.members_all = FakeGitLabMembers()
        self.milestones = mock.MagicMock()


class FakeGitLab:
    """GitLab."""

    def __init__(self):
        """Initialize."""
        self.projects = FakeGitLabDict(attr_lookup='path_with_namespace')
        self.groups = FakeGitLabDict()

    def add_project(self, project_id, path_with_namespace):
        """Add a project."""
        self.projects[project_id] = FakeGitLabProject(project_id, path_with_namespace)
        self.projects[project_id].manager = mock.Mock(spec=['gitlab'], gitlab=self)
        return self.projects[project_id]

    def add_group(self, group_name):
        """Add a group."""
        self.groups[group_name] = FakeGitLabGroup()
        return self.groups[group_name]

    def auth(self):
        """Mimic auth command."""
        pass


class FakeGitLabPipeline:
    """Fake pipeline."""

    def __init__(self, pipeline_id, attributes=None, variables=None):
        """Initialize."""
        self.id = pipeline_id
        self.attributes = dict(iid=pipeline_id, **(attributes or {}))
        mock_vars = mock.Mock()
        mock_vars.list.return_value = \
            [mock.Mock(key=kv['key'], value=kv['value']) for kv in variables] if variables else []
        self.variables = mock_vars
        self.bridges = mock.Mock()
        self.jobs = mock.Mock()
        self.retry = mock.Mock()


class FakeGitLabJob:
    """Fake pipeline job."""

    def __init__(self, job_id, stage, status):
        """Initialize."""
        setattr(self, 'id', job_id)
        setattr(self, 'stage', stage)
        setattr(self, 'status', status)
        self.retry = mock.Mock()

    def __getitem__(self, key):
        """Implement get/[]."""
        return getattr(self, key)
