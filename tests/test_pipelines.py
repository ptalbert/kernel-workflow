"""Webhook interaction tests."""
from copy import deepcopy
from datetime import datetime
from unittest import TestCase

from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType

# A regular pipeline which has completed successfully.
PIPE1_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'kernel-results'}]
PIPE1_DICT = {'allowFailure': False,
              'createdAt': '2023-04-11T13:39:18Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/345',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE1_STAGES},
                                     'status': 'SUCCESS'},
              'id': 'gid://gitlab/Ci::Bridge/865',
              'name': 'c9s_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'SUCCESS'}

# A running RT pipeline.
PIPE2_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'RUNNING'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'PENDING'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'PENDING'}]}, 'name': 'kernel-results'}]
PIPE2_DICT = {'allowFailure': True,
              'createdAt': '2023-04-11T13:37:10Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/936',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE2_STAGES},
                                     'status': 'RUNNING'},
              'id': 'gid://gitlab/Ci::Bridge/235',
              'name': 'c9s_rt_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'RUNNING'}

# An older failed RT pipeline.
PIPE3_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build-tools'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'setup'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'test'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'wait-for-triage'},
                {'jobs': {'nodes': [{'status': 'FAILED'}]}, 'name': 'kernel-results'}]
PIPE3_DICT = {'allowFailure': True,
              'createdAt': '2023-04-10T23:13:52Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/726',
                                     'project': {'id': 'gid://gitlab/Project/765',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE3_STAGES},
                                     'status': 'FAILED'},
              'id': 'gid://gitlab/Ci::Bridge/161',
              'name': 'c9s_rt_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/653'},
              'status': 'FAILED'}

# A kernel-ark pipeline.
PIPE4_STAGES = [{'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'prepare'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'merge'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'build'},
                {'jobs': {'nodes': [{'status': 'SUCCESS'}]}, 'name': 'publish'}]
PIPE4_DICT = {'allowFailure': False,
              'createdAt': '2023-04-15T01:12:12Z',
              'downstreamPipeline': {'id': 'gid://gitlab/Ci::Pipeline/622',
                                     'project': {'id': 'gid://gitlab/Project/836',
                                                 'fullPath': 'group/project'},
                                     'stages': {'nodes': PIPE4_STAGES},
                                     'status': 'SUCCESS'},
              'id': 'gid://gitlab/Ci::Bridge/243',
              'name': 'ark_merge_request',
              'pipeline': {'id': 'gid://gitlab/Ci::Pipeline/874'},
              'status': 'SUCCESS'}


class TestPipelineType(TestCase):
    """Tests for the PipelineType enum."""

    PIPE_MAP = {'rhel9_private_merge_request': PipelineType.RHEL,
                'c9s_merge_request': PipelineType.CENTOS,
                'c9s_realtime_merge_request': PipelineType.REALTIME,
                'c9s_rt_merge_request': PipelineType.REALTIME,
                'c9s_automotive_merge_request': PipelineType.AUTOMOTIVE,
                'c9s_rhel9_compat_merge_request': PipelineType.RHEL_COMPAT,
                'c9s_64k_merge_request': PipelineType._64K,
                'c9s_debug_merge_request': PipelineType.DEBUG,
                'ark_merge_request': PipelineType.ARK
                }

    def _test_pipeline(self, func, pipe_name, result):
        result_string = result.name if isinstance(result, PipelineType) else result
        print(f"Testing {func.__name__}() with '{pipe_name}', expecting '{result_string}'...")
        self.assertIs(func(pipe_name), result)

    def test_pipelinetype_from_str(self):
        """Returns the expected PipelineType value for the given input."""
        func = PipelineType.get
        self._test_pipeline(func, '', PipelineType.INVALID)
        self._test_pipeline(func, '_what_', PipelineType.INVALID)
        self._test_pipeline(func, 'rhel', PipelineType.RHEL)
        self._test_pipeline(func, 'CentOS', PipelineType.CENTOS)
        self._test_pipeline(func, 'REALTIME', PipelineType.REALTIME)
        self._test_pipeline(func, 'Rhel_Compat', PipelineType.RHEL_COMPAT)
        self._test_pipeline(func, 'Automotive', PipelineType.AUTOMOTIVE)
        self._test_pipeline(func, '64k', PipelineType._64K)
        self._test_pipeline(func, 'debug', PipelineType.DEBUG)
        self._test_pipeline(func, 'ark', PipelineType.ARK)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            self._test_pipeline(func, pipe_name, pipe_type)


class TestPipeStatus(TestCase):
    """Tests for the PipelineStatus enum."""

    def test_title(self):
        """Title should capitalize() all Statuses except OK."""
        self.assertEqual(PipelineStatus.FAILED.title, 'Failed')
        self.assertEqual(PipelineStatus.CREATED.title, 'Running')
        self.assertEqual(PipelineStatus.OK.title, 'OK')


class TestPipelineResult(TestCase):
    """Tests for the PipelineResult dataclass."""

    def test_init(self):
        """Creates a new PipelineResult with default values."""
        data = {'allow_failure': False,
                'bridge_gid': '',
                'bridge_name': '',
                'created_at': '',
                'ds_pipeline_gid': '',
                'ds_project_gid': '',
                'ds_namespace': '',
                'mr_pipeline_gid': '',
                'stage_data': [],
                'status': ''
                }

        new_pipe = PipelineResult(**data)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.UNKNOWN)
        self.assertEqual(new_pipe.created_at, '')

        # Derived properties.
        self.assertEqual(new_pipe.bridge_id, 0)
        self.assertEqual(new_pipe.ds_pipeline_id, 0)
        self.assertEqual(new_pipe.ds_project_id, 0)
        self.assertEqual(new_pipe.ds_url, None)
        self.assertEqual(new_pipe.mr_pipeline_id, 0)
        self.assertEqual(new_pipe.label, None)
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertEqual(new_pipe.kcidb_data, None)
        self.assertEqual(new_pipe.kcidb_report, None)
        self.assertIs(new_pipe.type, PipelineType.INVALID)

        # represent
        self.assertIn('status: UNKNOWN', str(new_pipe))

    def test_init_from_dict_pipe1(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE1_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, False)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.OK)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 11, 13, 39, 18))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.OK)
        self.assertEqual(new_pipe.bridge_id, 865)
        self.assertEqual(new_pipe.ds_pipeline_id, 345)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.ds_url, 'https://gitlab.com/group/project/-/pipelines/345')
        self.assertEqual(new_pipe.label, 'CKI_CentOS::OK')
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.CENTOS)

    def test_init_from_dict_pipe2(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE2_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, True)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.RUNNING)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 11, 13, 37, 10))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.RUNNING)
        self.assertEqual(new_pipe.bridge_id, 235)
        self.assertEqual(new_pipe.ds_pipeline_id, 936)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.ds_url, 'https://gitlab.com/group/project/-/pipelines/936')
        self.assertEqual(new_pipe.label, 'CKI_RT::Running')
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.REALTIME)

    def test_init_from_dict_pipe3(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE3_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, True)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.FAILED)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 10, 23, 13, 52))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.FAILED)
        self.assertEqual(new_pipe.bridge_id, 161)
        self.assertEqual(new_pipe.ds_pipeline_id, 726)
        self.assertEqual(new_pipe.ds_project_id, 765)
        self.assertEqual(new_pipe.mr_pipeline_id, 653)
        self.assertEqual(new_pipe.label, 'CKI_RT::Warning')
        self.assertEqual(new_pipe.failed_stage.name, 'kernel-results')
        self.assertIs(new_pipe.type, PipelineType.REALTIME)

    def test_init_from_dict_pipe4(self):
        """Object properties should be set."""
        input_dict = deepcopy(PIPE4_DICT)
        new_pipe = PipelineResult.from_dict(input_dict)

        # Attributes.
        self.assertIs(new_pipe.allow_failure, False)

        # Transformed attributes.
        self.assertIs(new_pipe.status, PipelineStatus.SUCCESS)
        self.assertEqual(new_pipe.created_at, datetime(2023, 4, 15, 1, 12, 12))

        # Derived properties.
        self.assertIs(new_pipe.status, PipelineStatus.SUCCESS)
        self.assertEqual(new_pipe.bridge_id, 243)
        self.assertEqual(new_pipe.ds_pipeline_id, 622)
        self.assertEqual(new_pipe.ds_project_id, 836)
        self.assertEqual(new_pipe.mr_pipeline_id, 874)
        self.assertEqual(new_pipe.label, 'CKI_ARK::OK')
        self.assertEqual(new_pipe.failed_stage, None)
        self.assertIs(new_pipe.type, PipelineType.ARK)

    def test_label_unknown_type_pipe4(self):
        """A pipeline of unknown type should not have a label."""
        input_dict = deepcopy(PIPE4_DICT)
        input_dict['name'] = 'unknown bridge job'
        new_pipe = PipelineResult.from_dict(input_dict)
        self.assertIs(new_pipe.type, PipelineType.INVALID)
        self.assertIs(new_pipe.label, None)

    def test_prepare_pipelines(self):
        """Generates a list of PipelineResults from the input dict and excludes older results."""
        pipes = PipelineResult.prepare_pipelines([PIPE3_DICT, PIPE2_DICT, PIPE1_DICT])

        self.assertEqual(len(pipes), 2)
        self.assertEqual(pipes[0].ds_pipeline_id, 936)
        self.assertEqual(pipes[1].ds_pipeline_id, 345)

    def test_get_stage(self):
        """Returns the stage_data value matching the given key."""
        new_pipe = PipelineResult.from_dict(PIPE4_DICT)
        self.assertEqual(new_pipe.get_stage('build'),
                         {'jobs': [{'status': 'SUCCESS'}], 'name': 'build'})
